/* ----- Overall PCB geometry ---------------------------------------------- */

#define	PCB_X		80mm
#define	PCB_Y		25mm

#define	R_LEFT		3.2mm
#define	R_RIGHT		(PCB_Y/2)

/* ----- Mechanical holes -------------------------------------------------- */

#define	PCB_LAN_X	R_LEFT
#define	PCB_LAN_Y	(R_LEFT+PCB_LAN_ECCENT/2)
#define	PCB_LAN_R	2.2mm
#define	PCB_LAN_ECCENT	1mm

#define	HOLE_1_X	7.7mm
#define	HOLE_1_Y	2.0mm
#define	HOLE_1_R	1.1mm

#define	HOLE_2_X	41.7mm
#define	HOLE_2_Y	23.0mm
#define	HOLE_2_R	1.1mm

#define	HOLE_3_X	58.6mm
#define	HOLE_3_Y	23.0mm
#define	HOLE_3_R	1.1mm

/* ----- Components -------------------------------------------------------- */

#include "pos.inc"


#define	BAT_X		CON4_X
#define	BAT_Y		CON4_Y

#define	SW_X		SW2_X
#define	SW_Y		SW2_Y

#define	USB_X		CON2_X
#define	USB_Y		CON2_Y

#define	MEM_X		CON3_X
#define	MEM_Y		CON3_Y

#define	WHEEL_X		SW1_X
#define	WHEEL_Y		SW1_Y

#define	DISP_X		30.9mm
#define	DISP_Y		13.3mm

#define	LED_X		D1_X
#define	LED_Y		D1_Y
