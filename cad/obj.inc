/*
 * Draw a rectangle.
 */

#define	RECT_COORD(name, center, x, y)		\
	name##_ll: vec center(-(x)/2, -(y)/2);	\
	name##_ul: vec .(0mm, (y));		\
	name##_ur: vec .(x, 0mm);		\
	name##_lr: vec .(0mm, -(y))

#define	RECT(name, center, x, y, w)		\
	RECT_COORD(name, center, x, y);	\
	line name##_ll name##_lr w;		\
	line name##_ul name##_ur w;		\
	line name##_ll name##_ul w;		\
	line name##_lr name##_ur w

/*
 * Draw a rounded edge in one quadrant
 *
 * Draw an arc from corner point vc plus rector ra to vc+rb. vc+ra is stored
 * in va and vc+rb is stored in vb.
 */

#define Q(va, vb, vc, ra, rb, w)	\
    va: vec vc ra;			\
    vb: vec vc rb;			\
    vc##_center: vec va rb;		\
    arc vc##_center va vb w

#define QLL(pfx, r, w) \
    Q(pfx##lly, pfx##llx, pfx##ll, (0mm, r), (r, 0mm), w)
#define QLR(pfx, r, w) \
    Q(pfx##lrx, pfx##lry, pfx##lr, (-(r), 0mm), (0mm, r), w)
#define QUL(pfx, r, w) \
    Q(pfx##ulx, pfx##uly, pfx##ul, (r, 0mm), (0mm, -(r)), w)
#define QUR(pfx, r, w) \
    Q(pfx##ury, pfx##urx, pfx##ur, (0mm, -(r)), (-(r), 0mm), w)

#define	DRAW_ARCS(pfx, r, w)	\
   QLL(pfx, r, w);		\
   QUL(pfx, r, w);		\
   QLR(pfx, r, w);		\
   QUR(pfx, r, w)
