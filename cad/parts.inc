#include "obj.inc"


/* ----- TSWA wheel -------------------------------------------------------- */

frame tswa_xy {
	// wheel edge
	vec @(0mm, 22mm/2)
	circ @ . w

	// center button
	vec @(0mm, 8.8mm/2)
	circ @ . w
}

frame tswa_xz {
	set r = 11mm
	set h = 4.95mm

	c: vec @(0mm, h/2)
	RECT(rect, c, r, h, w)
}


/* ----- 1.3" OLED --------------------------------------------------------- */

frame oled_xy {
	// panel size
	set px = 34.5mm
	set py = 23.0mm

	// active area
	set ax = 29.42mm
	set ay = 14.7mm
	set aoff = py/2-ay/2-2.1mm

	// cable (folded)
	set cx = 12.0mm
	set cy = 1.6mm

	pc: vec @(0mm, aoff)
	cc: vec @(0mm, -py/2-cy/2)

	RECT(panel, @, px, py, w)
	RECT(active, pc, ax, ay, w)
	RECT(cable, cc, cx, cy, w)
}

frame oled_xz {
	// panel size
	set px = 34.5mm
	set pz = 1.6mm

	c: vec @(0mm, pz/2)
	RECT(panel, c, px, pz, w)
}


/* ----- 0603 LED ---------------------------------------------------------- */


frame led_xy {
	RECT(rect, @, 0.8mm, 1.1mm, w)
}

frame led_xz {
}
