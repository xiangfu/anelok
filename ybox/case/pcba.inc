/* ----- Overall PCB geometry ---------------------------------------------- */

#define	PCB_X		30mm
#define	PCB_Y		18.5mm

#define	PCB_R		2mm	/* corners */

/* ----- Components -------------------------------------------------------- */

#include "pos.inc"


#define	USBA_X		CON3_X
#define	USBA_Y		CON3_Y

#define	USBB_X		CON2_X
#define	USBB_Y		CON2_Y

#define	PWR_X		CON1_X
#define	PWR_Y		CON1_Y

#define	LED_X		D1_X
#define	LED_Y		D1_Y
