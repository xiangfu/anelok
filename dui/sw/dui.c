#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <ubb/ubb.h>
#include <ubb/regs4740.h>


#include "hello.xbm"


#define	OLED_nCS	UBB_DAT2
#define	OLED_nRES	UBB_DAT3
#define	OLED_DnC	UBB_CMD
#define	OLED_SCLK	UBB_CLK
#define	OLED_SDIN	UBB_DAT0

#define	OLED_ALL	(OLED_nCS | OLED_nRES | OLED_DnC | OLED_SCLK | \
			    OLED_SDIN)

#define	WHEEL_COM	UBB_DAT1
#define	WHEEL_NO	UBB_CMD
#define	WHEEL_A		UBB_DAT0
#define	WHEEL_B		UBB_CLK

#define	OLED_CONTRAST	0x81	/* reset default: 0x7f */
#define	OLED_DISP_RAM	0xa4	/* reset refault */
#define	OLED_DISP_ALL	0xa5
#define	OLED_DISP_NORM	0xa6	/* reset default */
#define	OLED_DISP_INV	0xa7
#define	OLED_DISP_OFF	0xae	/* reset default */
#define	OLED_DISP_ON	0xaf

#define	OLED_CLKDIV	0xd5	/* reset default: 0x80 */


static void reset(void)
{
	CLR(OLED_nRES);
	usleep(100*1000);
	SET(OLED_nRES);
}


static void spi_begin(void)
{
	CLR(OLED_nCS);
}


static void spi_end(void)
{
	SET(OLED_nCS);
}


static void spi_send(uint8_t v)
{
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (v & mask)
			SET(OLED_SDIN);
		else
			CLR(OLED_SDIN);
		SET(OLED_SCLK);
		CLR(OLED_SCLK);
	}
}


static void cmd1(uint8_t cmd)
{
//fprintf(stderr, "cmd1 0x%02x\n", cmd);
//sleep(1);
	CLR(OLED_DnC);
	spi_begin();
	spi_send(cmd);
	spi_end();
}


static void cmd2(uint8_t cmd, uint8_t arg)
{
//fprintf(stderr, "cmd2 0x%02x 0x%02x\n", cmd, arg);
//sleep(1);
	CLR(OLED_DnC);
	spi_begin();
	spi_send(cmd);
	spi_send(arg);
	spi_end();

}


static void image(const unsigned char *img)
{
	uint8_t page, col;
	const uint8_t *p;
	uint8_t m, v, i;

	for (page = 0; page != 8; page++) {
		cmd1(0xb0 | page);
		cmd1(0x10);
		cmd1(0x00);
//			cmd1(0x10 | col >> 4);
//			cmd1(col & 0xf);
		SET(OLED_DnC);
		spi_begin();
		for (col = 0; col != 128; col++) {
			p = img+(col >> 3)+(page*128);
			m = 1 << (col & 7);
			v = 0;
			for (i = 0; i != 8; i++)
				if (p[16*i] & m)
					v |= 1 << i;
			spi_send(v);
		}		
		spi_end();
	}
}


#define	POS_Y	55
#define	POS_MIN	3
#define	POS_MAX	(127-POS_MIN-1)


static void pixel(int x, int y, bool on)
{
	uint8_t *p = hello_bits;
	uint8_t m;

	m = 1 << (x & 7);
	p += 16*y+(x >> 3);
	if (on)
		*p |= m;
	else
		*p &= ~m;
}


static void arrow(int pos, bool on)
{
	int d, x;

	for (d = 0; d != 3; d++)
		for (x = pos-d; x <= pos+d; x++)
			pixel(x, POS_Y-d, on);
}


static bool poll_wheel(void)
{
	static bool last, cw;
	static int pos = POS_MIN;
	bool no, a, b;

	CLR(WHEEL_COM);
	IN(WHEEL_NO | WHEEL_A | WHEEL_B);
	usleep(1);	/* minimum sleep */

	no = !PIN(WHEEL_NO);
	a = !PIN(WHEEL_A);
	b = !PIN(WHEEL_B);

	SET(WHEEL_COM);
	OUT(WHEEL_NO | WHEEL_A | WHEEL_B);

	if (a != b) {
		cw = a != last;
	} else {
		if (a != last) {
			last = a;
			arrow(pos, 0);
			pos += cw ? 4 : -4;
			if (pos < POS_MIN)
				pos = POS_MAX;
			if (pos > POS_MAX)
				pos = POS_MIN;
			arrow(pos, 1);
		}
	}

//fprintf(stderr, "no %d a %d b %d\n", no, a, b);
	return no;
}


int main(void)
{
	ubb_open(UBB_ALL);

	SET(OLED_ALL | WHEEL_COM);
	OUT(OLED_ALL | WHEEL_COM);

	/*
	 * CLK/SCLK/B has no hardwired pull-up in the Ben. Therefore, enable
	 * the CPU's pull-up.
	 */
	PDPULLC = UBB_CLK;

	fprintf(stderr, "pre-charging\n");
	usleep(200*1000);

	ubb_power(1);

	fprintf(stderr, "power on\n");

	reset();

	fprintf(stderr, "did reset\n");

	cmd1(0xae);		/* turn off oled panel */
	cmd2(0xd5, 0x80);	/* display clock divide ratio */
	cmd2(0xa8, 0x3f);	/* multiplex ratio  = 1/64 */
	cmd2(0xd3, 0x00);	/* display offset = 0 */
	cmd2(0x8d, 0x14);	/* enable charge pump */
	cmd1(0x40);		/* address line */
	cmd1(0xa6);		/* normal display */
	cmd1(0xa4);		/* display RAM */
	cmd1(0xa1);		/* re-map 128 to 0 */
	cmd1(0xc8);		/* scan direction 64 to 0 */
	cmd2(0xda, 0x12);	/* COM pins hardware configuration */
	cmd2(0x81, 0xf0);	/* contrast */
	cmd2(0xd9, 0xf1);	/* pre-charge period */
	cmd2(0xdb, 0x40);	/* set vcomh */
	cmd1(0xaf);		/* turn on panel */

	while (1) {
		image(hello_bits);
		if (poll_wheel())
			break;
	}

	cmd1(0xae);
	cmd2(0x8d, 0x10);
	usleep(100*1000);
	ubb_power(0);

	return 0;
}
