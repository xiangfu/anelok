#
# case/Makefile.cnc - Common Makefile for CNCing two-part cases
#
# Written 2013 by Werner Almesberger
# Copyright 2013 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

SHELL = /bin/bash

CAE_TOOLS = $(COMMON)/../..//cae-tools
CAMEO = $(CAE_TOOLS)/cameo
SPOOL = $(CAE_TOOLS)/spool/spool
CNGT = $(CAE_TOOLS)/cngt/cngt
GP2RML = $(CAE_TOOLS)/gp2rml/gp2rml

GENPIECE = $(COMMON)/genpiece.pl
CAM_SH = $(COMMON)/cam.sh

PARAMS = X0=$(X0)mm Y0=$(Y0)mm Z1=$(Z1)mm MILL=$(MILL) PIECE_Z=$(PIECE_Z)mm \
	 Z_OFFSET=$(Z_OFFSET)mm ROTATE=$(ROT)

Z_SCALE ?= 1

.PHONY:		all plot plot-fin mill finish pos cng
.PHONY:		stl view
.PHONY:		clean spotless

# ----- Milling ---------------------------------------------------------------

all:		$(PART)-mill.rml # $(PART)-finish.rml

$(NAME).gp:	$(NAME).fpd
		fped -g $(NAME).fpd || { rm -f $@; exit 1; }

PART_FLIP_top ?= -r
PART_FLIP_bot ?= -r -y
PART_FLIP = $(PART_FLIP_$(PART))

$(PART).gp:	$(NAME).gp
		$(CAMEO)/fped2d2z.pl $(PART_FLIP) 0=-$(OVERSHOOT) \
		    $(PART) $^ >$@ || { rm -f $@; exit 1; }

$(PART)-piece.gp: $(PART).gp
		$(GENPIECE) -r $(PIECE) $< >$@ || { rm -f $@; exit 1; }

$(PART)-area.gp: $(PART)-piece.gp $(PART).gp $(CAM_SH) Makefile
		$(CAM_SH) PART=$(PART) CAMEO=$(CAMEO)/cameo $(PARAMS) \
		    OUT=$@ || \
		    { rm -f $@; exit 1; }

$(PART)-finish.gp: $(PART)-piece.gp $(PART).gp $(CAM_SH) Makefile
		$(CAM_SH) PART=$(PART) CAMEO=$(CAMEO)/cameo $(PARAMS) \
		    Z_OFFSET=0mm REVERSE=reverse OUT=$@ || { rm -f $@; exit 1; }

$(PART)-mill.gp: $(PART)-area.gp Makefile
		$(CAMEO)/zstack.pl $(Z1) $(Z_STEP) $< >$@ || \
		    { rm -f $@; exit 1; }

$(PART)-mill.rml: $(PART)-mill.gp Makefile
		$(GP2RML) -s $(Z_SCALE) \
		    $(CLEARANCE) $(SPEED_XY) $(SPEED_Z) $< >$@ || \
		    { rm -f $@; exit 1; }

$(PART)-finish.rml: $(PART)-finish.gp Makefile
		$(GP2RML) -s $(Z_SCALE) \
		    $(CLEARANCE) $(FIN_SPEED_XY) $(FIN_SPEED_Z) \
		    $< >$@ || { rm -f $@; exit 1; }

plot:		$(PART)-mill.gp
		echo 'splot "$<" with lines' | gnuplot -persist

plot-fin:	$(PART)-finish.gp
		echo 'splot "$<" with lines' | gnuplot -persist

mill:		$(PART)-mill.rml # $(PART)-finish.rml
		PORT=/dev/ttyUSB0 $(SPOOL) $^

finish:		$(PART)-finish.rml
		PORT=/dev/ttyUSB0 $(SPOOL) $^

pos:
		$(CNGT) 0

cng:		$(PART)-mill.gp
		$(CNGT) $(Z1) 10 $<

# ----- Visualization ---------------------------------------------------------

stl:		$(PARTS:%=%.stl)

#STL_FLIP_top ?=
STL_FLIP_bot ?= -y

%.stl:		$(NAME).gp
		PATH=$(CAMEO):$$PATH $(CAMEO)/fped2stl.pl \
		    $(STL_FLIP_$(@:%.stl=%)) \
		    $(@:%.stl=%_) $^ >$@ || \
		    { rm -f $@; exit 1; }

view:		$(PARTS:%=%.stl)
		for n in $^; do meshlab $$n || exit; done

view%:		$(PARTS:%=%.stl)
		i=0; f=$@; for n in $^; do i=`expr $$i + 1`; \
		  if [ $$i -eq "$${f#view}" ]; then  meshlab $$n; exit; fi; done

# ----- Cleanup ---------------------------------------------------------------

clean::
		rm -f $(NAME).gp
		rm -f $(PARTS:%=%-piece.gp) $(PARTS:%=%.gp)
		rm -f $(PARTS:%=%-area.gp)
		rm -f $(PARTS:%=%-mill.gp)
		rm -f $(PARTS:%=%-finish.gp)
		rm -f $(PARTS:%=%.stl)

spotless::	clean
		rm -f $(PARTS:%=%-mill.rml)
		rm -f $(PARTS:%=%-finish.rml)
