#!/usr/bin/perl

while ($ARGV[0] =~ /^-[a-z]/) {
	if ($ARGV[0] eq "-r") {
		shift @ARGV;
		$reverse = 1;
	} elsif ($ARGV[0] eq "-d") {
		shift @ARGV;
		$delta = 1;
	}
}

$x0 = shift @ARGV;
$y0 = shift @ARGV;
$x1 = shift @ARGV;
$y1 = shift @ARGV;

while (<>) {
	next unless /(-?\d+(\.\d*)?)\s+(-?\d+(\.\d*)?)\s+(-?\d+(\.\d*)?)/;
	($x, $y, $z) = ($1, $3, $5);
	$z{$z} = 1;
	$xmin = $x if $x < $xmin || !defined $xmin;
	$xmax = $x if $x > $xmax || !defined $xmax;
	$ymin = $y if $y < $ymin || !defined $ymin;
	$ymax = $y if $y > $ymax || !defined $ymax;
	$zmax = $z if $z > $zmax || !defined $zmax;
}

if ($reverse) {         
	($x0, $y0) = ($y0, $x0);
	($x1, $y1) = ($y1, $x1);
}

if ($delta) {
	$x0 = $xmin-$x0;
	$x1 = $xmax+$x1;
	$y0 = $ymin-$y0;
	$y1 = $ymax+$y1;
}

for (keys %z) {
	print "$x0 $y0 $_\n";
	print "$x1 $y0 $_\n";
	print "$x1 $y1 $_\n";
	print "$x0 $y1 $_\n";
	print "$x0 $y0 $_\n\n";
}
