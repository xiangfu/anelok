#!/usr/bin/python

import sys, os
import tmc.meter, time
from tmc import instrument


def usage():
	sys.stderr.write("usage: " + sys.argv[0] +
	    " [-c command] outfile seconds\n")
	sys.exit(1)


argv = sys.argv

command = None
if argv[1] == "-c":
	command = argv[2]
	del argv[1:3]

if len(argv) != 3:
	usage()
file = argv[1]
t = float(argv[2])

instrument.instrument.debug_default = False
m = tmc.meter.fluke_8845a("fluke")

m.send('*RST;:SYST:REM;' +
    ':FUNC "CURR:DC";:CURR:DC:RANG 0.1;' +
    ':CURR:RES 1e-5;:CURR:NPLC 0.2;' +
    ':TRIG:SOUR IMM;:TRIG:DEL:AUTO ON;:TRIG:COUN INF')

m.send(':READ?')

m.start(file)
sys.stderr.write("measuring ...");
if command is not None:
	os.system(command)
time.sleep(t)
m.stop()
sys.stderr.write(" done\n");
