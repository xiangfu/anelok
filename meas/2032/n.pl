#!/usr/bin/perl
#use Time::Piece;
use Date::Parse;

#
# Note: Time::Piece is supposedly [1] "better" than Date::Parse
# [1] http://stackoverflow.com/questions/21714073/perl-how-to-parse-date-from-a-string
# However, it's unclear what formats Time::Piece->strptime supports and a best
# guess using available documentation does not work. Date::Parse, on the other
# hand, works great !
#

if ($ARGV[0] eq "-e") {
	shift @ARGV;
	$t0 = 0;
}
while (<>) {
	next unless /^(\S+)(\d\d)\s(\S+)\s*$/;
#	$t = Time::Piece->strptime($1.":".$2, "%FT%T%z");
#	$t = $t->epoch;
	$t = str2time($1.$2);
	$t0 = $t unless defined $t0;
	print $t - $t0, " ", $3, "\n";
}
