#!/usr/bin/python
import tmc.meter
from tmc import instrument

instrument.instrument.debug_default = False
m = tmc.meter.fluke_8845a("fluke")
m.debug = False
m.v(10)
print m.sample()
