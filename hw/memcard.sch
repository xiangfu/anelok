EESchema Schematic File Version 2
LIBS:pwr
LIBS:r
LIBS:c
LIBS:led
LIBS:powered
LIBS:kl25-48
LIBS:memcard8
LIBS:micro_usb_b
LIBS:varistor
LIBS:pmosfet-gsd
LIBS:antenna
LIBS:er-oled-fpc30
LIBS:cc2543
LIBS:balun-smt6
LIBS:xtal-4
LIBS:testpoint
LIBS:nmosfet-gsd
LIBS:diode
LIBS:gencon
LIBS:aat1217
LIBS:inductor
LIBS:crystal
LIBS:switch
LIBS:anelok-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "Anelok Password Safe"
Date "12 jul 2014"
Rev ""
Comp "Werner Almesberger"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MEMCARD8-SHIELD3-SW1 CON3
U 1 1 5235E012
P 7050 4550
F 0 "CON3" H 6950 5050 60  0000 L CNN
F 1 "101-00660-68-6" H 7300 4100 60  0000 C CNN
F 2 "" H 7050 4550 60  0000 C CNN
F 3 "" H 7050 4550 60  0000 C CNN
	1    7050 4550
	1    0    0    -1  
$EndComp
$Comp
L PMOSFET-GSD Q5
U 1 1 5235EA37
P 4900 2200
F 0 "Q5" H 4650 1900 60  0000 C CNN
F 1 "DMG1013T" H 4600 2500 60  0000 C CNN
F 2 "" H 4900 2200 60  0000 C CNN
F 3 "" H 4900 2200 60  0000 C CNN
	1    4900 2200
	1    0    0    1   
$EndComp
Wire Wire Line
	5000 4450 6700 4450
Wire Wire Line
	5000 4450 5000 2600
$Comp
L GND #PWR067
U 1 1 5235EA65
P 5000 5000
F 0 "#PWR067" H 5000 5000 30  0001 C CNN
F 1 "GND" H 5000 4930 30  0001 C CNN
F 2 "" H 5000 5000 60  0000 C CNN
F 3 "" H 5000 5000 60  0000 C CNN
	1    5000 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR068
U 1 1 5235EA72
P 8100 5050
F 0 "#PWR068" H 8100 5050 30  0001 C CNN
F 1 "GND" H 8100 4980 30  0001 C CNN
F 2 "" H 8100 5050 60  0000 C CNN
F 3 "" H 8100 5050 60  0000 C CNN
	1    8100 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4650 8100 4650
Wire Wire Line
	8100 4650 8100 5050
Wire Wire Line
	7950 4750 8100 4750
Connection ~ 8100 4750
Wire Wire Line
	6700 4650 5000 4650
Wire Wire Line
	5000 4650 5000 5000
Text GLabel 4300 4550 0    60   Input ~ 0
CARD_CLK
Text GLabel 4300 4700 0    60   3State ~ 0
CARD_DAT0
Text GLabel 4300 4850 0    60   BiDi ~ 0
CARD_DAT1
Text GLabel 4300 4400 0    60   BiDi ~ 0
CARD_CMD
Text GLabel 4300 4250 0    60   BiDi ~ 0
CARD_DAT3
Text GLabel 4300 4100 0    60   BiDi ~ 0
CARD_DAT2
Wire Wire Line
	4300 4550 6700 4550
Wire Wire Line
	4300 4700 4500 4700
Wire Wire Line
	4500 4700 4500 4750
Wire Wire Line
	4500 4750 6700 4750
Wire Wire Line
	4300 4850 6700 4850
Wire Wire Line
	4300 4400 4500 4400
Wire Wire Line
	4500 4400 4500 4350
Wire Wire Line
	4500 4350 6700 4350
Wire Wire Line
	4300 4250 6700 4250
Wire Wire Line
	4300 4100 4500 4100
Wire Wire Line
	4500 4100 4500 4150
Wire Wire Line
	4500 4150 6700 4150
Text GLabel 3350 2900 0    60   Input ~ 0
CARD_ON
Wire Wire Line
	4200 2200 4600 2200
Text GLabel 8300 4150 2    60   Output ~ 0
CARD_SW
Wire Wire Line
	7950 4150 8300 4150
$Comp
L 3V3 #PWR069
U 1 1 5235ECA6
P 5000 1100
F 0 "#PWR069" H 5000 1060 30  0001 C CNN
F 1 "3V3" H 5000 1250 60  0000 C CNN
F 2 "" H 5000 1100 60  0000 C CNN
F 3 "" H 5000 1100 60  0000 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1100 5000 1800
Text Notes 850  1200 0    200  ~ 40
Memory card
$Comp
L TESTPOINT TP10
U 1 1 524F47B8
P 5600 3800
F 0 "TP10" H 5600 4050 60  0000 C CNN
F 1 "TESTPOINT" H 5600 3750 60  0001 C CNN
F 2 "" H 5600 3800 60  0000 C CNN
F 3 "" H 5600 3800 60  0000 C CNN
	1    5600 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3800 5600 4450
Connection ~ 5600 4450
Text Label 5800 4450 0    60   ~ 0
CARD_VDD
$Comp
L NMOSFET-GSD Q6
U 1 1 536E4C63
P 4100 2900
F 0 "Q6" H 3950 3225 60  0000 C CNN
F 1 "DMG1012T" H 3750 2600 60  0000 C CNN
F 2 "" H 4100 2900 60  0000 C CNN
F 3 "" H 4100 2900 60  0000 C CNN
	1    4100 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR070
U 1 1 536E4D22
P 4200 3500
F 0 "#PWR070" H 4200 3500 30  0001 C CNN
F 1 "GND" H 4200 3430 30  0001 C CNN
F 2 "" H 4200 3500 60  0000 C CNN
F 3 "" H 4200 3500 60  0000 C CNN
	1    4200 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3300 4200 3500
Wire Wire Line
	3350 2900 3800 2900
Wire Wire Line
	4200 2000 4200 2500
$Comp
L R R18
U 1 1 536E4DAF
P 4200 1750
F 0 "R18" H 4330 1800 60  0000 C CNN
F 1 "1M" H 4350 1700 60  0000 C CNN
F 2 "" H 4200 1750 60  0000 C CNN
F 3 "" H 4200 1750 60  0000 C CNN
	1    4200 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 1500 4200 1300
Wire Wire Line
	4200 1300 5000 1300
Connection ~ 5000 1300
Connection ~ 4200 2200
Text Notes 7800 5500 0    60   ~ 0
CON3               CARD_SW\n\n101-00660-68-6  closes\n101-00581-59     opens\n693071010811      opens\n
Wire Notes Line
	7800 5550 9150 5550
Wire Notes Line
	8700 5400 8700 5900
NoConn ~ 7950 4850
$EndSCHEMATC
