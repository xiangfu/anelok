#!/usr/bin/perl
#
# web/dwg/dwg.pl - Anelok marketing drawing generator
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#



$STEP_WIDTH = 0.5;

$col_pcb = "0.1,0.1,0.7,0.1";
$FIG_ZOOM = 2;
@CAM = (30, 20, 300);


sub steps
{
	local ($r) = @_;

	return 1+int($r*3.141/4/$STEP_WIDTH);
}


sub push
{
	push(@orig, [ $x0, $y0, $z0 ]);
	$x0 += $_[0];
	$y0 += $_[1];
	$z0 += $_[2];
}


sub pop
{
	local @a = @{ pop @orig };

	$x0 = $a[0];
	$y0 = $a[1];
	$z0 = $a[2];
}


sub rad
{
	return $_[0]/180*3.1415926;
}


sub fig
{
	return int($_[0]/25.4*1200*$FIG_ZOOM+0.5);
}


sub project
{
	local ($x, $y, $z) = @_;

#	local ($a, $b) = (&rad(35), &rad(-40));
	local ($a, $b) = (&rad(30), &rad(-58));
	local ($a, $b) = (&rad(22), &rad(-40));

	($x, $y) = (cos($a)*$x-sin($a)*$y, cos($a)*$y+sin($a)*$x);
	($y, $z) = (cos($b)*$y-sin($b)*$z, cos($b)*$z+sin($b)*$y);

	my $s = ($CAM[2]+$z)/$CAM[2];

	($x, $y) = (($x-$CAM[0])*$s+$CAM[0], ($y-$CAM[1])*$s+$CAM[0]);

	push(@p2d, [&fig($x), -&fig($y)]) if $fig;
	print "$x $y\n" unless $fig;
}


sub line
{
	local ($ax, $ay, $az, $bx, $by, $bz) = @_;

	return if $stl;
	if ($gp2d || $fig) {
		&project($x0+$ax, $y0+$ay, $z0+$az);
		&project($x0+$bx, $y0+$by, $z0+$bz);
		print "\n" if $gp2d;
		return;
	}
	print $x0+$ax, " ", $y0+$ay, " ", $z0+$az, "\n";
	print $x0+$bx, " ", $y0+$by, " ", $z0+$bz, "\n\n\n";
}


sub facet
{
	return unless $stl;
	print "facet normal 0 0 0\n";
	print "\touter loop\n";
	for (my $i = 0; $i != 3; $i++) {
		print "\t\tvertex ", join(" ", splice(@_, 0, 3)), "\n";
	}
	print "\tendloop\n";
	print "endfacet\n";
}


sub outline
{
	
}


sub poly3d
{
	my $z = shift @_;
	my $x = $_[$#_-1];
	my $y = $_[$#_];

	if ($scad) {
		my ($za, $zb) = $z > 0 ? ($z0, $z) : ($z0+$z, -$z);

		print "color ([$col]) translate([0,0,$za]) linear_extrude(height = $zb) polygon([";
		while ($#_ != -1) {
			my $x = shift @_;
			my $y = shift @_;

			print "[", $x0+$x, ",", $y0+$y, "]";
			print "," unless $#_ == -1;
		}
		print "], convexity = 4);\n";
		return;
	}

	while ($#_ != -1) {
		my $nx = shift @_;
		my $ny = shift @_;

		&line($x, $y, 0, $nx, $ny, 0);
		&line($x, $y, $z, $nx, $ny, $z);
		&line($nx, $ny, 0, $nx, $ny, $z);
		&facet($x, $y, 0, $x, $y, $z, $nx, $ny, $z);
		&facet($nx, $ny, $z, $nx, $ny, 0, $x, $y, 0);
		($x, $y) = ($nx, $ny);
	}

	if ($fig) {
		my ($x0, $x1, $y0, $y1) = undef;
		for (@p2d) {
			my ($x, $y) = @{ $_ };

			$x0 = $x if $x < $x0 || !defined $x0;
			$x1 = $x if $x > $x1 || !defined $x1;
			$y0 = $y if $y < $y0 || !defined $y0;
			$y1 = $y if $y > $y1 || !defined $y1;
		}
		print "6 $x0 $y0 $x1 $y1\n";
		&outline(@p2d);
		while ($#p2d != -1) {
			my @a = @{ shift @p2d };
			my @b = @{ shift @p2d };

			print "2 1 0 1 0 7 50 -1 -1 0.0 0 1 -1 0 0 2\n";
			print "\t$a[0] $a[1] $b[0] $b[1]\n";
		}
		print "-6\n";
	}
}


sub hypot
{
	local ($a, $b) = @_;

	return sqrt($a*$a+$b*$b);
}


sub poly3d_round
{
	local $z = shift @_;
	local $r = shift @_;

	my @p = ();
	my @t = (@_, $_[0], $_[1], $_[2], $_[3]);

	my $ax = shift @t;
	my $ay = shift @t;
	while ($#t >= 3) {
		my $bx = shift @t;
		my $by = shift @t;
		my $cx = $t[0];
		my $cy = $t[1];

		my $vax = $bx-$ax;
		my $vay = $by-$ay;
		my $vbx = $cx-$bx;
		my $vby = $cy-$by;
		my $la = &hypot($vax, $vay);
		my $lb = &hypot($vbx, $vby);

		my $rax = $vax/$la*$r;
		my $ray = $vay/$la*$r;
		my $rbx = $vbx/$lb*$r;
		my $rby = $vby/$lb*$r;

		my $px = $bx-$rax;
		my $py = $by-$ray;

		push(@p, $px, $py);
		my $n = &steps($r);
		for (my $i = 0; $i <= $n; $i++) {
			my $f = $i/$n;
			my $dx = (1-$f)*$rax + $f*$rbx;
			my $dy = (1-$f)*$ray + $f*$rby;
			$px += $dx/$n*2;
			$py += $dy/$n*2;
			push(@p, $px, $py);
		}
#		push(@p, $bx, $by);
		($ax, $ay) = ($bx, $by);
		
	}
	&poly3d($z, @p);
}


sub rect3d
{
	local ($z, $x, $y) = @_;

	&poly3d($z, 0, 0, $x, 0, $x, $y, 0, $y);
}


sub rect3d_center
{
	local ($z, $x, $y) = @_;

	&push(-$x/2, -$y/2, 0);
	&rect3d(@_);
	&pop;
}


sub cyl
{
	local ($z, $r) = @_;
	my @p = ();

	my $n = &steps($r)*4;
	for (my $i = 0; $i != $n; $i++) {
		my $a = 3.1415926*2/$n*$i;

		push(@p, sin($a)*$r, cos($a)*$r);
	}
	&poly3d($z, @p);
}


# ----- OLED ------------------------------------------------------------------


sub oled
{
	local ($px, $py, $pz) = (34.5, 23, 1.45);
	local ($vx, $vy, $vz) =  (31.42, 16.7, 0.05);

	$col = "0,0,0,0.5";
	&rect3d($pz-$vz, $px, $py);

	$col = "1,1,1,0.5";
	&push(1.54, 1.1, $pz-$vz);
	&rect3d($vz, $vx, $vy);
	&pop;
}


# ----- Case ------------------------------------------------------------------


sub case
{
	&poly3d_round(8.5, 3,
		0, 0,	0, 33,	67, 33,	67, 0);

	&push(3.2, 3.2, 0);
	&cyl(8.5, 1.7);
	&pop;
}


# ----- Main PCB --------------------------------------------------------------


sub cc
{
	$col = "0,0,0,1";
	&rect3d_center(1, 5, 5);
}


sub kl26
{
	$col = "0,0,0,1";
	&rect3d_center(1, 7, 7);
}


sub led
{
	$col = "1,0,0,0.8";
	&rect3d_center(0.8, 0.8, 1.6);
}


sub card_holder
{
	$col = "0.5,0.7,0.5,0.7";
	&rect3d(-1.93, 15.2, 14);
}


sub usb
{
	$col = "0.5,0.5,0.7,0.7";
	&rect3d(-2.8, 7.5, 5.6);
}


sub switch
{
	$col = "0.5,0.5,0.5,0.5";
	&rect3d(-1.4, 6.7, 2.6);

	$col = "0.7,0.7,0.4,0.9";
	&push((6.7-1.3-1.5)/2, 2.6, -0.1);
	&rect3d(-1.1, 1.3, 1.5);
	&pop;
}


sub pcb_main
{
	$col = $col_pcb;
	&poly3d(0.8,
		0, 5.4,		0, 28,		2, 30,		46, 30,
		46, 24.6,	44, 24.6,	42, 22.6,	42, 0,
		5.4, 0,		5.4, 3.2,	3.2, 5.4);

	&push(17.8, 10.7, 0.8);
	&cc;
	&pop;

	&push(31.7, 15.1, 0.8);
	&kl26;
	&pop;

	&push(40.6, 2.1, 0.8)
	&led;
	&pop;

	&push(26.1, 6.7, 0);
	&card_holder;
	&pop;

	&push(27.15, -0.5, 0);
	&usb;
	&pop;

	&push(14.9, 26.9, 0);
	&switch;
	&pop;
}


# ----- Sub-PCB ---------------------------------------------------------------


sub touch
{
	$col = "1,0,0,0.5";
	&poly3d(0.05,
		0, 0,	0, 26,	6, 26,	6, 0);
}


sub battery
{
	$col = "0.5,0.5,0.5,0.8";
	&cyl(-3.2, 10);
}


sub pcb_sub
{
	$col = $col_pcb;
	&poly3d(0.8,
		2, 0,		2, 4.9,		1, 5.9,		0, 5.9,
		0, 30,		18, 30,		20, 28,		20, 2,
		18, 0);

	&push(12, 2, 0.8);
	&touch;
	&pop;

	&push(10, 15, -2.3);
	&battery;
	&pop;
}


# ----- 


sub pcb
{
	&pcb_main;
	&push(44, 0, 3);
	&pcb_sub;
	&pop;
}

#$stl = 1;
#$scad = 1;
#$gp2d = 1;
#$fig = 1;

print "solid foo\n" if $stl;

if ($fig) {
	print "#FIG 3.2\n";
	print "Landscape\nCenter\nMetric\nA4\n";
	print "100.00\nSingle\n-2\n";
	print "1200 2\n"
}

$col = "0.5,0.5,0.5,0.1";
&case;

$col = "0.5, 0.5, 0.5, 0.5";
&push(1.5, 1.5, 3.7);
&pcb;
&pop;

&push(9, 7.05, 6)
&oled;
&pop;
