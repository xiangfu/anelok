#!/usr/bin/perl
#
# web/dwg/sel100.pl - FIG filter
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

$id = shift @ARGV;

undef $skip;
while (<>) {
	if (/^#\s+(\S+)\s*$/) {
		$comment = $1;
		next;
        }
	if (/^6\s/) {
		$keep = $comment eq $id;
		next;
	}
	if (/^-6/) {
		$keep = 0;
		next;
	}
	if (/^\s/) {
		print if $keep && !$skip;
		next;
	}
	if (/^\d/) {
		next unless $keep;
		@a = split(/\s+/, $_);
		$di = $a[0] == 4 ? 3 : 6;
		if ($a[$di] < 100) {
			$skip = 1;
			next;
		} else {
			$skip = 0;
			$a[$di] -= 100;
			print join(" ", @a), "\n";
			next;
		}
	}
	print if $keep;
}
