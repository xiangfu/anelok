#!/usr/bin/perl
#
# web/dwg/filter.pl - FIG filter
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# This filter makes the following changes:
# - make all red (4) polylines of width 1 grey (user color 32)
# - remove all blue (color 1) polygons of width 1
# - remove anything at depth 100 and beyond
#
# The intended markup is as follows:
#
# - the color of lines that are part of an item's outline are manually set to
#   red. A future script can then collect them and generate suitable polygons.
#   These lines are still part of the overview drawing and are thus rendered.
#
# - items whose outline can't be constructed just from polylines need a
#   manually drawn polygon for this purpose. For easy identification, we
#   draw such polygons in blue. They are not part of the visible drawing and
#   are therefore removed by this filter.
#
# - anything at depth 100 or deeper is to be used by other scripts
#


undef $skip;
while (<>) {
	next if /^\s+/ && $skip;
	$skip = 0;
	if (/^2\s+3\s+\d+\s+1\s+1\s+/) {
		$skip = 1;
		next;
	}
	if (/^[0-9]\s/) {
		@a = split(/\s+/, $_);
		$di = $a[0] == 4 ? 3 : 6;	# text
		if ($a[$di] >= 100) {	# depth >= 100
			$skip = 1;
			next;
		}
	}
	s/^(2\s+1\s+\d+\s+1)\s+4(\s+)/\1 32\2/;
	print;
}
