#!/usr/bin/perl

$ann = shift @ARGV;
open(ANN, $ann) || die "$ann: $!";
while (<ANN>) {
	s/#.*//;
	next if /^\s+$/;
	next unless /^(\S+?)[*:]/;
	$prio{$1} = $n++;
}

while (<>) {
	next unless /^(<AREA\s.*href="(.*?)".*)>/;
	next unless defined $prio{$2};
	$area[$prio{$2}] = $1.
	    " onmouseover=\"map_enter('$2')\"".
	    " onmouseout=\"map_leave('$2')\">\n";
}

print join("", @area);
