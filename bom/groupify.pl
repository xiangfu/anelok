#!/usr/bin/perl
#
# bom/groupify.pl - Add group information to BOM entries
#
# Written 2014-2015 by Werner Almesberger
# Copyright 2014-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR "usage: $0 [-g group-assoc-file] [bom-file ...]\n";
	exit(1);
}


sub groups
{
	local ($file) = @_;

	open(FILE, $file) || die "$file: $!";
	while (<FILE>) {
		next if /^#/;
		next if /^\s*$/;

		s/\s*$//;
		@a = split /\s+/;
		$g = shift @a;
		for (@a) {
			die if exists $group{$_};
			$group{$_} = $g;
		}
	}
	close FILE;
}


sub part
{
	local ($qty, $src, $name, $group) = @_;

	if ($name =~ /^([A-Z]+\d+)_/) {
		$group = $group{$1} if $group eq "";
	}
	print "$qty $src $name $group\n";
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-g") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		&groups($ARGV[0]);
	} else {
		&usage;
	}
	shift @ARGV;
}

while (<>) {
	next if /^#/;
	next if /^\s*$/;
	die unless /^(\d)+\s+(\S+)\s+(\S+)\s*(\S*)/;
	&part($1, $2, $3, $4) if $1;
}

