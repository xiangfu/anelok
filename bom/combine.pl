#!/usr/bin/perl
#
# bom/combine.pl - Combine equivalent parts
#
# Written 2014-2015 by Werner Almesberger
# Copyright 2014-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR "usage: $0 [-e file.equiv] [-r] [file.bom ...]\n";
	exit(1);
}


sub equiv
{
	local ($name) = @_;
	local $eq, @eq;

	undef $eq;
	open(EQUIV, $name) || die "$name: $!";
	while (<EQUIV>) {
		next if /^#/;
		next if /^\s*$/;
		die unless /^(\s*)(\S+)\s+(\S+)\s*$/;
		if ($1 eq "") {
			$eq = "$2 $3";
		} else {
			die unless defined $eq;
			push(@{ $equiv{$eq} }, "$2 $3");
			push(@{ $equiv{"$2 $3"} }, $eq);
		}
	}
	close EQUIV;
}


sub find
{
	local ($id) = @_;

	return undef if $tried{$id};
	$tried{$id} = 1;

	return $id if $have{$id};

	for (@{ $equiv{$id}}) {
		my $found = &find($_);
		return $found if defined $found;
	}
	return undef;
}


sub part
{
	local ($qty, $src, $name) = @_;
	my $id = "$src $name";

	return unless $qty;

	undef %tried;

	my $found = &find($id);
	if (defined $found) {
		$id = $found;
	} else {
		$have{$id} = 1;
	}
	$qty{$id} += $qty;
	if ($ref) {
		die unless $name =~ /^([^_]+\d+)_/;
		push(@{ $refs{$id} }, $1);
	}
}


sub compact
{
	local @r = @_;
	my %c;
	my $s = undef;

	for (@r) {
		die unless /^(\S+?)(\d+)$/;
		push(@{ $c{$1} }, $2);
	}
	for (sort keys %c) {
		my @n = sort({ $a <=> $b } @{ $c{$_} });
		my $last = undef;
		my $first = 1;

		$s .= "," if defined $s;
		$s .= $_;	
		for (@n) {
			if ($last eq $_ - 1) {
				$s .= "$last-" unless $s =~ /-$/;
			} else {
				$s .= $last;
				$s .= "," unless $first;
			}
			$last = $_;
			$first = 0;
		}
		$s .= $last;
	}
	return $s;
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-e") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		&equiv($ARGV[0]);
	} elsif ($ARGV[0] eq "-r") {
		$ref = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}

while (<>) {
	next if /^#/;
	next if /^\s*$/;
	die unless /^(\d)+\s+(\S+)\s+(\S+)\s*(\S*)/;
	&part($1, $2, $3, $4) if $1;
}

$first = 1;
for $id (sort keys %qty) {
	print "$qty{$id} $id";
	print " ".&compact(@{ $refs{$id} }) if $ref;
	print "\n";
}
