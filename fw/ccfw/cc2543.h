/*
 * fw/ccfw/cc2543.h - CC2543 register definitions
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CCFW_CC2543_H
#define	CCFW_CC2543_H


enum {
	/* ----- Clock control --------------------------------------------- */

	CLKCONCMD_REG	= 0xc6,	/* Clock Control Command */
		CLKCONCMD_CLKSPD_SHIFT	= 0,	/* Clock speed */
		CLKCONCMD_CLKSPD_MASK	= 7 << CLKCONCMD_CLKSPD_SHIFT,
			CLKCONCMD_CLKSPD_32M	= 0,
			CLKCONCMD_CLKSPD_16M	= 1,
			CLKCONCMD_CLKSPD_8M	= 2,
			CLKCONCMD_CLKSPD_4M	= 3,
			CLKCONCMD_CLKSPD_2M	= 4,
			CLKCONCMD_CLKSPD_1M	= 5,
			CLKCONCMD_CLKSPD_500k	= 6,
			CLKCONCMD_CLKSPD_250k	= 7,
		CLKCONCMD_TICKSPD_SHIFT	= 3,	/* Timer ticks output */
		CLKCONCMD_TICKSPD_MASK	= 7 << CLKCONCMD_TICKSPD_SHIFT,
			CLKCONCMD_TICKSPD_32M	= 0 << 3,
			CLKCONCMD_TICKSPD_16M	= 1 << 3,
			CLKCONCMD_TICKSPD_8M	= 2 << 3,
			CLKCONCMD_TICKSPD_4M	= 3 << 3,
			CLKCONCMD_TICKSPD_2M	= 4 << 3,
			CLKCONCMD_TICKSPD_1M	= 5 << 3,
			CLKCONCMD_TICKSPD_500k	= 6 << 3,
			CLKCONCMD_TICKSPD_250k	= 7 << 3,
		CLKCONCMD_OSC		= 1 << 6,	/* 0: XOSC, 1: RCOSC */
		CLKCONCMD_OSC32K	= 1 << 7,	/* 0: XOSC, 1: RCOSC */
	CLKCONSTA_REG	= 0x9e,	/* Clock Control Status */

	/* ----- I/O Ports ------------------------------------------------- */

	P0_REG		= 0x80,	/* Port 0 */
	P1_REG		= 0x90,	/* Port 1 */
	P2_REG		= 0xa0,	/* Port 2 */
	PERCFG_REG	= 0xf1,	/* Peripheral Control */
		PERCFG_U0CFG_SHIFT	= 0,
		PERCFG_U0CFG_MASK	= 3 << PERCFG_U0CFG_SHIFT,
		PERCFG_I2CCFG		= 1 << 2,
		PERCFG_T4CFG		= 1 << 3,
		PERCFG_T3CFG		= 1 << 4,
		PERCFG_T2CFG		= 1 << 5,
		PERCFG_T1CFG		= 1 << 6,
		PERCFG_PRI0P2_SHIFT	= 5,
		PERCFG_PRI0P2_MASK	= 3 << PERCFG_PRI0P2_SHIFT,
			PERCFG_PRI0P2_U14 = 0 << 5,	/* P2: UART, T1, T4 */
			PERCFG_PRI0P2_14U = 1 << 5,	/* P2: T1, T4, UART */
			PERCFG_PRI0P2_41U = 2 << 5,	/* P2: T4, T1, UART */
	APCFG_REG	= 0xf2,	/* Analog Peripheral I/O Configuration */
	P0SEL_REG	= 0xf3,	/* P0 Function Select */
	P1SEL_REG	= 0xf4,	/* P1 Function Select */
	P2SEL_REG	= 0xf5,	/* P2 Function Select */
	PPRI_REG	= 0xfb,	/* Peripheral Priority Setup */
		PPRI_PRI0P0		= 1 << 0,	/* P0: 0: U,  1: T1 */
		PPRI_PRI1P0		= 1 << 1,	/* P0: 0: I2C, 1: T1 */
		PPRI_PRI0P1		= 1 << 2,	/* P1: 0: U, 1: T1 */
		PPRI_PRI_P1_0_SHIFT	= 3,
		PPRI_PRI_P1_0_MASK	= 3 << PPRI_PRI_P1_0_SHIFT,
			PPRI_PRI_P1_0_134 = 0 << 3,	/* P1_0: T1, T3, T4 */
			PPRI_PRI_P1_0_314 = 1 << 3,	/* P1_0: T3, T1, T4 */
			PPRI_PRI_P1_0_413 = 2 << 3,	/* P1_0: T4, T1, T3 */
		PPRI_PRI_P1_1_SHIFT	= 5,
		PPRI_PRI_P1_1_MASK	= 3 << PPRI_PRI_P1_1_SHIFT,
			PPRI_PRI_P1_1_134 = 0 << 5,	/* P1_1: T1, T3, T4 */
			PPRI_PRI_P1_1_314 = 1 << 5,	/* P1_1: T1, T3, T4 */
			PPRI_PRI_P1_1_413 = 2 << 5,	/* P1_1: T1, T3, T4 */
		PPRI_PRI_P1_2		= 1 << 7,	/* P1_2: 0: T1, 1: T3 */
	P0DIR_REG	= 0xfd,	/* Port 0 Direction Control (1: output) */
	P1DIR_REG	= 0xfe,	/* Port 1 Direction Control (1: output) */
	P2DIR_REG	= 0xff,	/* Port 2 Direction Control (1: output) */
	P0INP_REG	= 0x8f,	/* Port 0 Input Mode (0: pull) */
	P1INP_REG	= 0xf6,	/* Port 1 Input Mode (0: pull) */
	P2INP_REG	= 0xf7,	/* Port 2 Input Mode (0: pull) */
	PPULL_REG	= 0xf8,	/* Port Pullup/Pulldown Control */
		PPULL_PDUP0L	= 1 << 0,	/* P0[3:0] down */
		PPULL_PDUP0H	= 1 << 1,	/* P0[7:4] down */
		PPULL_PDUP1L	= 1 << 2,	/* P1[3] down */
		PPULL_PDUP1H	= 1 << 3,	/* P1[6:4] down */
		PPULL_PDUP2L	= 1 << 4,	/* P2[2:0] down */
	P0IFG_REG	= 0x89,	/* Port 0 Interrupt Status Flag */
	P1IFG_REG	= 0x8a,	/* Port 1 Interrupt Status Flag */
	P2IFG_REG	= 0x8b,	/* Port 2 Interrupt Status Flag */
	PICTL_REG	= 0x8c,
		PICTL_P0ICONL	= 1 << 0,	/* P0[3:0], 0: rise, 1: fall */
		PICTL_P0ICONH	= 1 << 1,	/* P0[7:4], 0: rise, 1: fall */
		PICTL_P1ICONL	= 1 << 2,	/* P1[3:0], 0: rise, 1: fall */
		PICTL_P1ICONH	= 1 << 3,	/* P1[7:4], 0: rise, 1: fall */
		PICTL_P2ICONL	= 1 << 4,	/* P2[3:0], 0: rise, 1: fall */
		PICTL_PADSC	= 1 << 6,	/* Drive strength control */
	P0IEN_REG	= 0xab,	/* Port 0 Interrupt Mask (1: enable) */
	P1IEN_REG	= 0x8d,	/* Port 1 Interrupt Mask (1: enable) */
	P2IEN_REG	= 0xac,	/* Port 2 Interrupt Mask (1: enable) */
	PMUX_REG	= 0xae,	/* Power-Down Signal Mux */
		PMUX_DREGSTAPIN_SHIFT	= 0,	/* Digital Regul. Status Pin */
		PMUX_DREGSTAPIN_MASK	= 7 << PMUX_DREGSTAPIN_SHIFT,
		PMUX_DREGSTA	= 1 << 3,	/* Digital Regulator Status */
		PMUX_CKOPIN_SHIFT	= 4,	/* Clock Out Pin */
		PMUX_CKOPIN_MASK	= 7 << PMUX_CKOPIN_SHIFT,
		PMUX_CKOEN	= 1 << 7,	/* Clock Out Enable */
	OBSSEL0_REG	= 0x6243,	/* Observation output 0 (P1.0) */
		OBBSEL_SEL_SHIFT = 0,
		OBBSEL_SEL_MASK	= 0x7f << OBBSEL_SEL_SHIFT,
			OBBSEL_SEL_0	= 0x7b,	/* rfc_obs_sig0 */
			OBBSEL_SEL_1	= 0x7c,	/* rfc_obs_sig1 */
			OBBSEL_SEL_2	= 0x7d,	/* rfc_obs_sig2 */
		OBSSEL_EN	= 1 << 7,	/* enable */
	OBSSEL1_REG	= 0x6244,	/* Observation output 1 (P1.3) */
	OBSSEL2_REG	= 0x6245,	/* Observation output 2 (P1.4) */
	OBSSEL3_REG	= 0x6246,	/* Observation output 3 (P0.0) */
	OBSSEL4_REG	= 0x6247,	/* Observation output 4 (P0.1) */
	OBSSEL5_REG	= 0x6248,	/* Observation output 5 (P0.2) */

	/* ----- Timer 3 --------------------------------------------------- */

	T3CNT_REG	= 0xca,		/* Timer count byte */
	T3CTL_REG	= 0xcb,		/* Timer 3 Control */
		T3CTL_MODE_SHIFT	= 0,	/* Timer 3 mode */
		T3CTL_MODE_MASK		= 3 << T3CTL_MODE_SHIFT,
			T3CTL_MODE_FREE	= 0,	/* Free-running, 0x00 - 0xff */
			T3CTL_MODE_DOWN	= 1,	/* Down, T3CC0 - 0x00 */
			T3CTL_MODE_MOD	= 2,	/* Modulo, 0x00 - T3CC0 */
			T3CTL_MODE_UPDO	= 3,	/* Up/down, 0x00-T3CC0-0x00 */
		T3CTL_CLR	= 1 << 2,	/* Clear counter */
		T3CTL_OVFIM	= 1 << 3,	/* Overflow interrupt mask */
		T3CTL_START	= 1 << 4,	/* Start timer */
		T3CTL_DIV_SHIFT	= 5,	/* Prescaler divider value */
		T3CTL_DIV_MASK	= 7 << T3CTL_DIV_SHIFT,
			T3CTL_DIV_1	= 0 << 5,	/* Tick / 1 */
			T3CTL_DIV_2	= 1 << 5,
			T3CTL_DIV_4	= 2 << 5,
			T3CTL_DIV_8	= 3 << 5,
			T3CTL_DIV_16	= 4 << 5,
			T3CTL_DIV_32	= 5 << 5,
			T3CTL_DIV_64	= 6 << 5,
			T3CTL_DIV_128	= 7 << 5,
	T3CCTL0_REG	= 0xcc,	/* Timer 3 Channel 0 Capture/Compare Control */
		T3CCTL0_CAP_SHIFT	= 0,	/* Capture mode select */
		T3CCTL0_CAP_MASK	= 3 << T3CCTL0_CAP_SHIFT,
			T3CCTL0_CAP_NONE	= 0,	/* No capture */
			T3CCTL0_CAP_RISE	= 1,	/* Rising edge */
			T3CCTL0_CAP_FALL	= 2,	/* Falling edge */
			T3CCTL0_CAP_BOTH	= 3,	/* Both edges */
		T3CCTL0_MODE		= 1 << 2,	/* Mode (1: compare) */
		T3CCTL0_CMP_SHIFT	= 3,	/* Compare output mode select */
		T3CCTL0_CMP_MASK	= 7 << T3CCTL0_CMP_SHIFT,
			T3CCTL0_CMP_SET_CMP = 0 << 3,	/* Set on compare */
			T3CCTL0_CMP_CLR_CMP = 1 << 3,	/* Clear on compare */
			T3CCTL0_CMP_TOG_CMP = 2 << 3,	/* Toggle on compare */
			T3CCTL0_CMP_UP_0    = 3 << 3,	/* Compare-up to 0 */
			T3CCTL0_CMP_0_UP    = 4 << 3,	/* 0 to compare-up */
			T3CCTL0_CMP_CMP_FF  = 5 << 3,	/* Compare to 0xff */
			T3CCTL0_CMP_0_CMP   = 6 << 3,	/* 0 to compare */
			T3CCTL0_CMP_INIT    = 7 << 3,	/* Initialize pin */
		T3CCTL0_IM		= 1 << 7,	/* Interrupt mask */
	T3CC0_REG	= 0xcd,	/* Timer 3 Channel 0 Capture/Compare Value */
	T3CCTL1_REG	= 0xce,	/* Timer 3 Channel 1 Capture/Compare Control */
		T3CCTL1_CAP_SHIFT	= T3CCTL0_CAP_SHIFT,
		T3CCTL1_CAP_MASK	= T3CCTL0_CAP_MASK,
			T3CCTL1_CAP_NONE	= T3CCTL0_CAP_NONE,
			T3CCTL1_CAP_RISE	= T3CCTL0_CAP_RISE,
			T3CCTL1_CAP_FALL	= T3CCTL0_CAP_FALL,
			T3CCTL1_CAP_BOTH	= T3CCTL0_CAP_BOTH,
		T3CCTL1_MODE		= T3CCTL0_MODE,
		T3CCTL1_CMP_SHIFT	= T3CCTL0_CMP_SHIFT,
		T3CCTL1_CMP_MASK	= T3CCTL0_CMP_MASK,
			T3CCTL1_CMP_SET_CMP	= T3CCTL0_CMP_SET_CMP,
			T3CCTL1_CMP_CLR_CMP	= T3CCTL0_CMP_CLR_CMP,
			T3CCTL1_CMP_TOG_CMP	= T3CCTL0_CMP_TOG_CMP,
			T3CCTL1_CMP_UP_0	= T3CCTL0_CMP_UP_0,
			T3CCTL1_CMP_0_UP	= T3CCTL0_CMP_0_UP,
			T3CCTL1_CMP_CMP_FF	= T3CCTL0_CMP_CMP_FF,
			T3CCTL1_CMP_0_CMP	= T3CCTL0_CMP_0_CMP,
			T3CCTL1_CMP_INIT	= T3CCTL0_CMP_INIT,
		T3CCTL1_IM		= T3CCTL0_IM,
	T3CC1_REG	= 0xcf,	/* Timer 3 Channel 1 Capture/Compare Value */

	/* ----- DMA ------------------------------------------------------- */

	DMAARM_REG	= 0xd6,	/* DMA Channel Arm */
		DMAARM_DMAARM0	= 1 << 0,	/* DMA arm channel 0 */
		DMAARM_DMAARM1	= 1 << 1,	/* DMA arm channel 0 */
		DMAARM_ABORT	= 1 << 7,	/* DMA abort */
	// DMAREQ
	DMA0CFGH_REG	= 0xd5,	/* DMA Channel-0 Configuration Address High */
	DMA0CFGL_REG	= 0xd4,	/* DMA Channel-0 Configuration Address Low */
	DMA1CFGH_REG	= 0xd3,	/* DMA Channel-1 Configuration Address High */
	DMA1CFGL_REG	= 0xd2,	/* DMA Channel-1 Configuration Address Low */
	// DMAIRQ

	/* ----- Flash controller ------------------------------------------ */

	FCTL_REG	= 0x6270,	/* Flash Control */
		FCTL_ERASE	= 1 << 0,	/* Page erase */
		FCTL_WRITE	= 1 << 1,	/* Write */
		FCTL_CM_SHIFT	= 2,		/* Cache mode */
		FCTL_CM_MASK	= 3 << FCTL_CM_SHIFT,
			FCTL_CM_OFF	= 0 << 2,	/* Cache disabled */
			FCTL_CM_ON	= 1 << 2,	/* Cache enabled */
			FCTL_CM_PREFETCH = 2 << 2,	/* prefetch mode */
			FCTL_CM_RT	= 3 << 2,	/* real-time mode */
		FCTL_ABORT	= 1 << 5,	/* Abort status */
		FCTL_FULL	= 1 << 6,	/* Write buffer-full status */
		FCTL_BUSY	= 1 << 7,	/* Write or erase operation */
	FWDATA_REG	= 0x6273,	/* Flash Write Data */
	FADDRH		= 0x6272,	/* Flash-Address High Byte */
	FADDRL		= 0x6271,	/* Flash-Address Low Byte */

	/* ----- USART ----------------------------------------------------- */

	U0CSR_REG	= 0x86,		/* USART 0 Control and Status */
		U0CSR_ACTIVE	= 1 << 0,	/* USART busy */
		U0CSR_TX_BYTE	= 1 << 1,	/* Byte has been transmitted */
		U0CSR_RX_BYTE	= 1 << 2,	/* Received byte ready */
		U0CSR_ERR	= 1 << 3,	/* Parity error */
		U0CSR_FE	= 1 << 4,	/* Framing error */
		U0CSR_SLAVE	= 1 << 5,	/* SPI slave */
		U0CSR_RE	= 1 << 6,	/* Receiver enabled */
		U0CSR_MODE	= 1 << 7,	/* UART mode */
	U0UCR_REG	= 0xc4,		/* USART 0 UART Control */
		U0UCR_START	= 1 << 0,	/* High start bit */
		U0UCR_STOP	= 1 << 1,	/* High stop bit */
		U0UCR_SPB	= 1 << 2,	/* 2 stop bits */
		U0UCR_PARITY	= 1 << 3,	/* Parity enabled */
		U0UCR_BIT9	= 1 << 4,	/* 9-bit transfer */
		U0UCR_D9	= 1 << 5,	/* Even parity */
		U0UCR_FLOW	= 1 << 6,	/* Flow control enabled */
		U0UCR_FLUSH	= 1 << 7,	/* Flush unit */
	U0GCR_REG	= 0xc5,		/* USART 0 Generic Control */
		U0GCR_BAUD_E_SHIFT	= 0,	/* Baud rate exponent */
		U0GCR_BAUD_E_MASK	= 0x1f << U0GCR_BAUD_E_SHIFT,
		U0GCR_ORDER	= 1 << 5,	/* MSB first */
		U0GCR_CPHA	= 1 << 6,	/* SPI clock phase */
		U0GCR_CPOL	= 1 << 7,	/* SPI clock polarity */
	U0DBUF_REG	= 0xc1,		/* Receive/Transmit Data Buffer */
	U0BAUD_REG	= 0xc2,		/* USART 0 Baud-Rate Control */

	/* ----- Power management ------------------------------------------ */

	PCON_REG	= 0x87,		/* Power Mode Control */
		PCON_IDLE	= 1 << 0,	/* enter stop/low-power mode */
	SLEEPCMD_REG	= 0xbe,		/* Sleep-Mode Control Command */
		SLEEPCMD_OSC32K_CALDIS_SHIFT = 7, /* disable 32 k OSC cal */
		SLEEPCMD_OSC32K_CALDIS_MASK = 1 << SLEEPCMD_OSC32K_CALDIS_SHIFT,
		SLEEPCMD_MODE_SHIFT	= 0,	/* Power-mode setting */
		SLEEPCMD_MODE_MASK	= 3 << SLEEPCMD_MODE_SHIFT,
			SLEEPCMD_MODE_ACTIVE	= 0,	/* Active/idle */
			SLEEPCMD_MODE_PM1	= 1,	/* PM1 */
			SLEEPCMD_MODE_PM2	= 2,	/* PM2 */
			SLEEPCMD_MODE_PM3	= 3,	/* PM3 */
};

#endif /* !CCFW_CC2543_H */
