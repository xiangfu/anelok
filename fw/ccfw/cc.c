#include <stdbool.h>
#include <stdint.h>

#include "sfr.h"


/* ----- Clock output ------------------------------------------------------ */


#if 0

/*
 * Enable 32 MHz crystal oscillator and set T3 to output 2 MHz on CH1 aka P1_3.
 */

static void clock_output(void)
{
	CLKCONCMD = CLKCONCMD_OSC32K;
	P1SEL |= 1 << 3;
	T3CTL = T3CTL_CLR;
	T3CCTL1 =
	    T3CCTL1_MODE
	    | T3CCTL1_CMP_TOG_CMP;
	T3CC0 = 0;
	T3CTL =
	    T3CTL_MODE_MOD
	    | T3CTL_CLR
	    | T3CTL_START
	    | T3CTL_DIV_8;
}

#endif


/* ----- Ports setup ------------------------------------------------------- */


static void ports_off(void)
{
	PPULL = 0x1f;	/* all ports pull down */
}


/* ----- Main loop --------------------------------------------------------- */


static void power_down(void)
{
	SLEEPCMD = SLEEPCMD_MODE_PM3 << SLEEPCMD_MODE_SHIFT;
	__asm__(
		"\t.bndry	2\n"
		"\tmov		0x87, #1\n");	/* PCON */
}


void main(void)
{
	ports_off();

	power_down();

	while (1);
}
