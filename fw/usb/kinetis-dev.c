/*
 * fw/usb/kinetis-dev.c - Kinetis USB device
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <string.h>

#include "usb.h"

#include "regs.h"
#include "board.h"
#include "usb-board.h"
#include "kinetis-common.h"


struct ep_descr eps[NUM_EPS];
bool usb_enumerated = 0;

static struct setup_request setup_buf;
static bool in_setup = 0; /* processing a SETUP */


/* ----- Buffer descriptor table ------------------------------------------- */


static void submit_buffer(uint8_t ep, bool tx, void *buf, uint16_t size)
{
	volatile struct BD *bd = curr_bd(ep, tx);

//	DEBUGF("@%p", bd);

	bd->buf = buf;
	bd->flags =
	    BD_OWN | size << BD_BC_SHIFT | (tx && bd_data[ep] ? BD_DATA : 0);
	flip_odd(ep, tx);

	DEBUGF("<%u.%s.%u>", ep, tx ? bd_data[ep] ? "D1" : "D0" : "R", size);

	if (tx)
		bd_data[ep] ^= 1;
}


static bool submit_ep_buffer_tx(struct ep_descr *ep, bool first)
{
	uint8_t n = ep - eps;
	uint16_t size = ep->end - ep->buf;

	if (!size && !first)
		return 0;
	if (size > ep->size)
		size = ep->size;
	submit_buffer(n, 1, ep->buf, size);
	ep->buf += size;
	return 1;
}


static bool submit_ep_buffer_rx(struct ep_descr *ep, bool first)
{
	uint8_t n = ep - eps;
	uint16_t size = ep->end - ep->buf;

	if (!size && !first)
		return 0;
	if (size > ep->size)
		size = ep->size;
	submit_buffer(n, 0, ep->buf, size);
	return 1;
}


static void submit_setup(void)
{
	submit_buffer(0, 0, &setup_buf, 8);
	in_setup = 0;
}


/* ----- Reset and initialization ------------------------------------------ */


static void setup_device(void)
{
	USB0_CTL = USB_CTL_USBENSOFEN_MASK;	/* enable module */

	USB0_CONTROL = USB_CONTROL_DPPULLUPNONOTG_MASK;
		/* internal pull-up */
	USB0_USBCTRL = 0;	/* disable suspend and pull-downs */
	USB0_CONTROL = USB_CONTROL_DPPULLUPNONOTG_MASK;
		/* enable pull-up in non-OTG */
}


static void device_reset(void)
{
	uint8_t i;

	kinetis_reset_module();
	kinetis_setup_module();
	setup_device();

	submit_setup();

	for (i = 0; i != NUM_EPS; i++)
		eps[i].state = EP_IDLE;
	eps[0].size = EP0_SIZE;
	USB0_ENDPT0 = USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPRXEN_MASK |
	    USB_ENDPT_EPHSHK_MASK;

#if NUM_EPS > 1
	eps[1].size = EP1_SIZE;
	USB0_ENDPT(1) =
	    USB_ENDPT_EPCTLDIS_MASK	/* disable SETUP on EP1 */
	    | USB_ENDPT_EPTXEN_MASK	/* enable TX */
	    | USB_ENDPT_EPHSHK_MASK;	/* enable handshake */
#endif
}


/* ----- Device address ---------------------------------------------------- */


static uint8_t next_addr;


static void enable_addr(void *user)
{
	DEBUGF("!");

	USB0_ADDR = next_addr;
	usb_enumerated = 1;
}


void set_addr(uint8_t addr)
{
	DEBUGF("A");

	next_addr = addr;
	usb_send(&eps[0], NULL, 0, enable_addr, NULL);
}


/* ----- Control transfers ------------------------------------------------- */


static bool ep_setup(void)
{
	volatile struct BD *bd = curr_bd(0, 0);

	/* something went wrong with the previous SETUP. try to patch it up. */
	if (in_setup)
		memcpy(&setup_buf, bd->buf, 8);

#ifdef DEBUG
	{
		uint8_t i;

		DEBUGF("[");
		for (i = 0; i != 8; i++)
			DEBUGF("%02x", ((const uint8_t *) &setup_buf)[i]);
		DEBUGF("]");
	}
#endif

	bd_data[0] = 1;
//flip_odd(0, 1);
	if (!handle_setup(&setup_buf))
		goto fail;
	in_setup = 1;
	if (eps[0].state != EP_IDLE)
		return 1;
	if (!(setup_buf.bmRequestType & 0x80))	/* FROM_* transfer */
		usb_send(&eps[0], NULL, 0, NULL, NULL);
	return 1;

fail:
	DEBUGF(":-(");

	submit_setup();
	return 0;
}


/* ----- Data transfer ----------------------------------------------------- */


void usb_ep_change(struct ep_descr *ep)
{
	if (ep->state == EP_TX)
		submit_ep_buffer_tx(ep, 1);
	else
		submit_ep_buffer_rx(ep, 1);
}


static bool ep_rx(struct ep_descr *ep, uint8_t len)
{
	ep->buf += len;
	if (len == ep->size && submit_ep_buffer_rx(ep, 0))
		return 1;
	ep->state = EP_IDLE;

	if (ep->callback)
		ep->callback(ep->user);

	/* OUT after IN */
	if (in_setup && ep == eps) {
		submit_buffer(0, 1, &setup_buf, 0);
		return 1;
	}

	return 1;
}


static void ep_tx(struct ep_descr *ep)
{
	if (submit_ep_buffer_tx(ep, 0))
		return;
	ep->state = EP_IDLE;

	/* IN after OUT */
	if (in_setup && ep == eps)
		submit_buffer(0, 0, &setup_buf, sizeof(setup_buf));
}


/* ----- Interrupt dispatcher ---------------------------------------------- */


static void token_done(void)
{
	uint8_t stat, n;
	bool tx, odd;
	struct ep_descr *ep;
	volatile struct BD *bd;
	bool ok;

	stat = USB0_STAT;

	n = (stat & USB_STAT_ENDP_MASK) >> USB_STAT_ENDP_SHIFT;
	tx = stat & USB_STAT_TX_MASK;
	odd = stat & USB_STAT_ODD_MASK;
	ep = eps + n;
	bd = &BDT[n][tx][odd];

	DEBUGF("<%u.%u.%u>", n, tx, odd);

	switch ((bd->flags & BD_PID_MASK) >> BD_PID_SHIFT) {
	case PID_SETUP:
		DEBUGF("S");

		ep->state = EP_IDLE;
		ok = ep_setup();
		USB0_CTL = USB_CTL_USBENSOFEN_MASK;
		    /* resume token processing */
		if (!ok)
			goto stall;
		break;
	case PID_OUT:
		DEBUGF("O");

		if (!n && ep->state == EP_IDLE && in_setup) {
			submit_setup();
			break;
		}
		if (ep->state != EP_RX)
			goto stall;
		if (!ep_rx(ep, (bd->flags & BD_BC_MASK) >> BD_BC_SHIFT))
			goto stall;
		break;
	case PID_IN:
		DEBUGF("I");

		if (!n && ep->state == EP_IDLE && in_setup) {
			submit_setup();
			break;
		}
		if (ep->state == EP_TX) {
			ep_tx(ep);
			if (ep->state == EP_IDLE && ep->callback)
				ep->callback(ep->user);
		}
		break;
	default:
		DEBUGF("?");
		break;
	}
	return;

stall:
	; /* @@@ to do */
}


void usb_poll_device(void)
{
	uint8_t irq;

	while (1) {
		irq = USB0_ISTAT;
		if (!irq)
			break;
		if (irq & USB_ISTAT_TOKDNE_MASK) {
			DEBUGF("T");

			token_done();
		}
		if (irq & USB_ISTAT_USBRST_MASK) {
			DEBUGF("R");

			console_update();

			if (user_reset)
				user_reset();

			device_reset();
			continue;
		}
		if (irq & USB_ISTAT_ERROR_MASK) {
			DEBUGF("E%02x", USB0_ERRSTAT);

			/* @@@ count errors ? */
			USB0_ERRSTAT = USB0_ERRSTAT;
		}
		USB0_ISTAT = irq;
	}
}


/* ----- Interrupt handler ------------------------------------------------- */


void usb_isr(void)
{
	usb_poll_device();
	NVIC_ClearPendingIRQ(USB0_IRQn);
}


void usb_device_interrupts(bool enable)
{
	if (enable) {
		NVIC_ClearPendingIRQ(USB0_IRQn);
		NVIC_EnableIRQ(USB0_IRQn);
		USB0_INTEN =
		    USB_INTEN_TOKDNEEN_MASK
		    | USB_INTEN_USBRSTEN_MASK
		    | USB_INTEN_ERROREN_MASK;
	} else {
		NVIC_DisableIRQ(USB0_IRQn);
		USB0_INTEN = 0;
	}
}


/* ----- Configuration ----------------------------------------------------- */


void usb_enable_bus_reset(void)
{
	/* @@@ abstract */
}


void usb_begin_device(void)
{
	usb_board_begin();

	kinetis_reset_module();
	kinetis_setup_module();
	setup_device();
}


void usb_end_device(void)
{
	USB0_CONTROL = 0;	/* disable pull-up */
	kinetis_end();
}
