/*
 * fw/usb/host.h - USB host
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef USB_HOST_H
#define	USB_HOST_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>


struct host_ep {
	uint8_t addr;
	uint8_t n;
	uint8_t size;
	bool toggle;	/* data toggle */
};


struct urb {
	struct host_ep *ep;
	uint8_t pid;		/* PID.nPID */
	const struct setup_request *setup;
				/* must be NULL if not a setup */
	void *buf;
	uint16_t len;		/* in/out */
	uint16_t done;		/* size transferred */
	void (*fn)(struct urb *urb, bool ok);

	/* fields below are for use by the driver */
	struct urb *next;
};


/* ----- Driver functions -------------------------------------------------- */


void usb_submit_urb(struct urb *urb);
bool usb_cancel_urb(struct urb *urb);


/* ----- User API ---------------------------------------------------------- */


static inline void usb_fill_setup(struct setup_request *setup,
    uint8_t type, uint8_t req, uint16_t value, uint16_t index, uint16_t len)
{
	setup->bmRequestType = type;
	setup->bRequest = req;
	setup->wValue = value;
	setup->wIndex = index;
	setup->wLength = len;
}


static inline void usb_setup_urb(struct urb *urb, struct host_ep *ep,
    const struct setup_request *setup, void *buf, uint16_t len,
    void (*fn)(struct urb *urb, bool ok))
{
	ep->toggle = 0;
	urb->ep = ep;
	urb->pid = PID_SETUP;
	urb->setup = setup;
	urb->buf = buf;
	urb->len = len;
	urb->fn = fn;
}


static inline void usb_data_urb(struct urb *urb, struct host_ep *ep,
    void *buf, uint16_t len, bool out, void (*fn)(struct urb *urb, bool ok))
{
	urb->ep = ep;
	urb->pid = out ? PID_OUT : PID_IN;
	urb->setup = NULL;
	urb->buf = buf;
	urb->len = len;
	urb->fn = fn;
}


/* ----- Device-independent functions -------------------------------------- */


void usb_attach(void);
void usb_detach(void);

#endif /* !USB_HOST_H */
