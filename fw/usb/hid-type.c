/*
 * fw/usb/hid-type.c - Type strings on HID
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "hid.h"
#include "hid-type.h"


#define	SHIFT	(1 << (HID_MOD_LEFT_SHIFT + 8))


/* US keyboard layout */

static const uint16_t ascii_to_hid[] = {
	/* 0 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,

	/* 4 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,

	/* 8 */
	HID_KEY_DELETE,
	HID_KEY_TAB,
	HID_KEY_ENTER,
	HID_KEY_UNDEFINED,

	/* 12 */
	HID_KEY_UNDEFINED,
	HID_KEY_ENTER,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,

	/* 16 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	
	/* 20 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,

	/* 24 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_ESCAPE,

	/* 28 */
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,
	HID_KEY_UNDEFINED,

	/* 32 */
	HID_KEY_SPACE,
	HID_KEY_1		| SHIFT,
	HID_KEY_APOSTROPHE	| SHIFT,
	HID_KEY_3		| SHIFT,

	/* 36 */
	HID_KEY_4		| SHIFT,
	HID_KEY_5		| SHIFT,
	HID_KEY_7		| SHIFT,
	HID_KEY_APOSTROPHE,

	/* 40 */
	HID_KEY_9		| SHIFT,
	HID_KEY_0		| SHIFT,
	HID_KEY_8		| SHIFT,
	HID_KEY_EQUAL		| SHIFT,

	/* 44 */
	HID_KEY_COMMA,
	HID_KEY_MINUS,
	HID_KEY_PERIOD,
	HID_KEY_SLASH,

	/* 48 */
	HID_KEY_0,
	HID_KEY_1,
	HID_KEY_2,
	HID_KEY_3,

	/* 52 */
	HID_KEY_4,
	HID_KEY_5,
	HID_KEY_6,
	HID_KEY_7,

	/* 56 */
	HID_KEY_8,
	HID_KEY_9,
	HID_KEY_SEMICOLON	| SHIFT,
	HID_KEY_SEMICOLON,

	/* 60 */
	HID_KEY_COMMA		| SHIFT,
	HID_KEY_EQUAL,
	HID_KEY_PERIOD		| SHIFT,
	HID_KEY_SLASH		| SHIFT,

	/* 64 */
	HID_KEY_2		| SHIFT,
	HID_KEY_A		| SHIFT,
	HID_KEY_B		| SHIFT,
	HID_KEY_C		| SHIFT,

	/* 68 */
	HID_KEY_D		| SHIFT,
	HID_KEY_E		| SHIFT,
	HID_KEY_F		| SHIFT,
	HID_KEY_G		| SHIFT,

	/* 72 */
	HID_KEY_H		| SHIFT,
	HID_KEY_I		| SHIFT,
	HID_KEY_J		| SHIFT,
	HID_KEY_L		| SHIFT,

	/* 76 */
	HID_KEY_K		| SHIFT,
	HID_KEY_M		| SHIFT,
	HID_KEY_N		| SHIFT,
	HID_KEY_O		| SHIFT,

	/* 80 */
	HID_KEY_P		| SHIFT,
	HID_KEY_Q		| SHIFT,
	HID_KEY_R		| SHIFT,
	HID_KEY_S		| SHIFT,

	/* 84 */
	HID_KEY_T		| SHIFT,
	HID_KEY_U		| SHIFT,
	HID_KEY_V		| SHIFT,
	HID_KEY_W		| SHIFT,

	/* 88 */
	HID_KEY_X		| SHIFT,
	HID_KEY_Y		| SHIFT,
	HID_KEY_Z		| SHIFT,
	HID_KEY_BRACKET_OPEN,

	/* 92 */
	HID_KEY_BACKSLASH,
	HID_KEY_BRACKET_CLOSE,
	HID_KEY_6		| SHIFT,
	HID_KEY_MINUS		| SHIFT,

	/* 96 */
	HID_KEY_GRAVE,
	HID_KEY_A,
	HID_KEY_B,
	HID_KEY_C,

	/* 100 */
	HID_KEY_D,
	HID_KEY_E,
	HID_KEY_F,
	HID_KEY_G,

	/* 104 */
	HID_KEY_H,
	HID_KEY_I,
	HID_KEY_J,
	HID_KEY_K,

	/* 108 */
	HID_KEY_L,
	HID_KEY_M,
	HID_KEY_N,
	HID_KEY_O,

	/* 112 */
	HID_KEY_P,
	HID_KEY_Q,
	HID_KEY_R,
	HID_KEY_S,

	/* 116 */
	HID_KEY_T,
	HID_KEY_U,
	HID_KEY_V,
	HID_KEY_W,

	/* 120 */
	HID_KEY_X,
	HID_KEY_Y,
	HID_KEY_Z,
	HID_KEY_BRACKET_OPEN	| SHIFT,

	/* 124 */
	HID_KEY_BACKSLASH	| SHIFT,
	HID_KEY_BRACKET_CLOSE	| SHIFT,
	HID_KEY_GRAVE		| SHIFT,
	HID_KEY_DELETE,
};


static bool translate(uint8_t c, uint8_t *mod, uint8_t *key)
{
	if (c & 0x80)
		return 0;
	*mod = ascii_to_hid[c] >> 8;
	*key = ascii_to_hid[c];
	return 1;
}


static void hid_type_next(void *user)
{
	struct hid_type_ctx *ctx = user;

	if (!*ctx->s) {
		if (ctx->fn)
			ctx->fn(ctx->user);
		return;
	}
	if (!translate(*ctx->s++, &ctx->req.modifiers, &ctx->req.key)) {
		ctx->req.modifiers = 0;
		ctx->req.key = HID_KEY_UNDEFINED;
	}
	hid_press_key(&ctx->req);
}


void hid_type(struct hid_type_ctx *ctx, const char *s,
    void (*fn)(void *user), void *user)
{
	ctx->fn = fn;
	ctx->user = user;
	ctx->s = s;

	ctx->req.done = hid_type_next;
	ctx->req.user = ctx;

	hid_type_next(ctx);
}
