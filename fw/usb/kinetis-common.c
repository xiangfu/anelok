/*
 * fw/usb/kinetis-common.c - Kinetis USB, shared device and host code
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "regs.h"
#include "board.h"
#include "usb.h"
#include "usb-board.h"
#include "kinetis-common.h"


/* ----- Buffer descriptor table ------------------------------------------- */


volatile struct BD BDT[NUM_EPS][2][2] __attribute__((aligned(512)));
bool bd_odd[NUM_EPS][2];
bool bd_data[NUM_EPS];


/* ----- Reset and initialization ------------------------------------------ */


/*
 * According to errata e5928, we can't use
 * USB0_USBTRC0 = USB_USBTRC0_USBRESET_MASK
 * So instead, we set registers to their reset values.
 */

void kinetis_reset_module(void)
{
	uint8_t i;

	USB0_ADDR = 0;
	usb_enumerated = 0;

	for (i = 0; i != 16; i++)
		USB0_ENDPT(i) = 0;
	memset(bd_odd, 0, sizeof(bd_odd));
	USB0_CTL |= USB_CTL_ODDRST_MASK; /* reset ping/pong fields */

	USB0_ISTAT = 0xff;
	USB0_ERRSTAT = 0xff;
}


void kinetis_setup_module(void)
{
	memset(BDT, 0, sizeof(BDT));
	USB0_BDTPAGE3 = (uint32_t) BDT >> 24;
	USB0_BDTPAGE2 = (uint32_t) BDT >> 16;
	USB0_BDTPAGE1 = (uint32_t) BDT >> 8;

	memset(bd_data, 0, sizeof(bd_data));
}


void kinetis_end(void)
{
	kinetis_reset_module();

	USB0_CTL = 0;	/* disable module */

	usb_board_end();
}


void usb_init(void)
{
	usb_board_init();
}
