/*
 * fw/usb-board.h - Board-specific USB functions
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef USB_BOARD_H
#define	USB_BOARD_H

#include <stdbool.h>


extern bool usb_enumerated;


bool usb_sof(void);
bool usb_a(void);

void usb_board_begin(void);
void usb_board_end(void);
void usb_board_init(void);

void usb_poll_device(void);
void usb_poll_host(void);

void usb_device_interrupts(bool enable);

/* @@@ move them to USB stack later */

void usb_begin_device(void);
void usb_end_device(void);
void usb_begin_host(void);
void usb_end_host(void);

#endif /* !USB_BOARD_H */
