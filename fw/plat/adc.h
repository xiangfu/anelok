/*
 * fw/adc.h - ADC
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ADC_H
#define	ADC_H

#include <stdint.h>


uint16_t adc_sys_mv(void);
uint16_t adc_temp_mC(uint16_t *vref_mV);
uint16_t adc_temp_raw(void);

bool adc_begin(void);
void adc_end(void);

#endif /* !ADC_H */
