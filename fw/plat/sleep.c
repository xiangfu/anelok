/*
 * fw/plat/sleep.c - Sleep and wakeup
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 20l3-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "regs.h"
#include "irq.h"
#include "extint.h"
#include "board.h"
#include "sleep.h"


volatile bool allow_stop = 1;


/*
 * @@@ Known bug: we now have multiple wakeup sources: internal modules (either
 * LPTMR or TSI) and a GPIO. If waking due to a GPIO change while waiting for
 * TSI, we may get a bad sample.
 *
 * To fix this, the TSI code should explicitly wait for ESOF.
 */


void llwu_isr(void)
{
	/*
	 * The manual (15.1.2) says the LLWU gets "immediately disabled" when
	 * leaving LLS, but this doesn't seem to include its de-asserting its
	 * interrupt.
	 */
	LLWU_ME = 0;

	extint_llwu(LLWU_F1 | LLWU_F2 << 8);
	LLWU_F1 = 0xff;
	LLWU_F2 = 0xff;

	NVIC_ClearPendingIRQ(LLW_IRQn);
}


void enter_stop(uint8_t mask)
{
	if (!allow_stop) {
		regular_sleep();
		return;
	}

	SMC_PMPROT = SMC_PMPROT_ALLS_MASK;
	SMC_PMCTRL = SMC_PMCTRL_STOPM(3);	/* enter LLS mode */

	NVIC_EnableIRQ(LLW_IRQn);
	NVIC_ClearPendingIRQ(LLW_IRQn);		/* just in case */
	LLWU_ME = mask;

//	SMC_PMCTRL = SMC_PMCTRL_STOPM(0);	/* enter STOP mode */
	(void) SMC_PMCTRL;	/* make sure the write has finished */

	deep_sleep();
}
