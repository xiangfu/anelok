/*
 * fw/tick.h - System ticks (about 1 ms) for inexact timekeeping
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 20l3-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TICK_H
#define	TICK_H

#include <stdint.h>


uint16_t tick_now(void);
uint16_t tick_delta(uint16_t *t);
void msleep(uint32_t ms);
void tick_init(void);

#endif /* !TICK_H */
