/*
 * fw/plat/sleep.h - Sleep and wakeup
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 20l3-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SLEEP_H
#define	SLEEP_H

#include <stdbool.h>
#include <stdint.h>

#include "regs.h"


extern volatile bool allow_stop;


static inline void deep_sleep(void)
{
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
	(void) SCB->SCR;	/* make sure the write has finished */

	asm("wfi");
}


static inline void regular_sleep(void)
{
	SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
	(void) SCB->SCR;	/* make sure the write has finished */

	asm("wfi");
}


void enter_stop(uint8_t mask);

#endif /* !SLEEP_H */
