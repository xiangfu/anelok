/*
 * fw/plat/board.c - Board-specific functions for Anelok
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "misc.h"
#include "irq.h"
#include "led.h"
#include "qa.h"


void panic(void)
{
	irq_disable();
	led_init();
	while (1) {
		led(1);
		mdelay(250);
		led(0);
		mdelay(250);
	}
}
