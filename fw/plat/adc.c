/*
 * fw/adc.h - ADC
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "regs.h"
#include "adc.h"


static inline uint16_t do_adc(uint8_t ch)
{
	ADC0_SC1A = ch;

	while (!(ADC0_SC1A & ADC_SC1_COCO_MASK));

	return ADC0_RA;
}


uint16_t adc_sys_mv(void)
{
	/*
	 * adc = 65535*(1.0V / Vsys)
	 * Vsys = 65535 / adc;
	 */
	return 65535000 / do_adc(0x1b); /* bandgap */
}


/*
 * V(25C) = 719 mV
 * m = 1.715 mV/K
 *
 * temp = 25 C - (Vtemp - V(25C)) / m
 *
 * adc_t = 65535*(Vtemp / Vsys)
 * Vtemp = adc_t*Vsys / 65535
 *
 * adc_v = 65535*(1.0V / Vsys)
 * Vsys = 65535 / adc_v
 *
 * Vtemp = adc_t/adc_v  (multiplied with Vbandgap)
 */

uint16_t adc_temp_mC(uint16_t *vref_mV)
{
	uint16_t adc_v, adc_t;
	int32_t v_temp;
	int16_t d_mK;

	/*
	 * Bandgap voltage
	 *
	 * Range:
	 *   0 -> VDDA infinite
	 *   65535 -> VDDA = 1 V
	 * Resolution (res):
	 *   at VDDA = 1.8 V: 0.049 mV
	 *   at VDDA = 3.3 V: 0.166 mV
	 *
	 * Calculations (for bc):
	 * adc = 65535/1.8
	 * adc = 65535/3.3
	 * res = (65535/adc-65535/(adc+1))*1000
	 */

	adc_v = do_adc(0x1b);	/* bandgap */

	/*
	 * Temperature sensor
	 *
	 * Range (t):
	 *   0 -> 0V = 444.24 C
	 *   65535, = VDDA = 1.8 V -> -605.32 C
	 *   65535, = VDDA = 3.3 V -> -1479.96 C
	 * Resolution (r):
	 *   at VDDA = 1.8 V: 0.016 C
	 *   at VDDA = 3.3 V: 0.029 C
	 *
	 * Calculations (for bc):
	 * t = 25-(-719/1.715)
	 * t = 25-(1800-719)/1.715
	 * t = 25-(3300-719)/1.715
	 * r = (1800/65535)/1.715
	 * r = (3300/65535)/1.715
	 */

	adc_t = do_adc(0x1a);	/* temperature sensor */

	if (vref_mV)
		*vref_mV = 65535000 / adc_v;

	/*
	 * Scale by 1e5:
	 *   min = 0
	 *   max = 2^32-1 = (adc_t = 42949)
	 *     at VDDA = 1.8 V -> -243.60 C
	 *     at VDDA = 3.3 V -> -816.80 C
	 *
	 * Calculations (for bc):
	 * t = 25-(1800/65535*42949-719)/1.715
	 * t = 25-(3300/65535*42949-719)/1.715
	 *
	 * Division by adc_v, quantization error (q):
	 * q at VDDA = 1.8 V, t = 25 V: 0.32 uK
	 * q at VDDA = 3.3 V, t = 25 V: 1.06 uK
	 *
	 * Calculation:
	 * adc = 65535/1.8
	 * adc = 65535/3.3
	 * q = (719/1.715)*(1/adc-1/(adc+1))*10^6
	 */

	v_temp = 100000 * adc_t / adc_v; /* in V/1e5 */

	/*
	 * Scale by 1e4:
	 *   min = 0
	 *   max = 2^31-1
	 *     at VDDA = 1.8 V, v_temp comes from X / adc where adc = 36408
	 *     and the scaling therefore does not constrain the range.
	 *
	 * Output range:
	 *   25 C +/- 32.7 K
	 *
 	 * Calculation:
	 *   adc = 65535/1.8
	 */

	d_mK = (v_temp * 10000 - 719000000) / 1715;

	/*
	 * The device can operate in the range -40 to 105 C, but this function
	 * only supports reporting 0 to about 57.7 C.
	 */
	if (d_mK > 25000)
		return 0;

	return 25000 - d_mK;
}


uint16_t adc_temp_raw(void)
{
	return do_adc(0x1a);	/* temperature sensor */
}


/*
 * Section 28.4.6
 */

static bool calibrate(void)
{
	uint16_t tmp;

	/*
	 * tADCK(<16 bit) = 2 ... 12 MHz
	 * tADCK(calibration) <= 4 MHz
	 *
	 * ADIV = 2: clock / 4 (~2.5-3 MHz)
	 * ADLSMP = 0: short sample time
	 * MODE = 3: 16 bit conversion
	 * ADICLK = 1: use bus clock/2 (~10-12 MHz)
	 */
	ADC0_CFG1 = ADC_CFG1_MODE(3) | ADC_CFG1_ADICLK(1) | ADC_CFG1_ADIV(2);

	/*
	 * CAL = 1: perform calibration
	 * AVGE = 1: use hardware averaging
	 * AVGS = 3: 32 samples averaged
	 */
	ADC0_SC3 = ADC_SC3_CAL_MASK | ADC_SC3_AVGE_MASK | ADC_SC3_AVGS(3);

	while (ADC0_SC3 & ADC_SC3_CAL_MASK);

	if (ADC0_SC3 & ADC_SC3_CALF_MASK)
		return 0;

	tmp = ADC0_CLP0 + ADC0_CLP1 + ADC0_CLP2 + ADC0_CLP3 + ADC0_CLP4 +
	    ADC0_CLPS;
	ADC0_PG = (tmp >> 1) | 0x8000;

	tmp = ADC0_CLM0 + ADC0_CLM1 + ADC0_CLM2 + ADC0_CLM3 + ADC0_CLM4 +
	    ADC0_CLMS;
	ADC0_MG = (tmp >> 1) | 0x8000;

	return 1;
}


bool adc_begin(void)
{
	SIM_SCGC6 |= SIM_SCGC6_ADC0_MASK;	/* enable ADC clock */

	if (!calibrate()) {
		adc_end();
		return 0;
	}

	PMC_REGSC = PMC_REGSC_BGBE_MASK;	/* enable bandgap */

	/*
	 * fADCK(<= 13 bit) = 1 ... 18 MHz
	 * fADCK(<16 bit) = 2 ... 12 MHz
	 *
	 * ADVI = 0: use input clock (undivided)
	 * ADLSMP = 0: short sample time
	 * MODE = 3: 16 bit conversion
	 * ADICLK = 1: use bus clock/2 (~10-12 MHz)
	 */
	ADC0_CFG1 = ADC_CFG1_MODE(3) | ADC_CFG1_ADICLK(1);

	return 1;
}


void adc_end(void)
{
	PMC_REGSC = 0;		/* disable bandgap */
	ADC0_SC1A = 0x1f;	/* disable module */
	SIM_SCGC6 &= ~SIM_SCGC6_ADC0_MASK;
}
