/*
 * fw/flash.h - Board-specific flash functions
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef FLASH_H
#define	FLASH_H

#include "dfu.h"


extern struct dfu_flash_ops plat_flash_ops; /* KL2x */
extern struct dfu_flash_ops cc_flash_ops;


void flash_init(void);

#endif /* !FLASH_H */
