/*
 * fw/common/power.h - Power management (3.3V rail)
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef POWER_H
#define	POWER_H

#include <stdbool.h>


void power_3v3(bool on);
void power_boost_init(void);

void power_disp(bool on);
void power_card(bool on);
void power_init(void);

void power_3v3_force_on(void);
void power_disp_force_on(void);

#endif /* !POWER_H */
