/*
 * fw/list.h - Doubly linked lists
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Inspired by include/linux/list.h
 */


#ifndef LIST_H
#define	LIST_H

#include <stddef.h>
#include <stdbool.h>


struct list {
	struct list *prev;
	struct list *next;
};


#define	LIST(name)	struct list name = { &name, &name }


static inline void list_init(struct list *h)
{
	h->next = h->prev = h;
}


static inline void list_append(struct list *h, struct list *e)
{
	e->next = h;
	e->prev = h->prev;
	h->prev->next = e;
	h->prev = e;
	
}


static inline void list_prepend(struct list *h, struct list *e)
{
	e->next = h->next;
	e->prev = h;
	h->next->prev = e;
	h->next = e;
}


static inline bool list_empty(struct list *h)
{
	return h->next == h;
}


static inline struct list *list_next(struct list *h)
{
	return h->next == h ? NULL : h->next;
}


static inline void list_del(struct list *e)
{
	e->prev->next = e->next;
	e->next->prev = e->prev;
}


static inline void list_del_init(struct list *e)
{
	list_del(e);
	list_init(e);
}

#endif /* !LIST_H */
