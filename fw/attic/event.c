/*
 * fw/event.c - Event handling
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "irq.h"
#include "list.h"
#include "event.h"


static LIST(events);


bool event_queue_first(struct event *ev)
{
	unsigned long flags;
	bool queued;

	flags = irq_save();
	queued = list_empty(&ev->list);
	if (queued)
		list_prepend(&events, &ev->list);
	irq_restore(flags);
	return queued;
}


bool event_queue_last(struct event *ev)
{
	unsigned long flags;
	bool queued;

	flags = irq_save();
	queued = list_empty(&ev->list);
	if (queued)
		list_append(&events, &ev->list);
	irq_restore(flags);
	return queued;
}


bool event_dequeue(struct event *ev)
{
	unsigned long flags;
	bool dequeued;

	flags = irq_save();
	dequeued = !list_empty(&ev->list);
	if (dequeued)
		list_del(&ev->list);
	irq_restore(flags);
	return dequeued;
}


struct event *event_poll(void)
{
	unsigned long flags;
	struct event *ev;

	flags = irq_save();
	ev = (struct event *) list_next(&events);
	if (ev)
		list_del_init(&ev->list);
	irq_restore(flags);
	return ev;
}
