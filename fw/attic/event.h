/*
 * fw/event.h - Event handling
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EVENT_H
#define	EVENT_H

#include <stdbool.h>

#include "list.h"


struct event {
	struct list list;
	void (*fn)(void *user);
	void *user;
};


#define	EVENT(name)	struct event name = { { &name.list, &name.list } };


bool event_queue_first(struct event *ev);
bool event_queue_last(struct event *ev);
bool event_dequeue(struct event *ev);
struct event *event_poll(void);

#endif /* !EVENT_H */
