/*
 * fw/wheel.h - Wheel driver
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef WHEEL_H
#define	WHEEL_H

#include <stdbool.h>
#include <stdint.h>


bool wheel_button(void);
int8_t wheel_rot(void);
void wheel_init(void);

#endif /* !WHEEL_H */
