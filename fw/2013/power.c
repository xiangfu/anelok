/*
 * fw/power.c - Power management (3.3 V rail)
 *
 * Written 2013, 2014 by Werner Almesberger
 * Copyright 2013, 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "gpio.h"
#include "power.h"


/* ----- Display power ----------------------------------------------------- */


void power_disp(bool on)
{
	power_3v3(on);
}


/* ----- Memory card power ------------------------------------------------- */


void power_card(bool on)
{
	static uint8_t count;

	gpio_begin(CARD_OFF);
	if (on) {
		if (!count++) {
			power_3v3(1);
			gpio_clr(CARD_OFF);
		}
	} else {
		if (!--count) {
			gpio_set(CARD_OFF);
			power_3v3(0);
		}
	}
	gpio_end(CARD_OFF);
}


/* ----- Initialize power distribution ------------------------------------- */


void power_init(void)
{
	power_boost_init();

	/* disable memory card power  */
	gpio_init_out(CARD_OFF, 1);
}
