/*
 * fw/2013/usb-board.c - Board-specific USB functions
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "regs.h"
#include "board.h"
#include "rf.h"
#include "clock.h"
#include "usb-board.h"


void usb_board_begin(void)
{
	rf_start(); /* activate 2 MHz clock */
	/* configure RF_CLK as EXTAL0 input (the "off" doesn't apply here) */
        gpio_init_off(RF_CLK);

	clock_external();

	/*
	 * PLLFLLSEL = 1: PLL clock (96 MHz) divided by two (already set)
	 * USBSRC = 1: use PLL/2 clock
	 */
	SIM_SOPT2 |= SIM_SOPT2_USBSRC_MASK;

	SIM_SCGC4 |= SIM_SCGC4_USBOTG_MASK;	/* enable USB clock */
}


void usb_board_end(void)
{
	SIM_SCGC4 &= ~SIM_SCGC4_USBOTG_MASK;	/* disable USB clock */

	clock_internal();
}
