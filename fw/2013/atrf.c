/*
 * fw/rf.c - RF transceiver
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "../../ben-wpan/atusb/fw/include/at86rf230.h"

#include "misc.h"
#include "board.h"
#include "gpio.h"
#include "rf.h"
#include "fmt.h"


/* ----- Interface control ------------------------------------------------- */


static void rf_reset(void)
{
	gpio_begin(RF_nRST);
	gpio_clr(RF_nRST);
	mdelay(1);
	gpio_set(RF_nRST);
	gpio_end(RF_nRST);
	mdelay(1);
}



static void slp_tr(bool on)
{
	gpio_begin(RF_SLP_TR);
	if (on)
		gpio_set(RF_SLP_TR);
	else
		gpio_clr(RF_SLP_TR);
	gpio_end(RF_SLP_TR);
}


static void rf_begin(void)
{
	gpio_begin(RF_MOSI);
	gpio_begin(RF_SCLK);
	gpio_begin(RF_MISO);
	gpio_begin(RF_nSS);
	gpio_init_in(RF_MISO, 0);
}


static void rf_end(void)
{
	gpio_init_off(RF_MISO);
	gpio_end(RF_MOSI);
	gpio_end(RF_MISO);
	gpio_end(RF_SCLK);
	gpio_end(RF_nSS);
}


/* ----- SPI --------------------------------------------------------------- */


static void spi_begin(void)
{
	gpio_clr(RF_nSS);
}


static void spi_end(void)
{
	gpio_set(RF_nSS);
}


static void spi_send(uint8_t v)
{
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (v & mask)
			gpio_set(RF_MOSI);
		else
			gpio_clr(RF_MOSI);
		gpio_set(RF_SCLK);
		gpio_clr(RF_SCLK);
	}
}


static uint8_t spi_recv(void)
{
	uint8_t res = 0;
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (gpio_read(RF_MISO))
			res |= mask;
		gpio_set(RF_SCLK);
		gpio_clr(RF_SCLK);
	}
	return res;
}


static uint8_t spi_io(uint8_t v)
{
	uint8_t res = 0;
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (v & mask)
			gpio_set(RF_MOSI);
		else
			gpio_clr(RF_MOSI);
		if (gpio_read(RF_MISO))
			res |= mask;
		gpio_set(RF_SCLK);
		gpio_clr(RF_SCLK);
	}
	return res;
}


/* ----- Register access --------------------------------------------------- */


static void reg_write(uint8_t reg, uint8_t v)
{
	spi_begin();
	spi_send(AT86RF230_REG_WRITE | reg);
	spi_send(v);
	spi_end();
}


static uint8_t reg_read(uint8_t reg)
{
	uint8_t res;

	spi_begin();
	spi_send(AT86RF230_REG_READ | reg);
	res = spi_recv();
	spi_end();
	return res;
}


/* ----- Access random number generator ------------------------------------ */


static void setup_rng(void)
{
	/* start oscillator */
	reg_write(REG_TRX_STATE, TRX_CMD_TRX_OFF);

	/* return REG_RSSI when sending command byte */
	reg_write(REG_TRX_CTRL_1, SPI_CMD_MODE_PHY_RSSI << SPI_CMD_MODE_SHIFT);

	/* start receiver (required by RNG) */
	reg_write(REG_TRX_STATE, TRX_CMD_RX_ON);
}


uint8_t rf_rng(void)
{
	static bool active = 0;
	uint8_t rssi;

	if (!active) {
		setup_rng();
		active = 1;
	}
	spi_begin();
	rssi = spi_io(AT86RF230_REG_READ);
	spi_end();
	return (rssi >> RND_VALUE_SHIFT) & RND_VALUE_MASK;
}


const char *rf_start(void)
{
	static char buf[30];
	const char *name;
	uint8_t v, m0, m1;
	char *s;

	rf_begin();
	slp_tr(0);	/* exit sleep */
	rf_reset();

	/*
	 * Set CLKM_CTRL = 2 for 2 MHz CLKM output. Note that the AT86RF232
	 * (unlike the 230, 231, and 233) isn't supposed to be able to do this.
	 * However, it does ...
	 */
	reg_write(REG_TRX_CTRL_0, 2);

	switch (reg_read(REG_PART_NUM)) {
	case 0x02:
		name = "AT86RF230";
		break;
	case 0x03:
		name = "AT86RF231";
		break;
	case 0x0a:
		name = "AT86RF232";
		break;
	case 0x0b:
		name = "AT86RF233";
		break;
	default:
		name = "??";
		break;
	}
	v = reg_read(REG_VERSION_NUM);
	m0 = reg_read(REG_MAN_ID_0);
	m1 = reg_read(REG_MAN_ID_1);

	strcpy(buf, name);
	s = strchr(buf, 0);
	*s++ = ' ';
	*s++ = 'v';
	s += print_number(s, v, 0, 10);
	memcpy(s, " xxxx", 5);
	s += 5;
	s += print_number(s, m1, 2, 16);
	s += print_number(s, m0, 2, 16);
	*s = 0;
	return buf;
}


void rf_init(void)
{
	gpio_init_out(RF_nRST, 0);
	gpio_init_out(RF_SLP_TR, 1);

	gpio_init_out(RF_MOSI, 0);
	gpio_init_off(RF_MISO);
	gpio_init_out(RF_SCLK, 0);
	gpio_init_out(RF_nSS, 1);
	gpio_init_in(RF_DIG2, 0);
	gpio_init_off(RF_IRQ);
	gpio_init_off(RF_CLK);

	gpio_init_out(RF_nRST, 1);	// driving nRST high saves 100 uA
}
