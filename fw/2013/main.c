/*
 * fw/2013/main.c - Experimental firmware for Anelok 2013 design
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "regs.h"
#include "misc.h"
#include "gpio.h"
#include "board.h"
#include "power.h"
#include "clock.h"
#include "led.h"
#include "display.h"
#include "text.h"
#include "wheel.h"
#include "rf.h"
#include "mmc-hw.h"
#include "mmc.h"
#include "adc.h"
#include "usb-board.h"
#include "usb.h"
#include "fmt.h"
#include "console.h"

#include "dfu.h"


/* ---- Miscellaneous ------------------------------------------------------ */


static void misc_init(void)
{
	/*
	 * Initializing these pins to "output low" instead of "off" reduces
	 * system current (at 3.3 V) by roughly 280 uA.
	 */

	gpio_init_out(GPIO_ID(B, 0), 0);	/* TP4 */

	gpio_init_out(GPIO_ID(E, 29), 0);	/* TP5 */
	gpio_init_out(GPIO_ID(E, 30), 0);	/* TP6 */
	gpio_init_out(GPIO_ID(E, 24), 0);	/* TP7 */
	gpio_init_out(GPIO_ID(E, 25), 0);	/* TP8 */
}


/* ----- Status screen ----------------------------------------------------- */


static char clean_ascii(uint8_t c)
{
	return c >= ' ' && c < 127 ? c : '?';
}


static const char *card_status(void)
{
	static char buf[13]; /* "OI PNM__ P.R" */
	uint8_t cid[16];
	uint8_t i;
	char *s = buf;

	if (!card_present())
		return "No card";
	if (!mmc_init())
		return "Card failed";
	if (!mmc_read_cid(cid))
		return "No CID";
	/*
	 * CID:
	 * 0     Manufacturer ID (MID), apparently not publicly documented [1]
	 * 1-2   OEM/Application ID (OID), two ASCII characters
	 * 3-8   Product Name (PNM), 5 ASCII characters
	 * 9     Product Revision (PRV), two BCD digits
	 *
	 * [1] http://www.bunniestudios.com/blog/?p=2297
	 */
	for (i = 1; i != 3; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' ';
	for (i = 3; i != 8; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' ';
	print_number(s++, cid[9] >> 4, 1, 16);
	*s++ = '.';
	print_number(s++, cid[9] & 15, 1, 16);
	*s = 0;
	return buf;
}


static const char *cpu_id(void)
{
	static char buf[22];
	char *s = buf;
	uint8_t n;
	uint32_t f;

	n = (SIM_SDID & SIM_SDID_SERIESID_MASK) >> SIM_SDID_SERIESID_SHIFT;
	switch (n) {
	case 1:	/* KL family */
		*s++ = 'K';
		*s++ = 'L';
		break;
	default:
		s += print_number(s, n, 1, 10);
		*s++ = '?';
		goto revision;
	}

	n = (SIM_SDID & SIM_SDID_FAMID_MASK) >> SIM_SDID_FAMID_SHIFT;
	s += print_number(s, n, 1, 10);

	n = (SIM_SDID & SIM_SDID_SUBFAMID_MASK) >> SIM_SDID_SUBFAMID_SHIFT;
	s += print_number(s, n, 1, 10);

revision:
	*s++ = ' ';

	n = (SIM_SDID & SIM_SDID_REVID_MASK) >> SIM_SDID_REVID_SHIFT;
	s += print_number(s, n, 1, 10);
	*s++ = '.';

	n = (SIM_SDID & SIM_SDID_DIEID_MASK) >> SIM_SDID_DIEID_SHIFT;
	s += print_number(s, n, 1, 10);
	*s++ = ' ';

#if 0
	n = (SIM_SDID & SIM_SDID_SRAMSIZE_MASK) >> SIM_SDID_SRAMSIZE_SHIFT;
	s += print_number(s, n ? 1 << (n-1) : 0, 1, 10);

	*s++ = 'k';
	*s++ = 'B';
	*s++ = ' ';
#endif

	rf_start();	/* cpu_clock_kHz needs the 2 MHz clock */

	f = cpu_clock_kHz();
	s += print_number(s, f/1000, 1, 10);
	*s++ = '.';
	s += print_number(s, f % 1000, 3, 10);
	*s++ = 'M';
	*s++ = 'H';
	*s++ = 'z';
	*s = 0;

	return buf;
}


static const char *vsys(void)
{
	static char buf[22];
	char *s = buf;
	uint16_t mV, mC;

	if (!adc_begin())
		return "ADC failed";

	mC = adc_temp_mC(&mV);
	adc_end();

	s += print_number(s, mV/1000, 1, 10);
	*s++ = '.';
	s += print_number(s, mV % 1000, 3, 10);
	*s++ = 'V';
	*s++ = ' ';

	s += print_number(s, mC/1000, 1, 10);
	*s++ = '.';
	s += print_number(s, mC % 1000, 3, 10);
	*s++ = 'C';
	*s = 0;

	return buf;
}


static void status_screen(void)
{
	uint8_t x;

	display_clear();
	text_str(&font_10x20, "ANELOK", 0, 0);

	/* MCU identification */
	text_str(&font_small, cpu_id(), 0, 19);

	/* Build date */
	x = text_str(&font_small, "Build ", 0, 28);
	text_str(&font_small, BUILD_DATE, x, 28);

	/* RF identification */
	text_str(&font_small, rf_start(), 0, 37);

	/* Memory card status */
	text_str(&font_small, card_status(), 0, 46);

#if 0
	/* USB */
	if (usb_a()) {
		text_str(&font_small, "USB A", 0, 55);
	} else {
		usb_begin_device();
		if (usb_sof())
			text_str(&font_small, "USB B", 0, 55);
		else
			text_str(&font_small, "---", 0, 55);
		usb_end_device();
	}
#endif

	/* Vsys, temperature */
	text_str(&font_small, vsys(), 42, 55);

	display_update();
}


/* ----- RNG test ---------------------------------------------------------- */


static void test_rng(void)
{
	uint8_t n[128] = { 0, };
	uint8_t x, r = 0;
	uint8_t got = 0;

	display_clear();
	for (x = 0; x != 127; x++)
		display_set(x, 63);
	while (1) {
		while (got < 7) {
			r = r << 2 | rf_rng();
			got += 2;
		}
		x = r & 127;
		r >>= 7;
		got -= 7;

		if (n[x]++ == 63)
			break;
		display_clr(x, 64-n[x]);
		display_set(x, 63-n[x]);
		display_update();
	}
}


/* ----- PIN input --------------------------------------------------------- */


static const char *a = NULL;
static const char *pa = NULL;
static int8_t ax0;


static void alphabet(const char *s)
{
	uint8_t x, w;

	ax0 = 64-(strlen(s)*5)/2;
	if (ax0 < 0)
		ax0 = 0;
	for (x = ax0; *s; x += w) {
		text_char(&font_5x7, *s, x, 56);
		w = *s == 'H' || *s == 'I' ? 4 : 5;
		s++;
	}
}


static void arrow(void)
{
	int8_t c = ax0+1+(pa-a)*5;
	uint8_t x, y;

	if (*pa > 'H')
		c--;
	if (*pa > 'I')
		c--;
	if (c < 2)
		c = 2;
	for (y = 0; y != 3; y++)
		for (x = c-y; x <= c+y; x++)
			display_set(x, 54-y);
}


static void pin_text(const char *s)
{
	display_clear();
	text_str(&font_10x20, s, 64-5*strlen(s), 20);
	if (a) {
		alphabet(a);
		arrow();
	}
	display_update();
}



#define	LINGER	100
#define	ACCEPT	200


static bool pin(void)
{
	char buf[20], *p = buf;
	char show[20];
	int8_t dir = 0;
	unsigned n = 0;
	bool linger = 0;

	a = pa = NULL;
	while (1) {
		if (!a) {
			pin_text("-");
		} else {
			if (linger && p > buf) {
				memset(show, '*', p-buf-1);
				show[p-buf-1] = p[-1];
			} else {
				memset(show, '*', p-buf);
			}
			show[p-buf] = *pa;
			show[p-buf+1] = 0;
			pin_text(show);
		}
		n = 0;
		while (1) {
			msleep(10);
			n++;
			if (n == LINGER && linger) {
				linger = 0;
				break;
			}
			if (n == ACCEPT) {
				*p++ = *pa;
				*p = 0;
				return !strcmp(buf, "ANELOK") ||
				    !strcmp(buf, "2013");
			}
			if (wheel_button()) {
				do msleep(20);
				while (wheel_button());
				if (p != buf)
					p--;
				if (p == buf)
					a = pa = NULL;
				/* continue in any direction after deletion */
				dir = 0;
				linger = 0;
				break;
			}
			switch (wheel_rot()) {
			case 1:
				if (!a) {
					pa = a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
					dir = 1;
					break;
				}
				if (dir == -1) {
					*p++ = *pa;
					linger = 1;
					dir = 1;
				} else {
					linger = 0;
				}
				if (!*++pa)
					pa = a;
				break;
			case -1:
				if (!a) {
					pa = a = "0123456789";
					dir = -1;
					break;
				}
				if (dir == 1) {
					*p++ = *pa;
					linger = 1;
					dir = -1;
				} else {
					linger = 0;
				}
				if (pa == a)
					pa = strchr(a, 0)-1;
				else
					pa--;
				break;
			default:
				continue;
			}
			break;
		}
	}
}


/* ----- Main -------------------------------------------------------------- */


static void usb(void)
{
	console_init();
	console_printf("USB ...");
	usb_begin_host();
	while (1) {
#ifdef BARE_METAL_USB
		static uint16_t n = 0;
		USBOTG_IRQHandler();
		if (!++n) {
			console_printf(".");
			console_update();
		}
#else
		usb_poll_host();
		console_update();
#endif
	}
}


static void dfu_mode(void)
{
	console_init();
	clock_external();
	console_printf("DFU ...");
	console_update();
	usb_begin_device();
	dfu_init();
	while (1) {
		usb_poll_device();
		if (wheel_button())
			console_update();
	}
}


int main(void)
{
	uint16_t n = 0;

	/*
	 * Current measurements are of board 0 while attached to a FRDM-KL25Z
	 * and powered by it. Values tend to vary by up to about 0.2 mA between
	 * measurement sessions.
	 */
				// 7.3 mA
	power_init();		// 4.4 mA
	led_init();		// 4.0 mA
	display_init();		// 4.0 mA
	wheel_init();		// 4.0 mA
	card_init();		// 4.1 mA
	rf_init();		// 4.1 mA
	usb_init();		// 4.1 mA
	misc_init();		// 4.1 mA

	power_disp(1);		// 4.8-4.9 mA

	clock_internal();	// 6.6 mA

	msleep(200); /* give user some time to release the button */
	if (wheel_button()) {
		display_on(1);
		status_screen();
		while (wheel_button());
		display_off();
	}
	msleep(20); /* debounce */

	dfu_mode();

//usb();

	while (1) {
		bool ok;

		if (!wheel_button()) {
			msleep(10);
			continue;
		}
		display_on(1);

		do msleep(20);
		while (wheel_button());

		ok = pin();
		if (!ok) {
			display_off();
			continue;
		}
		rf_start(); /* @@@ should also stop it */
		test_rng();
		while (!wheel_button()) {
			led(n >> 15);
			n++;
		}
		led(0);
		display_off();

		do msleep(20);
		while (wheel_button());
	}

	while (!wheel_button()) {
#if 0
		switch (wheel_rot()) {
		case -1:
			if (p != fb)
				*--p = 0;
			update = 1;
			break;
		case 1:
			*p++ = 0xff;
			update = 1;
			break;
		default:
			break;
		}
#endif
	}
	display_off();
	led(0);

	power_disp(0);

	while (1);
}
