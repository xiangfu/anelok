/*
 * fw/wheel.c - Wheel driver
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "gpio.h"
#include "board.h"
#include "wheel.h"


static bool last_a, last_b;
static bool last_eq;


static bool read_pin(gpio_id id)
{
	bool v;

	gpio_begin(id);
	v = gpio_read(id);
	gpio_end(id);
	return v;
}


bool wheel_button(void)
{
	return !read_pin(WHEEL_CENTER);
}


int8_t wheel_rot(void)
{
	bool a = read_pin(WHEEL_A);
	bool b = read_pin(WHEEL_B);
	int8_t dir = 0;

	if (a == b && a != last_eq) {
		last_eq = a;
		dir = a == last_a ? 1 : -1;
	}
	last_a = a;
	last_b = b;
	return dir;
}


void wheel_init(void)
{
	gpio_init_in(WHEEL_CENTER, 1);
	gpio_init_in(WHEEL_A, 1);
	gpio_init_in(WHEEL_B, 1);

	last_a = read_pin(WHEEL_A);
	last_b = read_pin(WHEEL_B);
	last_eq = last_a;
}
