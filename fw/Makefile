#
# Makefile - Makefile of the Anelok firmware
#
# Written 2013-2015 by Werner Almesberger
# Copyright 2013-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Based on Andrew Payne's Makefile from bare-metal-arm
#


# ----- Device configuration database -----------------------------------------

ANELOK_DEVCFG ?= DEVCFG

# ----- Target selection ------------------------------------------------------

#
# Supported targets:
#
# ANELOK_2013	Anelok 2013 design, with wheel
# ANELOK_2014	Anelok 2014 design, with capacitive sensor
# BOOT_2014	DFU boot loader for Anelok 2014
# YBOX		Y-Box
# BOOT_YBOX	DFU boot loader for Y-Box
#

#TARGETS = ANELOK_2013 ANELOK_2014 BOOT_2014 YBOX BOOT_YBOX
TARGETS = ANELOK_2014 BOOT_2014 YBOX BOOT_YBOX

TARGET ?= ANELOK_2014

NAME_ANELOK_2013	= anelok-2013
NAME_ANELOK_2014	= anelok-2014
NAME_BOOT_2014		= boot-2014
NAME_YBOX		= ybox
NAME_BOOT_YBOX		= boot-ybox
NAME = $(NAME_$(TARGET))

BOARD_DIR_ANELOK_2013	= 2013
BOARD_DIR_ANELOK_2014	= 2014
BOARD_DIR_BOOT_2014	= 2014
BOARD_DIR_YBOX		= ybox
BOARD_DIR_BOOT_YBOX	= ybox
BOARD_DIR = $(BOARD_DIR_$(TARGET))

PART_BASE_ANELOK_2013	= $(BOOT_BASE)
PART_BASE_ANELOK_2014	= $(APP_BASE)
PART_BASE_BOOT_2014	= $(BOOT_BASE)
PART_BASE_YBOX		= $(APP_BASE)
PART_BASE_BOOT_YBOX	= $(BOOT_BASE)
PART_BASE = $(PART_BASE_$(TARGET))

USB_ID_ANELOK_2013	= 20b7:ae70
USB_ID_ANELOK_2014	= 20b7:ae70
USB_ID_BOOT_2014	= 20b7:ae70
USB_ID_YBOX		= 20b7:ae71
USB_ID_BOOT_YBOX	= 20b7:ae71

USB_ID = $(USB_ID_$(TARGET))

# ----- Memory layout ---------------------------------------------------------

BOOT_BASE	= 0x00410
APP_BASE	= 0x04000

BOOT_SIZE	= ($(APP_BASE) - $(BOOT_BASE))
APP_SIZE	= (0x20000 - $(APP_BASE))

# ----- General build definitions ---------------------------------------------

PREFIX = arm-none-eabi-

CC = $(PREFIX)gcc
AR = $(PREFIX)ar
OBJCOPY = $(PREFIX)objcopy
SIZE = $(PREFIX)size

SWDLIB = ../swdlib

LD_SCRIPT_BARE		= sdk/MKL46Z128xxx4_flash.ld
LD_SCRIPT_ANELOK_2013	= $(LD_SCRIPT_BARE)
LD_SCRIPT_ANELOK_2014	= $(NAME).lds
LD_SCRIPT_BOOT_2014	= $(LD_SCRIPT_BARE)
LD_SCRIPT_YBOX		= $(NAME).lds
LD_SCRIPT_BOOT_YBOX	= $(LD_SCRIPT_BARE)
LD_SCRIPT = $(LD_SCRIPT_$(TARGET))

USB_OBJS = kinetis-dev.o kinetis-host.o kinetis-common.o \
	   usb-board.o usb-board-common.o usb.o host.o
USB_STACK = ../../ben-wpan/atusb/fw/usb

USBWAIT = ../../ben-wpan/tools/usbwait/usbwait

CFLAGS_WARN = -Wall -Wextra -Wshadow -Wmissing-prototypes \
	      -Wmissing-declarations -Wno-format-zero-length \
	      -Wno-unused-parameter

CFLAGS = -g -Os $(CFLAGS_WARN) \
	 -mcpu=cortex-m0 -mthumb -mfloat-abi=soft \
	 -fdata-sections -ffunction-sections -Wl,--gc-sections \
	 -I$(shell pwd) -Iboard -I$(USB_STACK) -I$(SWDLIB) -Iccfw \
	 $(SUBDIRS:%=-I$(shell pwd)/%) \
	 -DAPP_BASE=$(APP_BASE) -DAPP_SIZE="$(APP_SIZE)" \
	 -D$(TARGET)

SUBDIRS = usb disp mmc base ui common plat rf cc board sdk fatfs db \
	  $(BOARD_DIR)
COMMON_OBJS = gpio.o extint.o board.o misc.o clock.o led.o $(USB_OBJS)
APP_OBJS = $(COMMON_OBJS) start.o main.o display.o text.o boost.o \
	   power.o mmc-hw.o mmc.o adc.o console.o fmt.o id.o \
	   devcfg.o sleep.o tick.o event.o ff.o diskio.o
BOOT_OBJS = $(COMMON_OBJS) start.o flashcfg.o boot.o \
	    dfu.o dfu_common.o dfu-wait.o \
	    flash.o platflash.o
OBJS_ANELOK_2013 = $(APP_OBJS) start.o flashcfg.o wheel.o atrf.o
OBJS_ANELOK_2014 = $(APP_OBJS) descr.o dfu_common.o ep0.o touch.o \
    hid.o hid-type.o comm.o \
    cc.o ccdbg.o \
    input.o sel.o textsel.o std.o icon.o account.o auth.o qa.o pm.o vusb.o \
    ui.o ui_off.o ui_login.o ui_login_setup.o ui_select.o ui_select_setup.o \
    ui_account.o \
    ui_show.o ui_sorry.o
OBJS_BOOT_2014 = $(BOOT_OBJS) ccflash.o cc.o ccdbg.o
OBJS_YBOX = $(COMMON_OBJS) ybox.o rf.o descr.o ep0.o touch.o
OBJS_BOOT_YBOX = $(BOOT_OBJS) ccflash.o cc.o ccdbg.o
OBJS = $(OBJS_$(TARGET))
FONTS = 5x7 7x13 10x20

vpath usb.c $(USB_STACK)
vpath dfu.c $(USB_STACK)
vpath dfu_common.c $(USB_STACK)
vpath ccdbg.c $(SWDLIB)
vpath %.c $(SUBDIRS)

# Include Makefile.c-common after setting CC, etc.

include ../common/Makefile.c-common

VERSION_C = board/version.c
include ../common/Makefile.version

# -----------------------------------------------------------------------------

.PHONY:	all fonts frdm flash cc dfu update ccdfu targets
.PHONY:	clean clean-all spotless


all::		$(NAME).bin

# ---- Generated C source -----------------------------------------------------

devcfg.o:	devcfg.inc

devcfg.inc:	$(ANELOK_DEVCFG) mkdevcfg
		./mkdevcfg $< >$@ || { rm -f $@; exit 1; }

start.o:	vecs.inc

vecs.inc:
		./genvecs.pl >vecs.inc \
		    DMA0 DMA1 DMA2 DMA3 FTFA LVD_LVW LLW I2C0 I2C1 \
		    SPI0 SPI1 UART0 UART1 UART2 ADC0 CMP0 \
		    TPM0 TPM1 TPM2 RTC RTC_Seconds PIT \
		    I2S0 USB0 DAC0 TSI0 MCG LPTMR0 PORTA PORTC_PORTD || \
		    { rm -f $@; exit 1; }

# ---- Fonts ------------------------------------------------------------------

fonts:		$(FONTS:%=font_%.h)

disp/text.c:	$(FONTS:%=font_%.h)

font_5x7.h:	fontify
		./fontify -z font_5x7 5x7 >$@ || { rm -f $@; exit 1; }

font_7x13.h:	fontify
		./fontify -z font_7x13 7x13bold >$@ || { rm -f $@; exit 1; }

font_10x20.h:	fontify
		./fontify font_10x20 10x20 >$@ || { rm -f $@; exit 1; }

# ----- Executable ------------------------------------------------------------

%.srec:		%.elf
		$(BUILD) $(OBJCOPY) -O srec $< $@

%.bin:		%.elf
		$(BUILD) $(OBJCOPY) -O binary $< $@
		@echo -n "build #$(BUILD_NUMBER) $(BUILD_DATE) "
		@echo "$(BUILD_HASH)$(if $(BUILD_DIRTY:0=),+)"

$(NAME).elf:	$(OBJS) $(LD_SCRIPT)
		$(MAKE) version.o
		$(CC) $(CFLAGS) -T $(LD_SCRIPT) -o $@ $(OBJS) version.o
		$(SIZE) $@

$(NAME).lds:	$(LD_SCRIPT_BARE) mklds
		$(BUILD) ./mklds $(PART_BASE) $< >$@ || { rm -f $@; exit 1; }

# ----- Flashing (FRDM-KL25Z) -------------------------------------------------

frdm:		$(NAME).srec
		mount -L FRDM-KL25Z /mnt/tmp
		cp $(NAME).srec /mnt/tmp
		sync
		cat /mnt/tmp/LASTSTAT.TXT
		@echo
		umount /mnt/tmp

# ----- Flashing (Ben and libswd) ---------------------------------------------

flash:		$(NAME).bin
		scp $^ ben:
		ssh ben ./swd $^

# ----- CC2543 firmware -------------------------------------------------------

cc:
		$(MAKE) -C ccfw

# ----- DFU -------------------------------------------------------------------

dfu:		$(NAME).bin
		dfu-util -d $(USB_ID) -a KL26 -D $^

update:		$(NAME).bin
		$(USBWAIT) -r -i 0.01 $(USB_ID)
		$(MAKE) dfu

ccdfu:
		$(MAKE) -C ccfw USB_ID=$(USB_ID) dfu

# ----- Test-build all targets ------------------------------------------------

targets:
		for n in $(TARGETS); do make TARGET=$$n clean; done
		for n in $(TARGETS); do echo "# ----- $$n -----" 1>&2; \
		    make TARGET=$$n || exit; \
		    make TARGET=$$n clean || exit; done

# ----- Tags ------------------------------------------------------------------

etags:
		git ls-files | grep -v 2013 | etags -

# ----- Cleanup ---------------------------------------------------------------

clean::
		rm -f devcfg.inc vecs.inc
		rm -f $(NAME).elf $(NAME).srec $(NAME).bin
		rm -f $(NAME).lds
		$(MAKE) -C ccfw clean

clean-all:
		for n in $(TARGETS); do make TARGET=$$n clean; done

spotless:	clean
		rm -f $(FONTS:%=font_%.h)
#		rm -f $(FRDM_KL26Z_SC_EXE)
		rm -f .version
		$(MAKE) -C ccfw spotless
