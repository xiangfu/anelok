/*
 * fw/display.h - OLED display driver
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef DISPLAY_H
#define	DISPLAY_H

#include <stdbool.h>
#include <stdint.h>


#define	FB_X	128
#define	FB_Y	64


void display_clear(void);
void display_set(uint8_t x, uint8_t y);
void display_clr(uint8_t x, uint8_t y);
void display_blit(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y);
void display_clear_rect(uint8_t xa, uint8_t ya, uint8_t xb, uint8_t yb);
void display_rotate(bool on);

bool display_update(void);

void display_on(bool clear);
void display_off(void);
void display_init(void);

#endif /* !DISPLAY_H */
