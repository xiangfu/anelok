/*
 * fw/text.h - Text rendering
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TEXT_H
#define	TEXT_H

#include <stdint.h>


extern const struct font font_5x7;
extern const struct font font_7x13;
extern const struct font font_10x20;
extern const struct font font_small;
extern const struct font font_medium;


void text_char(const struct font *f, char ch, uint8_t x, int8_t y);
uint8_t text_str(const struct font *f, const char *s,
    uint8_t x, int8_t y);
uint8_t text_nl(const struct font *f);

#endif /* !TEXT_H */
