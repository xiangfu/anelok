/*
 * fw/text.c - Text rendering
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "display.h"
#include "text.h"

#include "font_small.xbm"
#include "font_medium.xbm"


struct font {
	const uint8_t *data;	/* font data */
	uint8_t w, h;		/* character width and height */
	uint8_t cx, cy;		/* character cell in matrix */
	uint8_t mx;		/* character matrix width */
} const font_small = {
	.data	= font_small_bits,
	.w	= 5,
	.h	= 8,
	.cx	= 8,
	.cy	= 8,
	.mx	= 16,
}, font_medium = {
	.data	= font_medium_bits,
	.w	= 6,
	.h	= 10,
	.cx	= 8,
	.cy	= 10,
	.mx	= 16,
};

#include "font_5x7.h"
#include "font_7x13.h"
#include "font_10x20.h"


void text_char(const struct font *f, char ch, uint8_t x, int8_t y)
{
	uint8_t px, py;

	px = (ch - ' ') % f->mx;
	py = (ch - ' ') / f->mx;
	display_blit(f->data + px * (f->cx / 8) +
	    py * (f->mx * f->cx / 8 * f->cy),
	    f->w, f->h, f->mx * f->cx / 8, x, y);
}


uint8_t text_str(const struct font *f, const char *s, uint8_t x, int8_t y)
{
	while (*s) {
		text_char(f, *s++, x, y);
		x += f->w + 1;
	}
	return x;
}


uint8_t text_nl(const struct font *f)
{
	return f->h;
}
