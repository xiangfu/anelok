/*
 * fw/board/board-2013.h - GPIOs of 2013 Anelok board
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_2013_H
#define	BOARD_2013_H

#include "gpio.h"


/* ----- Power distribution ------------------------------------------------ */

#define	PWR_EN		GPIO_ID(B, 17)
#define	CARD_OFF	GPIO_ID(E, 21)

/* ----- LED --------------------------------------------------------------- */

#define	LED		GPIO_ID(E, 20)

/* ----- Display ----------------------------------------------------------- */

#define	DISP_SCLK	GPIO_ID(D, 6)
#define	DISP_SDIN	GPIO_ID(B, 16)
#define	DISP_DnC	GPIO_ID(B, 3)
#define	DISP_nRES	GPIO_ID(B, 2)
#define	DISP_nCS	GPIO_ID(B, 1)

/* ----- Wheel ------------------------------------------------------------- */

#define	WHEEL_CENTER	GPIO_ID(A, 4)
#define	WHEEL_A		GPIO_ID(A, 1)
#define	WHEEL_B		GPIO_ID(A, 2)

/* ----- USB --------------------------------------------------------------- */

#define	USB_ID		GPIO_ID(D, 7)

/* ----- Memory card ------------------------------------------------------- */

#define	CARD_DAT2	GPIO_ID(D, 5)
#define	CARD_DAT3	GPIO_ID(D, 4)
#define	CARD_CMD	GPIO_ID(D, 3)
#define	CARD_CLK	GPIO_ID(D, 2)
#define	CARD_DAT0	GPIO_ID(D, 1)
#define	CARD_DAT1	GPIO_ID(D, 0)

#define	CARD_SW		GPIO_ID(C, 4)

/* ----- RF ---------------------------------------------------------------- */

#define	RF_MOSI		GPIO_ID(C, 7)
#define	RF_MISO		GPIO_ID(C, 6)
#define	RF_SCLK		GPIO_ID(C, 5)
#define	RF_nSS		GPIO_ID(C, 3)
#define	RF_nRST		GPIO_ID(C, 2)
#define	RF_DIG2		GPIO_ID(C, 1)
#define	RF_SLP_TR	GPIO_ID(C, 0)
#define	RF_IRQ		GPIO_ID(A, 19)
#define	RF_CLK		GPIO_ID(A, 18)

#endif /* !BOARD_2013_H */
