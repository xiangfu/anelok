/*
 * fw/board-ybox.h - Board-specific definitions for Y-Box
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_YBOX_H
#define	BOARD_YBOX_H

#include "gpio.h"


/* ----- LED --------------------------------------------------------------- */

#define	LED		GPIO_ID(E, 30)

/* ----- RF debug ---------------------------------------------------------- */

#define	RF_nRESET	GPIO_ID(D, 7)
#define	RF_DD		GPIO_ID(D, 6)
#define	RF_DC		GPIO_ID(D, 5)

/* ----- RF clock ---------------------------------------------------------- */

#define	RF_CLK		GPIO_ID(A, 18)
#define	CLKOUT		GPIO_ID(C, 3)		/* for debugging only */

/* ----- RF VDD sense ------------------------------------------------------ */

#define	RF_VDD		GPIO_ID(E, 0)

/* ----- RF SPI ------------------------------------------------------------ */

#define	RF_MOSI		GPIO_ID(C, 6)
#define	RF_MISO		GPIO_ID(C, 7)
#define	RF_SCLK		GPIO_ID(C, 5)
#define	RF_nSEL		GPIO_ID(C, 4)

#define	RF_nINT		RF_DD
#define	RF_nREQ		RF_DC

/* ----- USB --------------------------------------------------------------- */

#define	USB_PRODUCT	0xae71	/* AnELok, device 1 */
				/* - --           - */

#define	DFU_ALT_SETTINGS 2

#define	BOARD_MAX_mA	500

#endif /* !BOARD_YBOX_H */
