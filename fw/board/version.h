/*
 * fw/version.h - Build version
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef VERSION_H
#define VERSION_H

#include <stdbool.h>
#include <stdint.h>


extern const char *build_date;
extern const uint16_t build_number;
extern uint32_t build_hash;
extern bool build_dirty;

#endif /* !VERSION_H */
