/*
 * fw/version.c - Build version
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "version.h"


const char *build_date = BUILD_DATE;
const uint16_t build_number = BUILD_NUMBER;
uint32_t build_hash = BUILD_HASH;
bool build_dirty = BUILD_DIRTY;
