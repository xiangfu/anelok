/*
 * fw/base/qa.c - Quality assurance tools
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "power.h"
#include "display.h"
#include "console.h"
#include "qa.h"


void __oops(const char *file, unsigned line)
{
	power_disp_force_on();
	display_init();
	console_init();
	console_printf("%s:%d\n", file, line);
	console_update();
	panic();
}
