/*
 * fw/base/flashcfg.c - Flash configuration field
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Based on Andrew Payne's _startup.c
 */


#include <stdint.h>


/*
 * Flash configuration field (loaded into flash memory at 0x400)
 *
 * Note:  RESET_PIN_CFG is set to enable external RESET signal
 */

__attribute__((section (".FlashConfig"))) const uint8_t _cfm[0x10] = {
	0xFF,	/* NV_BACKKEY3: KEY=0xFF */
	0xFF,	/* NV_BACKKEY2: KEY=0xFF */
	0xFF,	/* NV_BACKKEY1: KEY=0xFF */
	0xFF,	/* NV_BACKKEY0: KEY=0xFF */
	0xFF,	/* NV_BACKKEY7: KEY=0xFF */
	0xFF,	/* NV_BACKKEY6: KEY=0xFF */
	0xFF,	/* NV_BACKKEY5: KEY=0xFF */
	0xFF,	/* NV_BACKKEY4: KEY=0xFF */
	0xFF,	/* NV_FPROT3: PROT=0xFF */
	0xFF,	/* NV_FPROT2: PROT=0xFF */
	0xFF,	/* NV_FPROT1: PROT=0xFF */
	0xFF,	/* NV_FPROT0: PROT=0xFF */
	0x7E,	/* NV_FSEC: KEYEN=1,MEEN=3,FSLACC=3,SEC=2 */
	0xFF,	/* NV_FOPT: ??=1,??=1,FAST_INIT=1,LPBOOT1=1,RESET_PIN_CFG=1,
			    NMI_DIS=1,EZPORT_DIS=1,LPBOOT0=1 */
	0xFF,
	0xFF
};
