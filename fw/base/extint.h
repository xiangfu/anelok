/*
 * fw/exting.h - External interrupts (GPIOs)
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EXTINT_H
#define	EXTINT_H

#include <stdint.h>

#include "gpio.h"


struct extint {
	gpio_id id;
	void (*fn)();
	enum int_type type;
	struct extint *next;
};


void extint_llwu(uint16_t flags);

void extint_disable(const struct extint *extint);
void extint_enable(const struct extint *extint);

void extint_add(struct extint *extint, gpio_id id,
    void (*fn)(void), enum int_type type);
void extint_del(struct extint *extint);

#endif /* !EXTINT_H */
