/*
 * fw/gpio.h - GPIO abstraction (KL25)
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef GPIO_H
#define	GPIO_H

#include <stdbool.h>
#include <stdint.h>

#include "regs.h"


#define	GPIO_PORTS	5


/* ----- GPIO IDs ---------------------------------------------------------- */


enum {
	GPIO_PORT_A	= 0,
	GPIO_PORT_B	= 1,
	GPIO_PORT_C	= 2,
	GPIO_PORT_D	= 3,
	GPIO_PORT_E	= 4
};


#define	GPIO_ID(port, bit)	((GPIO_PORT_##port) << 5 | (bit))


typedef uint8_t gpio_id;


static inline gpio_id gpio_mkid(uint8_t port, uint8_t bit)
{
	return port << 5 | bit;
}


static inline uint8_t gpio_port(gpio_id id)
{
	return id >> 5;
}


static inline uint8_t gpio_bit(gpio_id id)
{
	return id & 31;
}


static inline uint32_t gpio_mask(gpio_id id)
{
	return 1 << gpio_bit(id);
}


/* ----- Clock gating ------------------------------------------------------ */


extern uint16_t __gpio_enabled[GPIO_PORTS];


static inline void gpio_begin(gpio_id id)
{
	uint8_t port = gpio_port(id);

	if (!__gpio_enabled[port]++)
		SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK << port;
}


static inline void gpio_end(gpio_id id)
{
	uint8_t port = gpio_port(id);

	if (!--__gpio_enabled[port])
		SIM_SCGC5 &= ~(SIM_SCGC5_PORTA_MASK << port);
}


/* ----- Port function ----------------------------------------------------- */


enum port_function {
	port_analog	= 0,
	port_gpio	= 1,
};


void gpio_fn(gpio_id id, enum port_function fn);
void gpio_pull(gpio_id id, bool on);


/* ----- Interrupt control ------------------------------------------------- */


enum int_type {
	int_disabled	= 0,
	int_low		= 8,
	int_rising	= 9,
	int_falling	= 10,
	int_both	= 11,
	int_high	= 12
};


void gpio_int(gpio_id id, enum int_type type);


/* ----- GPIO control registers -------------------------------------------- */


extern GPIO_MemMapPtr gpio_base[];
extern FGPIO_MemMapPtr fgpio_base[];


#define	GPIO_REG(reg, id)	GPIO_##reg##_REG(gpio_base[gpio_port(id)])
#define	FGPIO_REG(reg, id)	FGPIO_##reg##_REG(fgpio_base[gpio_port(id)])


/* ----- Direction control ------------------------------------------------- */


static inline void gpio_output(gpio_id id)
{
	GPIO_REG(PDDR, id) |= gpio_mask(id);
}


static inline void gpio_input(gpio_id id)
{
	GPIO_REG(PDDR, id) &= ~gpio_mask(id);
}


/* ----- Bit in/out -------------------------------------------------------- */


static inline void gpio_set(gpio_id id)
{
	GPIO_REG(PSOR, id) = gpio_mask(id);
}


static inline void gpio_clr(gpio_id id)
{
	GPIO_REG(PCOR, id) = gpio_mask(id);
}


static inline bool gpio_read(gpio_id id)
{
	return FGPIO_REG(PDIR, id) & gpio_mask(id);
}


/* ----- Convenience functions --------------------------------------------- */


void gpio_init_off(gpio_id id);
void gpio_init_out(gpio_id id, bool v);
void gpio_init_in(gpio_id id, bool pull);

#endif /* !GPIO_H */
