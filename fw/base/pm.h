/*
 * fw/base/pm.h - Power management
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PM_H
#define	PM_H

#include <stdbool.h>
#include <stdint.h>


/*
 * State	Condition
 * ----------	----------------------------------------
 * pm_active	everything is running
 * pm_dark1	display is dark (no white pixels)
 * pm_dark2	panel is powered down
 * pm_ready	TBD
 * pm_standby	TBD
 */

enum pm_state {
	pm_active,
	pm_dark1,
	pm_dark2,
	pm_ready,
	pm_standby,
	pm_n		/* number of states */
};


struct pm_policy {
	uint32_t t[pm_n];
};

#define	PM_OFF	pm_standby	/* "off" state */


extern struct pm_policy pm_policy_default;
extern struct pm_policy pm_policy_login;
extern struct pm_policy pm_policy_peruse;


void pm_usb_power(bool have);
enum pm_state pm_state(void);
void pm_busy(void);
void pm_sleep(enum pm_state s);
void pm_idle(uint32_t ms);
void pm_set_policy(const struct pm_policy *policy);

#endif /* !PM_H */
