/*
 * fw/clock.h - Clock management
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CLOCK_H
#define	CLOCK_H

#include <stdint.h>


uint32_t cpu_clock_kHz(void);

void clkout_bus(void);
void clkout_osc(void);
void clkout_off(void);

void clock_internal(void);
void clock_xtal_32k_fll(void);
void clock_xtal_32k(void);
void clock_external(void);
void clock_low(void);

/*
 * Clock modes (see KL26 RM 24.4.1.1):
 *
 * Mode	Oscillator	Multiplier	Function
 * ----	----------	----------	-----------------------
 * FEI	32 kHz RC	FLL		clock_internal
 * FEE	external	FLL		clock_xtal_32k_fll
 * FBI	any RC		- (FLL on)	-
 * FBE	external	- (FLL on)	-
 * PEE	external	PLL		clock_external
 * PBE	external	- (PLL on)	-
 * BLPI	any RC		-		clock_low
 * BLPE	external	-		clock_xtal_32k
 */

#endif /* !CLOCK_H */
