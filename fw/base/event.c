/*
 * fw/base/event.c - Asynchronous events
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "irq.h"
#include "event.h"


uint32_t events = 0;


bool __event_consume(enum event n)
{
	unsigned long flags;
	uint32_t mask = 1 << n;

	flags = irq_save();
	if (events & mask) {
		events &= ~mask;
		irq_restore(flags);
		return 1;
	}
	irq_restore(flags);
	return 0;
}

