/*
 * fw/gpio.c - GPIO abstraction (KL25)
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "regs.h"
#include "gpio.h"


GPIO_MemMapPtr gpio_base[] = GPIO_BASE_PTRS;
FGPIO_MemMapPtr fgpio_base[] = FGPIO_BASE_PTRS;
uint16_t __gpio_enabled[GPIO_PORTS];


static PORT_MemMapPtr port_base[] = PORT_BASE_PTRS;


#define	GET_PCR(var, id) \
	volatile uint32_t *var = \
	    &PORT_PCR_REG(port_base[gpio_port(id)], gpio_bit(id))


/* ----- Configuration ----------------------------------------------------- */


void gpio_fn(gpio_id id, enum port_function fn)
{
	GET_PCR(pcr, id);

	*pcr = (*pcr & ~PORT_PCR_MUX_MASK) | fn << PORT_PCR_MUX_SHIFT;
}


void gpio_pull(gpio_id id, bool on)
{
	GET_PCR(pcr, id);

	if (on)
		*pcr |= PORT_PCR_PE_MASK;
	else
		*pcr &= ~PORT_PCR_PE_MASK;
}


/* ----- Interrupts -------------------------------------------------------- */


void gpio_int(gpio_id id, enum int_type type)
{
	GET_PCR(pcr, id);

	*pcr = (*pcr & ~PORT_PCR_IRQC_MASK) | type << PORT_PCR_IRQC_SHIFT;
}


/* ----- Initialization ---------------------------------------------------- */


void gpio_init_off(gpio_id id)
{
	gpio_begin(id);
	gpio_fn(id, port_analog);
	gpio_end(id);
}


void gpio_init_out(gpio_id id, bool v)
{
	gpio_begin(id);
	if (v)
		gpio_set(id);
	else
		gpio_clr(id);
	gpio_output(id);
	gpio_fn(id, port_gpio);
	gpio_end(id);
}


void gpio_init_in(gpio_id id, bool pull)
{
	gpio_begin(id);
	gpio_pull(id, pull);
	gpio_input(id);
	gpio_fn(id, port_gpio);
	gpio_end(id);
}
