/*
 * fw/touch.c - Touch sensing (for development only)
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"


/* PTB1 = TSI0_CH6 */

#define	TOUCH	GPIO_ID(B, 1)


bool touch_read(uint16_t *res)
{
	bool got = 0;

	if (TSI0_GENCS & TSI_GENCS_EOSF_MASK) {
		*res = TSI0_DATA;
		TSI0_GENCS |= TSI_GENCS_EOSF_MASK;
		got = 1;
	}
	if (!(TSI0_GENCS & TSI_GENCS_SCNIP_MASK))
		TSI0_DATA = TSI_DATA_TSICH(6) | TSI_DATA_SWTS_MASK;
	return got;
}


void touch_init(void)
{
	SIM_SCGC5 |= SIM_SCGC5_TSI_MASK;
	TSI0_GENCS =
	    TSI_GENCS_ESOR_MASK		// End-of-scan interrupt
            | TSI_GENCS_MODE(0)		// Capactive sensing
            | TSI_GENCS_REFCHRG(4)	// Reference charge 8 uA
            | TSI_GENCS_DVOLT(0)	// Voltage rails
            | TSI_GENCS_EXTCHRG(5)	// External osc charge 16 uA
            | TSI_GENCS_PS(7)		// Prescalar divide by 128
            | TSI_GENCS_NSCN(15);	// 16 scans per electrode

	TSI0_GENCS |= TSI_GENCS_TSIEN_MASK; // enable module
	TSI0_DATA = TSI_DATA_TSICH(6) | TSI_DATA_SWTS_MASK;
}
