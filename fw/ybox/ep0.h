/*
 * fw/ep0.h - EP0 extension protocol
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EP0_H
#define	EP0_H

#ifndef	USB_TYPE_VENDOR
#define	USB_TYPE_VENDOR	0x40
#endif

#ifndef	USB_DIR_IN
#define	USB_DIR_IN	0x80
#endif

#ifndef	USB_DIR_OUT
#define	USB_DIR_OUT	0x00
#endif


#define	YBOX_GET	(USB_TYPE_VENDOR | USB_DIR_IN  | 0 << 8)
#define	YBOX_PUT	(USB_TYPE_VENDOR | USB_DIR_OUT | 0 << 8)
#define	YBOX_PEEK	(USB_TYPE_VENDOR | USB_DIR_IN  | 1 << 8)
#define	YBOX_POKE	(USB_TYPE_VENDOR | USB_DIR_OUT | 1 << 8)
#define	YBOX_TOUCH	(USB_TYPE_VENDOR | USB_DIR_IN  | 2 << 8)


void ep0_init(void);

#endif /* !EP0_H */
