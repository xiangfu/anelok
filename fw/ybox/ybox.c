/*
 * fw/ybox.c - Y-Box firmware (KL2x)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <string.h>

#include "misc.h"
#include "led.h"
#include "rf.h"
#include "usb.h"
#include "usb-board.h"
#include "ep0.h"
#include "touch.h"


extern void _start(void);	// newlib C lib initialization
extern char __stack;
extern char __data_start__;
extern char __data_end__;
extern char __etext;


uint8_t rx_buf[128];
uint8_t rx_len;


int main(void)
{
	mdelay(1);	/* let host notice we've detached */

	usb_init();
	usb_begin_device();

	ep0_init();

	touch_init();

	rf_init();

	while (1) {
		usb_poll_device();
		if (rx_len)
			rf_poll(NULL, 0);
		else
			rx_len = rf_poll(rx_buf, sizeof(rx_buf));
	}

#if 0
	bool on = 0;
	int i;

	while (1) {
		led(on);
		on = !on;
		for (i = 0; i != 1000000; i++)
			asm("");
	}
#endif
}


void Reset_Handler(void)    __attribute__((naked, aligned(8)));
void _exit(int);


void __attribute__((naked, section(".reset_init"))) Reset_Handler(void)
{
	/* initialize data segment */
	memcpy(&__data_start__, &__etext, &__data_end__ - &__data_start__);

	_start();
}


void _exit(int status)
{
	while (1);
}
