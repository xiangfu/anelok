/*
 * fw/ui/account.h - Account database (dummy)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ACCOUNT_H
#define	ACCOUNT_H

#include "textsel.h"


enum field_type {
	field_name,
	field_url,
	field_login,
	field_pw
};


/*
 * @@@ these flags are bogus - transmission methods would not be a function
 * of the account entry but of the device status.
 */

enum pw_flags {
	pw_hidden	= 1 << 0,
	pw_inaccessible	= 1 << 1,
	pw_type		= 1 << 2,
	pw_radio	= 1 << 3,
};


struct account_field {
	const char *text;
	enum field_type type;
};


struct account {
	int flags;
	struct account_field fields[];
};


extern const struct textsel_entry accounts[];

#endif /* !ACCOUNT_H */
