/*
 * fw/ui/account.c - Account database (dummy)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>

#include "textsel.h"
#include "account.h"


static struct account account_mail = {
	0,
	{
		{ "Mail", 		field_name	}, // redundant
		{ "foomail.com",	field_url	}, // should be full URL
		{ "me",			field_login	}, // user name
		{ "Fohd3ien",		field_pw	}, // password
		{ NULL }
	}
};

static struct account account_SocNet = {
	pw_type,
	{
		{ "SocNet",		field_name	}, // redundant
		{ "sidetome.com",	field_url	},
		{ "goatee81",		field_login	},
		{ "asdf",		field_pw	},
		{ NULL}
	}
};

static struct account account_e_Bank = {
	pw_hidden | pw_radio,
	{
		{ "e-Bank",		field_name	}, // redundant
		{ "richfolk.sc",	field_url	},
		{ "40331678",		field_login	},
		{ "7r,#[),HQ\\6hWB",	field_pw	},
		{ NULL }
	}
};

static struct account account_ATM = {
	0,
	{
		{ "ATM",		field_name	}, // redundant
		{ "MoneyCard",		field_url	},
		{ "1234",		field_pw	},
		{ NULL }
	}
};

static struct account account_Hosting = {
	pw_inaccessible,
	{
		{ "Hosting",		field_name	}, // redundant
		{ "blah.hostng.sp",	field_url	},
		{ "root",		field_login	},
		{ "Geheim",		field_pw	},
		{ NULL }
	}
};


const struct textsel_entry accounts[] = {
	{ "Mail",		&account_mail },
	{ "SocNet",		&account_SocNet	},
	{ "e-Bank",		&account_e_Bank },
	{ "ATM",		&account_ATM },
	{ "Hosting",		&account_Hosting },
	{ "GitStuff",		NULL },
	{ "Qi-Hw ML",		NULL },
	{ "Building",		NULL },
	{ NULL,			NULL }
};
