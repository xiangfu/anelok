/*
 * fw/2014/touch.h - Touch sensing
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TOUCH_H
#define	TOUCH_H

#include <stdbool.h>
#include <stdint.h>


void touch_read(uint16_t res[4]);
bool touch_filter(uint8_t *pos);
void touch_init(void);

#endif /* !TOUCH_H */
