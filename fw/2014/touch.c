/*
 * fw/2014/touch.c - Touch sensing
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "board.h"
#include "sleep.h"
#include "tick.h"
#include "devcfg.h"
#include "touch.h"


/*
 * PTA1 = TSI0_CH2
 * PTA2 = TSI0_CH3
 */

#define	CHAN_A	2
#define	CHAN_B	3


#define GENCS_VALUE(enable)   \
    ( TSI_GENCS_ESOR_MASK	/* End-of-scan interrupt	*/ \
    | TSI_GENCS_MODE(0)		/* Capactive sensing		*/ \
    | TSI_GENCS_REFCHRG(4)	/* Reference charge 8 uA	*/ \
    | TSI_GENCS_DVOLT(0)	/* Voltage rails		*/ \
    | TSI_GENCS_EXTCHRG(5)	/* External osc charge 16 uA	*/ \
    | TSI_GENCS_PS(7)		/* Prescalar divide by 128	*/ \
    | TSI_GENCS_NSCN(15)	/* 16 scans per electrode	*/ \
    | TSI_GENCS_TSIIEN_MASK	/* generate interrupts		*/ \
    | TSI_GENCS_STPE_MASK	/* enabled in low power mode	*/ \
    | ((enable) ? TSI_GENCS_TSIEN_MASK : 0))


/*
 * This interrupt handler does nothing useful. We'll need it to be able to
 * wait for interrupts also without using the LLWU.
 */

void touch_isr(void)
{
	TSI0_GENCS = GENCS_VALUE(1) | TSI_GENCS_EOSF_MASK;
	NVIC_ClearPendingIRQ(TSI0_IRQn);
}


void touch_read(uint16_t res[4])
{
	uint8_t phase;

	for (phase = 0; phase != 4; phase++) {
		switch (phase) {
		case 0:
			gpio_init_out(CAP_B, 0);
			gpio_init_off(CAP_A);
			/* fall through */
		case 1:
			TSI0_DATA = TSI_DATA_TSICH(CHAN_A) | TSI_DATA_SWTS_MASK;
			break;
		case 2:
			gpio_init_out(CAP_A, 0);
			gpio_init_off(CAP_B);
			/* fall through */
		case 3:
			TSI0_DATA = TSI_DATA_TSICH(CHAN_B) | TSI_DATA_SWTS_MASK;
			break;
		}
		enter_stop(1 << 4);	/* WUME4 == TSI0 */
//		while (!(TSI0_GENCS & TSI_GENCS_EOSF_MASK));
		res[phase] = TSI0_DATA;
		TSI0_GENCS = GENCS_VALUE(1) | TSI_GENCS_EOSF_MASK;
	}
}


#define	POSITIONS	64

/*
 * INIT_CAL_RUNS: in experiments on two devices, values of 4 and 6 were
 * enough.
 */

#define	CAL_RUNS	2000
#define	INIT_CAL_RUNS	20
#define	EXCURSION	25


static struct cal {
	uint32_t avg;
	uint32_t sum;
	uint32_t n;
} cal_a, cal_b;


static int32_t normalize(struct cal *cal, uint32_t v)
{
	if ((v - EXCURSION) * cal->n > cal->sum ||
	    (v + EXCURSION) * cal->n < cal->sum) {
		cal->sum = 0;
		cal->n = 0;
	}
	cal->sum += v;
	cal->n++;
	if (cal->n == CAL_RUNS) {
		cal->avg = cal->sum / CAL_RUNS;
		cal->sum = 0;
		cal->n = 0;
	}
	return v - cal->avg;
}


static void init_cal(void)
{
	uint16_t i;
	uint16_t res[4];
	uint32_t a, b;

	a = b = 0;
	for (i = 0; i != INIT_CAL_RUNS; i++) {
		touch_read(res);
		msleep(1);
	}
	for (i = 0; i != INIT_CAL_RUNS; i++) {
		touch_read(res);
		a += res[0] + res[1];
		b += res[2] + res[3];
		msleep(1);
	}
	cal_a.avg = a / INIT_CAL_RUNS;
	cal_b.avg = b / INIT_CAL_RUNS;
}


#define	DEFAULT_THRESH		190
#define	DEFAULT_SCALE		96	/* POSITIONS*1.5 */
#define	DEFAULT_OFFSET		-10


static bool touch_pos(uint8_t *pos)
{
	uint16_t res[4];
	int32_t a, b;
	uint16_t thresh = devcfg->thresh ? devcfg->thresh : DEFAULT_THRESH;
	uint16_t scale = devcfg->scale ? devcfg->scale : DEFAULT_SCALE;
	uint16_t offset = devcfg->offset ? devcfg->offset : DEFAULT_OFFSET;
	int16_t tmp;

	touch_read(res);
	a = res[0] + res[1];
	b = res[2] + res[3];
	a = normalize(&cal_a, a);
	b = normalize(&cal_b, b);

	if (cal_a.avg == 0 || cal_b.avg == 0)
		return 0;

	if (a + b < thresh)
		return 0;

	tmp = scale * (b - a) / (a + b) + POSITIONS / 2 + offset;

	if (tmp < 0)
		tmp = 0;
	if (tmp >= POSITIONS)
		tmp = POSITIONS - 1;
	*pos = tmp;
	return 1;
}


#define	FILTER	8


bool touch_filter(uint8_t *pos)
{
	static uint8_t samples = 0;
	static uint8_t buf[FILTER];
	uint16_t sum;
	uint8_t tmp, i;

	if (!touch_pos(&tmp)) {
		samples = 0;
		return 0;
	}

	buf[samples++ % FILTER] = tmp;
	if (samples < FILTER)
		return 0;
	sum = 0;
	for (i = 0; i != FILTER; i++)
		sum += buf[(samples - i) % FILTER];
	*pos = sum / FILTER;
	return 1;
}


void touch_init(void)
{
	SIM_SCGC5 |= SIM_SCGC5_TSI_MASK;
	TSI0_GENCS = GENCS_VALUE(0);	// configure module
	TSI0_GENCS = GENCS_VALUE(1);	// enable it
//	TSI0_DATA = TSI_DATA_TSICH(CHAN_A) | TSI_DATA_SWTS_MASK;

	NVIC_ClearPendingIRQ(TSI0_IRQn);
	NVIC_EnableIRQ(TSI0_IRQn);

	init_cal();
}
