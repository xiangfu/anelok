/*
 * fw/2014/ep0.c - EP0 extension protocol
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "usb.h"
#include "dfu.h"

#include "board.h"
#include "misc.h"
#include "event.h"
#include "touch.h"
#include "hid.h"
#include "ep0.h"


uint8_t anelok_mode = 0;	/* 0 = keep looping */


static bool anelok_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;
	static uint16_t touch_res[4];

	switch (req) {
	case ANELOK_TOUCH:
		touch_read(touch_res);
		usb_send(&eps[0], touch_res, 8, NULL, NULL);
		return 1;
	case ANELOK_MODE:
		anelok_mode = setup->wValue;
		return 1;
	default:
		return 0;
	}
}


static bool dfu_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;

	switch (req) {
	case DFU_TO_DEV(DFU_DETACH):
		/* @@@ should use wTimeout */
		dfu.state = appDETACH;
		return 1;
	default:
		return dfu_setup_common(setup);
	}
}


static bool my_setup(const struct setup_request *setup)
{
	switch (setup->wIndex) {
	case 0:
		return anelok_setup(setup);
	case 1:
		if (hid_setup(setup)) {
			event_set_interrupt(ev_usb_hid_setup);
			return 1;
		}
		return 0;
	case 2:
		return dfu_setup(setup);
	default:
		return 0;
	}
}


static bool my_get_descriptor(uint8_t type, uint8_t index,
    const uint8_t **reply, uint8_t *size)
{
	if (hid_get_descriptor(type, index, reply, size))
		return 1;
	return dfu_my_descr(type, index, reply, size);
}


static void my_reset(void)
{
	if (dfu.state == appDETACH)
		reset_cpu();
}


void ep0_init(void)
{
	dfu.state = appIDLE;
	user_setup = my_setup;
	user_get_descriptor = my_get_descriptor;
	user_reset = my_reset;
}
