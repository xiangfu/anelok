/*
 * fw/cc.c - CC25xx debug-mode access
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "misc.h"
#include "gpio.h"
#include "rf.h"

#include "ccdbg.h"

#include "board.h"
#include "cc2543.h"
#include "cc.h"

/* @@@ Y-Box calls it RF_VDD, Anelok calls it VRF. Consolidate later. */

#ifndef RF_VDD
#define	RF_VDD	VRF
#endif


struct ccdbg ccdbg;


/* ----- GPIO management --------------------------------------------------- */


static void cc_begin(void)
{
	gpio_begin(RF_nRESET);
	gpio_begin(RF_DD);
	gpio_begin(RF_DC);
}


static void cc_end(void)
{
	gpio_end(RF_nRESET);
	gpio_end(RF_DD);
	gpio_end(RF_DC);
}


void cc_init(void)
{
	gpio_init_out(RF_nRESET, 0);
	gpio_init_out(RF_DD, 0);
	gpio_init_out(RF_DC, 0);

	gpio_init_in(RF_VDD, 0);
}


/* ----- 2 MHz clock output ------------------------------------------------ */


void cc_clock(void)
{
	/*
	 * Clock generation:
	 *
	 * Crystal -> 32 MHz,
	 * prescaler / 8 -> 4 MHz,
	 * count T3 modulo 1 (/1) -> 4 MHz,
	 * toggle output on match (with 0, reset default for T3CC1) -> 2 MHz
	 */

	/* Select 32 MHz XOSC for clk/tick, keep 32 kHz RCOSC for 32 kHz */

	ccdbg_write_reg(&ccdbg, CLKCONCMD_REG, CLKCONCMD_OSC32K);

	/* Timer 3, CH1, alt 1, on P1_3 */

	ccdbg_write_reg(&ccdbg, P1SEL_REG, 1 << 3);

	/* Make T3 output 2 MHz on CH1 */

	ccdbg_write_reg(&ccdbg, T3CTL_REG, T3CTL_CLR);

	ccdbg_write_reg(&ccdbg, T3CCTL1_REG,
	    T3CCTL1_MODE		/* compare mode */
	    | T3CCTL1_CMP_TOG_CMP);	/* toggle */

	ccdbg_write_reg(&ccdbg, T3CC0_REG, 0);	/* count modulo 1, aka / 1 */

	ccdbg_write_reg(&ccdbg, T3CTL_REG,
	    T3CTL_MODE_MOD	/* run 0 - T3CC0 */
	    | T3CTL_CLR		/* clear counter */
	    | T3CTL_START	/* start timer */
	    | T3CTL_DIV_8);	/* tick / 8 = 4 MHz */

	/*
	 * The CC2543 data sheet says under "32-MHz CRYSTAL OSCILLATOR" that
	 * the crystal oscillator needs about 0.25 ms to stabilize.
	 */

	mdelay(1);
}


/* ----- LED control ------------------------------------------------------- */


void cc_led(bool on)
{
	ccdbg_write_reg(&ccdbg, P1DIR_REG, 1);
	if (on)
		ccdbg_write_reg(&ccdbg, P1_REG, 0x1f);
	else
		ccdbg_write_reg(&ccdbg, P1_REG, 0x1e);
}


/* ----- rfkill status ----------------------------------------------------- */


bool cc_on(void)
{
	bool on;

	gpio_begin(RF_VDD);
	on = gpio_read(RF_VDD);
	gpio_end(RF_VDD);
	return on;
}


/* ----- Debug access ------------------------------------------------------ */


static void cc_pulse(void *ctx)
{
	gpio_set(RF_DC);
	gpio_clr(RF_DC);
}


static bool cc_sample(void *ctx)
{
	gpio_input(RF_DD);
udelay(1);
	return gpio_read(RF_DD);
}


static void cc_send(void *ctx, uint8_t data)
{
	uint8_t i;

	gpio_output(RF_DD);
	for (i = 0; i != 8; i++) {
		if ((data << i) & 0x80)
			gpio_set(RF_DD);
		else
			gpio_clr(RF_DD);
		cc_pulse(NULL);
	}
}


static uint8_t cc_recv(void *ctx)
{
	uint8_t val = 0;
	uint8_t i;

	gpio_input(RF_DD);
udelay(1);
	for (i = 0; i != 8; i++) {
		cc_pulse(NULL);
		val = (val << 1) | gpio_read(RF_DD);
	}
	return val;

}


static void cc_reset(void *ctx, bool active)
{
	if (active)
		gpio_clr(RF_nRESET);
	else
		gpio_set(RF_nRESET);
}


static void cc_report(const char *fmt, ...)
{
}


static const struct ccdbg_ops dbg_ops = {
	.send	= cc_send,
	.recv	= cc_recv,
	.sample	= cc_sample,
	.pulse	= cc_pulse,
	.reset	= cc_reset,
	.report	= cc_report,
};


bool cc_acquire(void)
{
	/*
	 * The CC2543 data sheet does not specify how soon after power-up the
	 * device will respond to debug commands. The 16 MHz RC oscillator
	 * should be running and finish calibration within about 60 us. (See
	 * section "16-MHz RC OSCILLATOR".)
	 *
	 * The wake-up time from low-power modes 2 and 3 to Active is given as
	 * 130 us in "GENERAL CHARACTERISTIC".
	 *
	 * Experiments show that no delay is needed, but we prefer on the safe
	 * side.
	 */

	cc_begin();

	mdelay(1);
	gpio_set(RF_nRESET);

	if (!ccdbg_open(&ccdbg, &dbg_ops, NULL))
		return 0;

	return 1;
}


void cc_release(void)
{
	gpio_clr(RF_nRESET);
	mdelay(1);	/* we need only 1 us */
	gpio_set(RF_nRESET);

	cc_end();
}
