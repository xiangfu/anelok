/*
 * fw/cc/op.h - CC254x operations
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_OP_H
#define	CC_OP_H

/*
 * Message format is:
 *
 * 0x80 | len, code, p1, p2, ...
 *
 * Response:
 *
 * len, r1, r2, ...
 *
 * The pi and ri lists can be empty.
 */

enum {
	OP_NULL,	/* reserved */
	OP_POKE,	/* xram[p1 | p2 << 8] = p3 */
	OP_PEEK,	/* r1 = xram[p1 | p2 << 8] */
	OP_LED,		/* control the LEDs (@@@ for development only) */
	OP_RADIO,	/* send tone at (p1 | p2 << 8) MHz */
};

#endif /* !CC_OP_H */
