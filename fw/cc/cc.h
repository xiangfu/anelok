/*
 * fw/cc.h - CC25xx debug-mode access
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_H
#define	CC_H

#include <stdbool.h>

#include "ccdbg.h"


extern struct ccdbg ccdbg;


void cc_clock(void);
void cc_led(bool on);

bool cc_on(void);

bool cc_acquire(void);
void cc_release(void);
void cc_init(void);

#endif /* !CC_H */
