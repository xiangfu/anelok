/*
 * fw/ccflash.c - Board-specific flash functions (CC25xx side)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "dfu.h"
#include "ccdbg.h"
#include "cc.h"
#include "flash.h"


#define	FW_BASE		0x8000
#define	FW_SIZE		0x8000


static uint32_t payload;


static void flash_start(void)
{
	payload = 0;
}


static bool flash_can_write(uint16_t size)
{
	if (size & 3)
		return 0;
	return payload + size <= FW_SIZE;
}


static void flash_write(const uint8_t *buf, uint16_t size)
{
	/* @@@ weird */
	if (!size)
		size = EP0_SIZE;

	if (!payload)
		ccdbg_erase(&ccdbg);
	ccdbg_flash_block(&ccdbg, buf, payload, size);
	payload += size;
}


static void flash_end_write(void)
{
}


static uint16_t flash_read(uint8_t *buf, uint16_t size)
{
	if (payload + size > FW_SIZE)
		size = FW_SIZE - payload;
	ccdbg_read_block(&ccdbg, buf, FW_BASE + payload, size);
	payload += size;
	return size;
}


struct dfu_flash_ops cc_flash_ops = {
	.start		= flash_start,
	.can_write	= flash_can_write,
	.write		= flash_write,
	.end_write	= flash_end_write,
	.read		= flash_read,
};
