/*
 * fw/rf.h - RF transceiver
 *
 * Written 2013, 2014 by Werner Almesberger
 * Copyright 2013, 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef RF_H
#define	RF_H

#include <stdbool.h>
#include <stdint.h>


/* atrf */

uint8_t rf_rng(void); /* value returned is 2 bits, thus 0-3 */
const char *rf_start(void);

/* cc */

uint8_t rf_poll(void *buf, uint8_t size);
bool rf_send(const void *buf, uint8_t cmd);

/* common */

void rf_init(void);

#endif /* !RF_H */
