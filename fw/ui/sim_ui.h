/*
 * fw/sim_ui.h - Direct access to the UI for the simulator
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SIM_UI_H
#define	SIM_UI_H

enum jump_to {
	jump_top,
	jump_middle,
	jump_bottom,
};

#endif /* !SIM_UI_H */
