/*
 * fw/ui_select.c - UI: account selection
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "textsel.h"
#include "account.h"
#include "ui.h"


static struct textsel textsel;


/* ----- Callbacks --------------------------------------------------------- */


static bool select_select(void *user, void *user_entry)
{
	if (!user_entry)
		return NULL;
	ui_account(user_entry);
	return 1;
}


static const struct textsel_ops select_ts_ops = {
	.setup		= ui_select_setup,
	.select		= select_select,
};


bool ui_select_proceed(void *dummy)
{
	pm_set_policy(&pm_policy_default);
	textsel_proceed(&textsel);
	return 1;
}


void ui_select(void)
{
	pm_set_policy(&pm_policy_default);
	textsel_init(&textsel, &select_ts_ops, NULL, accounts);
}


/* ----- Simulator support ------------------------------------------------- */


void sim_show(void)
{
	sim_ts_show(&textsel);
}


void sim_jump(enum jump_to whence)
{
	sim_sel_jump(&textsel.sel, whence);
}
