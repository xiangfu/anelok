/*
 * fw/ui_account.c - UI: account access
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "qa.h"
#include "pm.h"
#include "display.h"
#include "text.h"
#include "icon.h"
#include "sel.h"
#include "std.h"
#include "account.h"
#include "comm.h"
#include "ui.h"


#define	LINE_Y		11
#define	TEXT_X		7
#define	MARKER_Y	0


static bool reveal;

static const struct account_field *curr;
static struct std std;
static struct sel sel;
static uint8_t last;	/* delta at last swipe */


/* ----- Callbacks --------------------------------------------------------- */


static inline const struct account_field *current_field(
    const struct account *acct)
{
	return acct->fields + sel_cursor(&sel);
}


static inline enum field_type current_field_type(const struct account *acct)
{
	return current_field(acct)->type;
}


/* ----- Selection operations ---------------------------------------------- */


static void account_rewind(void *user)
{
	struct account *acct = user;

	curr = acct->fields;
}


static void account_prev(void *user)
{
	curr--;
}


static bool account_next(void *user)
{
	if (!curr[1].text)
		return 0;
	curr++;
	return 1;
}


static void hline(uint8_t x0, uint8_t x1, int8_t y)
{
	if (y < 0 || y >= FB_Y)
		return;
	while (x0 <= x1) {
		display_set(x0, y);
		x0++;
	}
}


static void account_display(void *user, int8_t y, bool at_cursor)
{
	struct account *acct = user;
	uint8_t x = main_x0 + TEXT_X;

	if (at_cursor)
		icon_draw(&icon_current, main_x0, y + MARKER_Y);
	switch (curr->type) {
	case field_name:
		hline(main_x0, main_x1, y + LINE_Y);
		/* fall through */
	case field_url:
		text_str(&font_medium, curr->text, x, y);
		return;
	case field_login:
		text_str(&font_medium, "User", x, y);
		break;
	case field_pw:
		text_str(&font_medium, "Password", x, y);
		if (acct->flags & pw_inaccessible)
			return;
		if ((acct->flags & pw_hidden) && !reveal)
			return;
		break;
	default:
		oops();
	}
	text_str(&font_medium, curr->text, x, y + LINE_Y);
}


static uint8_t account_size(void *user)
{
	switch (curr->type) {
	case field_name:
		return LINE_Y + 2;
	case field_url:
		return LINE_Y;
	default:
		return LINE_Y * 2;
	}
}


static struct sel_ops account_sel_ops = {
	.rewind	=	account_rewind,
	.prev	=	account_prev,
	.next	=	account_next,
	.size	=	account_size,
	.display =	account_display,
};


/* ----- Input actions ----------------------------------------------------- */


static void account_show(void *user)
{
	display_clear();
        sel_show(&sel);
}


static bool account_tap(enum area area, void *user)
{
	switch (area) {
	case area_top:
		sel_up(&sel);
		break;
	case area_middle:
		break;
	case area_bottom:
		sel_down(&sel);
		break;
	default:
		break;
	}
	return 0;
}


static bool account_middle_press(void *user)
{
	const struct account *acct = user;
	enum field_type field = current_field_type(acct);

	if (field != field_pw)
		return 0;
	if ((acct->flags & pw_hidden) && !reveal) {
		reveal = 1;
		return 0;
	}

	switch (comm_mode(acct, current_field(acct))) {
	case ui_comm_hid:
		return comm_send(acct, current_field(acct));
	default:
		break;
	}

	if (acct->flags & pw_type) {
		ui_sorry();
		return 1;
	}
	if (acct->flags & pw_radio) {
		ui_sorry();
		return 1;
	}
	if (acct->flags & pw_inaccessible) {
		ui_sorry();
		return 1;
	}
	return 0;
}


static const struct icon *account_middle_icon(bool bold, void *user)
{
	const struct account *acct = user;
	enum field_type field = current_field_type(acct);

	if (field != field_pw)
		return 0;
	if ((acct->flags & pw_hidden) && !reveal)
		return bold ? &icon_eye_bold : &icon_eye;

	switch (comm_mode(acct, current_field(acct))) {
	case ui_comm_hid:
		return bold ? &icon_keyboard_bold : &icon_keyboard;
	default:
		break;
	}

#if 0
	if (acct->flags & pw_type)
		return bold ? &icon_keyboard_bold : &icon_keyboard;
#endif
	if (acct->flags & pw_radio)
		return bold ? &icon_radio_bold : &icon_radio;
	if (acct->flags & pw_inaccessible)
		return bold ? &icon_exchange_bold : &icon_exchange;
	return NULL;
}


static void account_swipe_begin(enum area area, void *user)
{
	last = 0;
}


static bool account_swipe_move(int8_t delta, void *user)
{
	bool change;

	change = sel_swipe(&sel, delta - last);
	last = delta;
	return change;
}


static const struct std_ops account_std_ops = {
	.show		= account_show,
	.back		= ui_select_proceed,
	.setup		= ui_sorry_dummy,	// ui_account_setup
	.tap		= account_tap,
	.middle_press	= account_middle_press,
	.middle_icon	= account_middle_icon,
	.swipe_begin	= account_swipe_begin,
	.swipe_move	= account_swipe_move,
};


/* ----- Initialization ---------------------------------------------------- */


void ui_account(struct account *acct)
{
	reveal = 0;

	pm_set_policy(&pm_policy_peruse);
	account_rewind(acct);
	sel_init(&sel, &account_sel_ops, acct);
	std_init(&std, &account_std_ops, acct);
}
