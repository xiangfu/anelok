/*
 * fw/ui/textsel.h - Selection from text list
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TEXTSEL_H
#define	TEXTSEL_H

#include "std.h"
#include "sel.h"


struct textsel_entry {
	const char *text;	/* NULL if last entry */
	void *user_entry;
};


struct textsel_ops {
	bool (*have_back)(void *user);
	bool (*have_setup)(void *user);

	bool (*back)(void *user);
	bool (*setup)(void *user);
	bool (*select)(void *user, void *user_entry);

	void (*suspend)(void *user, void *user_entry);
	void (*resume)(void *user, void *user_entry);
};


struct textsel {
	const struct textsel_ops *ops;
	void *user;
	const struct textsel_entry *list;

	struct std std;
	struct sel sel;

	const struct textsel_entry *curr;	/* for list travel */
	uint8_t last;				/* delta at last swipe */
};


void textsel_proceed(struct textsel *ctx);
void textsel_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, const struct textsel_entry *list);

void sim_ts_show(struct textsel *ctx);

#endif /* !TEXTSEL_H */
