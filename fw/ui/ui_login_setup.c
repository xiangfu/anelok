/*
 * fw/ui_login_setup.c - UI: administrative tasks on the login screen
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "qa.h"
#include "misc.h"
#include "pm.h"
#include "display.h"
#include "console.h"
#include "input.h"
#include "icon.h"
#include "textsel.h"
#include "ui.h"


static bool rotated = 0;
static struct textsel textsel;


static void proceed(void)
{
	textsel_proceed(&textsel);
}


/* ----- Task: left/right -------------------------------------------------- */


static bool toggle_hand(void)
{
	rotated = !rotated;
	display_rotate(!rotated);
	input_rotate(rotated);
	icon_rotate(rotated);
	return 0;
}


/* ----- Tasks: version ---------------------------------------------------- */


#include "version.h"


static void do_version(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("#%d\n%s\n%x%s\n", build_number, build_date,
	    build_hash, build_dirty ? "+" : "");
	console_update();
}


static bool version(void)
{
	ui_show(do_version, NULL, proceed);
	return 1;
}



/* ----- Tasks: identify --------------------------------------------------- */


#include "devcfg.h"
#include "id.h"


static void do_identify(void *user)
{
	struct id id;

	id_get(&id);
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("\"%s\"\n",
	    devcfg->name ? devcfg->name : "unknown");
	console_printf("ID: %04X\n%08X\n%08X\n",
	    id.uidmh, id.uidml, id.uidl);
	console_update();
}


static bool identify(void)
{
	ui_show(do_identify, NULL, proceed);
	return 1;
}


/* ----- Task: LED --------------------------------------------------------- */


#include "led.h"
#include "tick.h"


static bool toggle_led(void)
{
	led_toggle();
	return 0;
}


static bool blink_led(void)
{
	uint8_t i;

	for (i = 0; i != 10; i++) {
		msleep(200);
		led_toggle();
	}
	return 0;
}


/* ----- Task: USB --------------------------------------------------------- */


#include "vusb.h"
#include "usb-board.h"


static void do_usb(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("USB: %s\n",
	    vusb_sense() ? "powered" : "no power");
	console_printf("%s role", usb_a() ? "host" : "device");
}


static bool usb(void)
{
	ui_show(do_usb, NULL, proceed);
	return 1;
}


/* ----- Task: type on HID ------------------------------------------------- */


#include "hid.h"
#include "hid-type.h"


static bool type(void)
{
	static struct hid_type_ctx ctx;

	hid_type(&ctx, "Hello, world !", NULL, NULL);
	return 0;
}


/* ----- Task: RF ---------------------------------------------------------- */


#include "ccdbg.h"
#include "cc.h"


static void do_rf(void *user)
{
	uint16_t id;

	console_window(main_x0, main_x1);
	console_clear();
	console_printf("RF: ");
	if (cc_on()) {
		if (!cc_acquire()) {
			console_printf("unresponsive\n");
		} else {
			id = ccdbg_get_chip_id(&ccdbg);
			cc_release();
			console_printf("CC25%02x\n    version 0x%02x\n",
			    id >> 8, id & 0xff);
		}
	} else {
		console_printf("off\n");
	}
	console_update();
}


static bool rf(void)
{
	ui_show(do_rf, NULL, proceed);
	return 1;
}


/* ----- Task: RF ---------------------------------------------------------- */


#include "fmt.h"
#include "mmc-hw.h"
#include "mmc.h"


static char clean_ascii(uint8_t c)
{
	return c >= ' ' && c < 127 ? c : '?';
}


/*
 * CID:
 * 0     Manufacturer ID (MID), apparently not publicly documented [1]
 * 1-2   OEM/Application ID (OID), two ASCII characters
 * 3-8   Product Name (PNM), 5 ASCII characters
 * 9     Product Revision (PRV), two BCD digits
 *
 * [1] http://www.bunniestudios.com/blog/?p=2297
 */

/*
 * Decoded format:
 * "OI PNM__ P.R\0"
 */

static void decode_cid(const uint8_t cid[16], char res[13])
{
	char *s = res;
	uint8_t i;

	for (i = 1; i != 3; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' '; 
	for (i = 3; i != 8; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' '; 
	print_number(s++, cid[9] >> 4, 1, 16);
	*s++ = '.';
	print_number(s++, cid[9] & 15, 1, 16);
	*s = 0;
}


static void id_memcard(void)
{
	uint8_t cid[16];
	char buf[13];


	if (!card_present()) {
		console_printf("No card");
		return;
	}

	if (!mmc_init()) {
		console_printf("Card failed");
		return;
	}
        if (!mmc_read_cid(cid)) {
		console_printf("Card failed");
		return;
	}
	decode_cid(cid, buf);
	console_printf("%s", buf);
}


static void do_memcard(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	id_memcard();
	console_update();
}


static bool memcard(void)
{
	ui_show(do_memcard, NULL, proceed);
	return 1;
}


/* ----- Task: dump memory card -------------------------------------------- */

/*
 * @@@ For design verification only. Note that the "Memory card" function has
 * to be invoked before trying to dump.
 */


static bool dump_once;


static void do_dump_card(void *user)
{
	uint8_t buf[512];
	uint32_t block;
	uint16_t i;
	uint8_t c;

	if (!dump_once)
		return;
	dump_once = 0;
	for (block = 0; block != 100; block++) {
		console_clear();
		console_printf("%u", block);
		console_update();
		msleep(100);
		if (!mmc_begin_read(block << 9)) {
			console_printf("mmc_begin_read failed");
			break;
		}
		for (i = 0; i != MMC_BLOCK; i++)
			buf[i] = mmc_read();
		mmc_end_read();
		msleep(100);
		for (i = 0; i != MMC_BLOCK; i++) {
			c = buf[i];
			if (!c || c == 0xff)
				break;
			console_char(c < ' ' || c >= 0x7f ? '?' : c);
		}
		console_update();
		msleep(200);
	}
	console_update();
}


static bool dump_card(void)
{
	dump_once = 1;
	ui_show(do_dump_card, NULL, proceed);
	return 1;
}


/* ----- Task: leave UI ---------------------------------------------------- */


static bool do_leave_ui(void)
{
	leave_ui = 1;
	return 1;
}


/* ----- Tasks ------------------------------------------------------------- */


static const struct textsel_entry tasks[] = {
	{ "Right/Left",		toggle_hand	},
	{ "Version",		version		},
	{ "Identify",		identify	},
	{ "USB",		usb		},
	{ "Memory card",	memcard		},
	{ "Dump card",		dump_card	},
	{ "RF",			rf		},
	{ "Type on HID",	type		},
	{ "LED on/off",		toggle_led	},
	{ "LED blink",		blink_led	},
	{ "Leave UI",		do_leave_ui	},
	{ "Reboot",		reset_cpu	},
	{ NULL,			NULL		},
};


/* ----- Callbacks --------------------------------------------------------- */


static bool login_setup_back(void *user)
{
	ui_login();
	return 1;
}


static bool login_setup_select(void *user, void *user_entry)
{
	bool (*fn)(void) = user_entry;

	return fn();
}


static void login_setup_suspend(void *user, void *user_entry)
{
	ui_off();
}


static const struct textsel_ops login_setup_ts_ops = {
	.back		= login_setup_back,
	.select		= login_setup_select,
	.suspend	= login_setup_suspend,
};


void ui_login_setup(void)
{
	pm_set_policy(&pm_policy_default);
	textsel_init(&textsel, &login_setup_ts_ops, NULL, tasks);
}
