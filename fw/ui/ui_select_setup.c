/*
 * fw/ui_select_setup.c - UI: administrative tasks at account selection
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "textsel.h"
#include "ui.h"


static struct textsel textsel;


/* ----- Task: not implemented --------------------------------------------- */


static bool sorry(void)
{
	ui_sorry();
	return 1;
}


/* ----- Tasks ------------------------------------------------------------- */


static const struct textsel_entry tasks[] = {
	{ "Add account",	sorry		},
	{ "Add directory",	sorry		},
//	{ "Delete directory",	sorry		},	if in subdirectory
	{ "Change code",	sorry		},
	{ NULL,			NULL		},
};


/* ----- Callbacks --------------------------------------------------------- */


static bool select_setup_back(void *user)
{
	ui_select();
	return 1;
}


static bool select_setup_select(void *user, void *user_entry)
{
	bool (*fn)(void) = user_entry;

	return fn();
}


static const struct textsel_ops select_setup_ts_ops = {
	.back		= select_setup_back,
	.select		= select_setup_select,
};


bool ui_select_setup(void *user)
{
	pm_set_policy(&pm_policy_default);
	textsel_init(&textsel, &select_setup_ts_ops, NULL, tasks);
	return 1;
}
