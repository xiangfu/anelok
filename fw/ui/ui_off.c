/*
 * fw/ui_off.c - UI: pseudo-UI to simulate device off
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>

#include "pm.h"
#include "display.h"
#include "icon.h"
#include "input.h"
#include "auth.h"
#include "ui.h"


bool ui_is_on;


/* ----- Input callbacks --------------------------------------------------- */


static void off_begin(enum area area, void *user)
{
	if (area == area_middle) {
		pm_busy();
		ui_is_on = 1;
		icon_draw(&icon_banner,
		    (FB_X - icon_banner.w) >> 1, (FB_Y - icon_banner.h) >> 1);
	}
}


static void off_cancel(enum area area, void *user)
{
	if (area == area_middle)
		ui_off();
}


static void off_press(enum area area, void *user)
{
	if (area == area_middle) {
		if (auth_valid())
			ui_select();
		else
			ui_login();
	}
}


static const struct input_ops off_ops = {
	.begin		= off_begin,
	.cancel		= off_cancel,
	.press		= off_press,
};


void ui_off(void)
{
	input_select(&off_ops, NULL);
	ui_is_on = 0;
	pm_sleep(PM_OFF);
}
