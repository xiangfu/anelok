/*
 * fw/ui/textsel.c - Selection from text list
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "display.h"
#include "icon.h"
#include "text.h"
#include "std.h"
#include "sel.h"
#include "textsel.h"


#define	LINE_Y		11
#define	TEXT_X		7
#define	MARKER_Y	0


/* ----- Selection operations ---------------------------------------------- */


static void textsel_rewind(void *user)
{
	struct textsel *ctx = user;

	ctx->curr = ctx->list;
}


static void textsel_prev(void *user)
{
	struct textsel *ctx = user;

	ctx->curr--;
}


static bool textsel_next(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->curr[1].text)
		return 0;
	ctx->curr++;
	return 1;
}


static uint8_t textsel_size(void *user)
{
	return LINE_Y;
}


static void textsel_display(void *user, int8_t y, bool at_cursor)
{
	struct textsel *ctx = user;

	if (at_cursor)
		icon_draw(&icon_current, main_x0, y + MARKER_Y);
	text_str(&font_medium, ctx->curr->text, main_x0 + TEXT_X, y);
}


static struct sel_ops textsel_sel_ops = {
	.rewind	=	textsel_rewind,
	.prev	=	textsel_prev,
	.next	=	textsel_next,
	.size	=	textsel_size,
	.display =	textsel_display,
};


/* ----- Display update ---------------------------------------------------- */


static inline void *current_user_entry(struct textsel *ctx)
{
	return ctx->list[sel_cursor(&ctx->sel)].user_entry;
}


/* ----- Standard interface actions ---------------------------------------- */


static void textsel_show(void *user)
{
	struct textsel *ctx = user;

	display_clear();
	sel_show(&ctx->sel);
}


static bool textsel_have_back(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->back)
		return 0;
	if (ctx->ops->have_back)
		return ctx->ops->have_back(ctx->user);
	return 1;
}


static bool textsel_have_setup(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->setup)
		return 0;
	if (ctx->ops->have_setup)
		return ctx->ops->have_setup(ctx->user);
	return 1;
}


static bool textsel_back(void *user)
{
	struct textsel *ctx = user;

	return ctx->ops->back(ctx->user);
}


static bool textsel_setup(void *user)
{
	struct textsel *ctx = user;

	return ctx->ops->setup(ctx->user);
}


static bool textsel_tap(enum area area, void *user)
{
	struct textsel *ctx = user;

	switch (area) {
	case area_top:
		sel_up(&ctx->sel);
		break;
	case area_middle:
		/* @@@ ignore */
		break;
	case area_bottom:
		sel_down(&ctx->sel);
		break;
	default:
		break;
	}
	return 0;
}


static bool textsel_middle_press(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->select)
		return ctx->ops->select(ctx->user, current_user_entry(ctx));
	return 0;
}


static const struct icon *textsel_middle_icon(bool bold, void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->select)
		return NULL;
	return bold ? &icon_next_bold : &icon_next;
}


static void textsel_swipe_begin(enum area area, void *user)
{
	struct textsel *ctx = user;

	ctx->last = 0;
}


static bool textsel_swipe_move(int8_t delta, void *user)
{
	struct textsel *ctx = user;
	bool change;

	change = sel_swipe(&ctx->sel, delta - ctx->last);
	ctx->last = delta;
	return change;
}


static void textsel_suspend(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->suspend)
		ctx->ops->suspend(ctx->user, current_user_entry(ctx));
}


static void textsel_resume(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->resume)
		ctx->ops->resume(ctx->user, current_user_entry(ctx));
}


static const struct std_ops textsel_std_ops = {
	.show		= textsel_show,
	.have_back	= textsel_have_back,
	.have_setup	= textsel_have_setup,
	.back		= textsel_back,
	.setup		= textsel_setup,
	.tap		= textsel_tap,
	.middle_press	= textsel_middle_press,
	.middle_icon	= textsel_middle_icon,
	.swipe_begin	= textsel_swipe_begin,
	.swipe_move	= textsel_swipe_move,
	.suspend	= textsel_suspend,
	.resume		= textsel_resume,
};


/* ----- Initialization ---------------------------------------------------- */


void textsel_proceed(struct textsel *ctx)
{
	std_init(&ctx->std, &textsel_std_ops, ctx);
}


void textsel_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, const struct textsel_entry *list)
{
	ctx->ops = ops;
	ctx->user = user;
	ctx->list = ctx->curr = list;

	sel_init(&ctx->sel, &textsel_sel_ops, ctx);
	textsel_proceed(ctx);
}


/* ----- Simulator support ------------------------------------------------- */


void sim_ts_show(struct textsel *ctx)
{
	sim_std_show(&ctx->std);
}
