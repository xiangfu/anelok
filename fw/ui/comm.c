/*
 * fw/ui/comm.c - Communication handler
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "event.h"
#include "input.h"
#include "account.h"
#include "hid-type.h"
#include "comm.h"


enum comm comm = comm_none;


static volatile bool comm_busy = 0;


static struct hid_type_ctx type;


static void clear_busy(void *user)
{
	comm_busy = 0;
	event_set_interrupt(ev_refresh);
}


bool comm_send(const struct account *acct,
    const struct account_field *field)
{
	if (field->type != field_pw)
		return 0;
	switch (comm) {
	case comm_hid:
		comm_busy = 1;
		hid_type(&type, field->text, clear_busy, NULL);
		return 1;
	default:
		return 0;
	}
}


enum comm comm_mode(const struct account *acct,
    const struct account_field *field)
{
	if (comm_busy)
		return comm_none;
	return comm;
}


void comm_update(enum comm new)
{
	comm = new;
	input_refresh();
}
