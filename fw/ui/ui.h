/*
 * fw/ui.h - User interface elements
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef UI_H
#define	UI_H

#include <stdbool.h>

#include "sim_ui.h"
#include "account.h"


extern bool leave_ui;
extern bool ui_is_on;

extern enum ui_comm {
	ui_comm_none	= 0,
	ui_comm_hid,
} ui_comm;


void ui_off(void);
void ui_login(void);
void ui_login_setup(void);
void ui_select(void);
bool ui_select_proceed(void *dummy);	// callback format
bool ui_select_setup(void *dummy);	// callback format
void ui_account(struct account *acct);
void ui_show(void (*show)(void *user), void *user, void (*back)(void));

void ui_sorry(void);
bool ui_sorry_dummy(void *dummy);	// callback format

bool ui(void);
void ui_init(void);

void sim_show(void);
void sim_jump(enum jump_to whence);

#endif /* !UI_H */
