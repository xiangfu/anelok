/*
 * fw/ui/sel.h - Selection (generic)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SEL_H
#define	SEL_H

#include <stdbool.h>
#include <stdint.h>

#include "sim_ui.h"


enum scroll_mode {
	smooth_and_centered,
	smooth_but_wrong,
	bumpy_and_correct,
};


struct sel_ops {
	void (*rewind)(void *user);	/* optional */
	void (*prev)(void *user);
	bool (*next)(void *user);
	uint8_t (*size)(void *user);
	void (*display)(void *user, int8_t y, bool at_cursor);
};

struct sel {
	const struct sel_ops *ops;	/* list operations */
	void *user;		/* user context */

	/* movement inside the list */
	uint16_t pos;		/* present position */

	uint8_t span;		/* distance we can travel */
	uint8_t height;		/* total height */

	uint16_t cursor;	/* currently selected item */
	uint16_t curr_y;	/* virtual y position */
};


void sel_init(struct sel *sel, const struct sel_ops *ops, void *user);
void sel_show(struct sel *sel);
uint16_t sel_cursor(struct sel *sel);
bool sel_up(struct sel *sel);
bool sel_down(struct sel *sel);
bool sel_swipe(struct sel *sel, int8_t delta);

void sim_scroll_mode(enum scroll_mode mode);
void sim_sel_jump(struct sel *sel, enum jump_to whence);

#endif /* !SEL_H */
