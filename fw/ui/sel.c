/* * fw/ui/sel.c - Selection
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "qa.h"
#include "display.h"
#include "sel.h"


static enum scroll_mode scroll_mode = smooth_and_centered;


/* ----- Helper functions -------------------------------------------------- */


static void prev(struct sel *sel)
{
	REQUIRE(sel->pos);
	sel->ops->prev(sel->user);
	sel->pos--;
}


static bool next(struct sel *sel)
{
	bool ok;

	ok = sel->ops->next(sel->user);
	if (ok)
		sel->pos++;
	return ok;
}


static uint8_t size(struct sel *sel)
{
	return sel->ops->size(sel->user);
}


static bool go_to(struct sel *sel, uint16_t n)
{
	while (n < sel->pos)
		prev(sel);
	while (n > sel->pos)
		if (!next(sel))
			return 0;
	return 1;
}


static void rewind(struct sel *sel)
{
	if (sel->ops->rewind) {
		sel->ops->rewind(sel->user);
		sel->pos = 0;
	} else {
		go_to(sel, 0);
	}
}


/* ----- Draw screen content ----------------------------------------------- */


static void redraw(struct sel *sel, int16_t y)
{
	rewind(sel);
	while (1) {
		uint8_t h;

		h = size(sel);
		if (y + h >= 0)
			sel->ops->display(sel->user, y,
			    sel->pos == sel->cursor);
		if (!next(sel))
			break;
		y += h;
		if (y >= FB_Y)
			break;
	}
}


static void show_top(struct sel *sel, uint8_t curr_h, int8_t yoff)
{
	int16_t y;

	if (scroll_mode == smooth_but_wrong)
		y = -yoff;
	else
		y = yoff;

	redraw(sel, y);
}


static void show_middle(struct sel *sel, uint8_t curr_h, int8_t yoff)
{
	int16_t y;

	if (scroll_mode == smooth_and_centered ||
	    scroll_mode == smooth_but_wrong)
		yoff = 0;

	y = (FB_Y >> 1) - (curr_h >> 1) - sel->curr_y + 2 * yoff;
	redraw(sel, y);
}


static void show_bottom(struct sel *sel, uint8_t curr_h, int8_t yoff)
{
	int16_t y;

	if (scroll_mode == smooth_but_wrong)
		y = FB_Y - sel->height - yoff;
	else
		y = FB_Y - sel->height + yoff;

	redraw(sel, y);
}


static void sel_update(struct sel *sel, bool partial)
{
	uint16_t y, left;
	uint8_t h;
	int8_t yoff;

	/* find current entry */

	rewind(sel);
	y = sel->curr_y + (size(sel) >> 1);

	left = y;
	while (1) {
		h = size(sel);
		if (h > left)
			break;
		left -= h;
		REQUIRE(next(sel));
	}

	sel->cursor = sel->pos;

	if (partial)
		return;

	/* distance of nominal position from mid-point of entry */

	yoff = left - (h >> 1);

	/* choose positioning policy depending on location in list */

	rewind(sel);

	if (scroll_mode == smooth_and_centered)
		goto centered;

	if (y <= FB_Y >> 1 || sel->height <= FB_Y)
		show_top(sel, h, yoff);
	else if (y > sel->height - (FB_Y >> 1))
		show_bottom(sel, h, yoff);
	else {
centered:
		show_middle(sel, h, yoff);
	}
}


uint16_t sel_cursor(struct sel *sel)
{
	sel_update(sel, 1);
	return sel->cursor;
}


void sel_show(struct sel *sel)
{
	sel_update(sel, 0);
}


/* ----- Setup ------------------------------------------------------------- */


static void center(struct sel *sel)
{
	uint16_t y;
	uint8_t h;

	rewind(sel);
	sel->curr_y = -(size(sel) >> 1);
	y = 0;
	while (1) {
		h = size(sel);
		if (y + h >= sel->height >> 1)
			break;
		if (!next(sel))
			break;
		y += h;
		sel->curr_y += h;
	}

	sel->curr_y += h >> 1;
}


void sel_init(struct sel *sel, const struct sel_ops *ops, void *user)
{
	uint8_t h;

	sel->ops = ops;
	sel->user = user;

	sel->pos = 0;
	sel->cursor = 0;

	h = size(sel);
	sel->height = h;
	sel->span = (h + 1) >> 1;

	while (next(sel)) {
		h = size(sel);
		sel->height += h;
		sel->span += h;
	}
	sel->span -= (h + 1) >> 1;

	center(sel);
}


/* ----- Movements: steps -------------------------------------------------- */


static bool get_y(struct sel *sel, uint16_t pos, uint16_t *res)
{
	int16_t y;
	uint8_t h;

	rewind(sel);
	h = size(sel);
	y = -(h >> 1);
	while (sel->pos != pos) {
		h = size(sel);
		if (!next(sel))
			return 0;
		y += h;
	}
	*res = y + (h >> 1);
	return 1;
}


bool sel_up(struct sel *sel)
{
	if (!sel->cursor)
		return 0;
	return get_y(sel, sel->cursor - 1, &sel->curr_y);
}


bool sel_down(struct sel *sel)
{
	return get_y(sel, sel->cursor + 1, &sel->curr_y);
}


/* ----- Movements: swiping ------------------------------------------------ */


static uint8_t limit_swipe(struct sel *sel, int8_t dir, uint8_t dist)
{
	uint16_t y;
	uint8_t h, limit;

	rewind(sel);
	h = size(sel) >> 1;
	y = h >> 1;
	while (sel->pos != sel->cursor + dir) {
		h = size(sel);
		if (!next(sel))
			return dist;
		y += h;
	}
	y -= h >> 1;
	limit = dir == 1 ? y - sel->curr_y : sel->curr_y - y;
	limit += h >> 2;	/* @@@ tune this */
	return dist > limit ? limit : dist;
}


static bool swipe_up(struct sel *sel, uint8_t delta)
{
	if (!sel->curr_y)
		return 0;

	delta = limit_swipe(sel, -1, delta);

	if (sel->curr_y < delta)
		sel->curr_y = 0;
	else
		sel->curr_y -= delta;
	return 1;
}


static bool swipe_down(struct sel *sel, uint8_t delta)
{
	if (sel->curr_y == sel->span - 1)
		return 0;

	delta = limit_swipe(sel, 1, delta);

	if (sel->curr_y + delta < sel->span)
		sel->curr_y += delta;
	else
		sel->curr_y = sel->span - 1;
	return 1;
}


bool sel_swipe(struct sel *sel, int8_t delta)
{
	if (!delta)
		return 0;
	delta += delta;		/* accelerate: one position = 2 pixels */
	if (delta < 0)
		return swipe_up(sel, -delta);
	else
		return swipe_down(sel, delta);
}


/* ----- Simulator support ------------------------------------------------- */


void sim_scroll_mode(enum scroll_mode mode)
{
	scroll_mode = mode;
}


void sim_sel_jump(struct sel *sel, enum jump_to whence)
{
	switch (whence) {
	case jump_top:
		sel->curr_y = 0;
		break;
	case jump_middle:
		center(sel);
		break;
	case jump_bottom:
		sel->curr_y = sel->span - 1;
		break;
	}
}
