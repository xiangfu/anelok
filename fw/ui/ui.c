/*
 * fw/ui/ui.c - User interface main loop
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "touch.h"
#include "tick.h"
#include "led.h"
#include "pm.h"
#include "input.h"
#include "display.h"
#include "ui.h"
#include "auth.h"


#define	BLINK_INTERVAL_MS	2000
#define	BLINK_ON_MS		1

bool leave_ui = 0;


static uint16_t t;
static uint32_t t_idle = 0;
static uint32_t t_last_blink = 0;



bool ui(void)
{
	uint16_t dt;
	uint8_t pos;

	if (leave_ui)
		return 0;
	dt = tick_delta(&t);
	auth_delta(dt);	/* @@@ ugly */
	if (touch_filter(&pos)) {
		input_press(FB_Y - pos - 1, dt);
		t_idle = t_last_blink = 0;
		if (ui_is_on)
			pm_busy();
	} else {
		input_release(dt);
		t_idle += dt;
		pm_idle(t_idle);
		if (pm_state() != pm_active &&
		    (t_idle - t_last_blink) >= BLINK_INTERVAL_MS &&
		    auth_valid()) {
			led(1);
			msleep(BLINK_ON_MS);
			led(0);
			t_last_blink = t_idle;
		}
	}
	if (display_update())
		msleep(6);
	else
		msleep(10);
	return 1;
}


void ui_init(void)
{
	t = tick_now();
}
