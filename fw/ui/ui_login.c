/*
 * fw/ui_login.c - UI: login
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "pm.h"
#include "display.h"
#include "input.h"
#include "icon.h"
#include "auth.h"
#include "ui.h"


static uint32_t code;
static uint8_t curr_col;
static bool extended;


#define	YES		icon_bullet
#define	NO		icon_circle

#define	GAP_X		4
#define	GAP_Y		8

#define	MAX_COLS	9


static uint8_t field_x(uint8_t col)
{
	return (YES.w + GAP_X) * col + main_x0;
}


static uint8_t field_y(uint8_t digit)
{
	return (FB_Y - YES.h) / 2 + (YES.h + GAP_Y) * ((int8_t) (digit - 1));
}


static void clear(uint8_t col)
{
	uint8_t i, x, y;

	x = field_x(col);
	for (i = 0; i != 3; i++) {
		y = field_y(i);
		display_clear_rect(x, y, x + YES.w - 1, y + YES.h - 1);
	}
}


static void empty(uint8_t col)
{
	uint8_t i, x, y;

	x = field_x(col);
	for (i = 0; i != 3; i++) {
		y = field_y(i);
		icon_draw(&NO, x, y);
	}
}


static void select(uint8_t col, uint8_t digit)
{
	uint8_t x, y;

	x = field_x(col);
	y = field_y(digit);
	clear(col);
	icon_draw(&YES, x, y);
}


/* ----- Login screen operations ------------------------------------------- */


static void bar(enum area highlight)
{
	const struct icon *top, *bot;

	if (curr_col)
		top = highlight == area_top ?
		    extended ? &icon_setup_bold : &icon_delete_bold :
		    &icon_delete;
	else
		top = highlight == area_top ? &icon_setup_bold : &icon_setup;
	if (curr_col)
		bot = highlight == area_bottom ?
		    extended ? &icon_off_bold : &icon_enter_bold : &icon_enter;
	else
		bot = highlight == area_bottom ? &icon_off_bold : &icon_off;

	icon_bar(top, NULL, bot);
}


static void reset(void)
{
	display_clear();
	curr_col = 0;
	code = 0;
	bar(area_none);
	empty(0);
}


static void enter(uint8_t digit)
{
	if (curr_col == MAX_COLS)
		return;
	select(curr_col, digit);
	code |= (digit + 1) << (curr_col * 2);
	curr_col++;
	if (curr_col < MAX_COLS)
		empty(curr_col);
}


static void delete(void)
{
	if (!curr_col)
		return;
	if (curr_col < MAX_COLS)
		clear(curr_col);
	curr_col--;
	code &= (1 << (curr_col * 2)) - 1;
	clear(curr_col);
	empty(curr_col);
}


static void login(void)
{
	if (auth_try(code)) {
		ui_select();
	} else {
		reset();
	}
}


/* ----- Input callbacks --------------------------------------------------- */


static void login_tap(enum area area, void *user)
{
	switch (area) {
	case area_top:
		enter(0);
		break;
	case area_middle:
		enter(1);
		break;
	case area_bottom:
		enter(2);
		break;
	default:
		break;
	}
	if (curr_col)
		bar(area_none);
}


static void login_begin(enum area area, void *user)
{
	switch (area) {
	case area_top:
		bar(area);
		break;
	case area_bottom:
		bar(area);
		break;
	default:
		break;
	}
}


static void login_cancel(enum area area, void *user)
{
	extended = 0;
	bar(area_none);
}


static void login_press(enum area area, void *user)
{
	switch (area) {
	case area_top:
		if (!curr_col) {
			ui_login_setup();
			return;
		}
		delete();
		break;
	case area_bottom:
		if (curr_col)
			login();
		else
			ui_off();
		return;
	default:
		break;
	}
	bar(area_none);
}


static bool login_extend(enum area area, void *user)
{
	extended = area != area_middle && curr_col;
	bar(area);
	return extended;
}


static void login_extended(enum area area, void *user)
{
	extended = 0;

	switch (area) {
	case area_top:
		ui_login_setup();
		return;
	case area_bottom:
		ui_off();
		return;
	default:
		oops();
	}
}


static void login_suspend(void *user)
{
	ui_off();
}


static const struct input_ops login_ops = {
	.tap		= login_tap,
	.begin		= login_begin,
	.cancel		= login_cancel,
	.press		= login_press,
	.extend		= login_extend,
	.extended	= login_extended,
	.suspend	= login_suspend,
};


void ui_login(void)
{
	extended = 0;
	pm_set_policy(&pm_policy_login);
	input_select(&login_ops, NULL);
	reset();
}
