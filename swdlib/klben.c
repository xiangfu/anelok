/*
 * swdlib/klben.c - Ben Nanonote UBB driver for SWD (KL2x)
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../fw/bare-metal-arm/MKL25Z4.h"

#include <ubb/ubb.h>
#include "ubbmap.h"
#include "swdlib.h"
#include "klflash.h"
#include "ben.h"


struct ben_swd {
	uint32_t reset, dio, clk;
	bool input;
	struct swd *swd;
};


static struct ubbmap map[] = {
	{ "reset",	UBB_CLK },
	{ "dio",	UBB_DAT1 },
	{ "clk",	UBB_DAT0 },
	{ NULL }
};


#define	KL_RESET	ubbmap_pin(map, "reset")
#define	SWD_DIO		ubbmap_pin(map, "dio")
#define	SWD_CLK		ubbmap_pin(map, "clk")

#define	FLASH_WORDS	32768	/* @@@ */

#define	BAR_WIDTH	78


/* ----- Bit-banging interface --------------------------------------------- */


static inline void kl_pulse(struct ben_swd *ben)
{
	SET(ben->clk);
	CLR(ben->clk);
}


static void kl_send(void *ctx, uint32_t data, uint8_t bits)
{
	struct ben_swd *ben = ctx;
	uint8_t i;

	DPRINTF(">>> 0x%x, %u\n", data, bits);
	if (ben->input) {
		kl_pulse(ben);
		DPRINTF("->-\n");
		OUT(ben->dio);
		ben->input = 0;
	}
	for (i = 0; i != bits; i++) {
		if ((data >> i) & 1)
			SET(ben->dio);
		else
			CLR(ben->dio);
		kl_pulse(ben);
	}
}


static uint32_t kl_recv(void *ctx, uint8_t bits)
{
	struct ben_swd *ben = ctx;
	uint32_t val = 0;
	uint8_t i;

	if (!ben->input) {
		IN(ben->dio);
		DPRINTF("-<-\n");
		kl_pulse(ben);
		ben->input = 1;
	}
	for (i = 0; i != bits; i++) {
		val |= PIN(ben->dio) << i;
		kl_pulse(ben);
	}
	DPRINTF("<<< 0x%x, %u\n", val, bits);
	return val;
}


static void kl_reset(void *ctx, bool active)
{
	struct ben_swd *ben = ctx;

	DPRINTF("reset %u\n", active);
	if (active) {
		OUT(ben->reset);
		usleep(1);
	} else {
		IN(ben->reset);
		usleep(10); /* takes about 2.5 us to raise */
	}
}


static void sleep_1s(void *ctx)
{
	sleep(1);
}


static const struct swd_ops kl_ops = {
	.send		= kl_send,
	.recv		= kl_recv,
	.reset		= kl_reset,
	.sleep_1s	= sleep_1s,
	.report		= report,
};


bool ben_swd_ubb_open(uint32_t reset, uint32_t dio, uint32_t clk)
{
	if (ubb_open(~(reset | dio | clk))) {
		fprintf(stderr, "ubb_open failed\n");
		return 0;
	}

	/* drain power from target */

	ubb_power(0);

	CLR(reset);
	CLR(clk);
	CLR(dio);

	OUT(reset);
	OUT(clk);
	OUT(dio);

	sleep(1);

	/*
	 * Set pull-up on nRESET. The two states of nRESET are strong drive to
	 * ground and weak pull to VDD. This allows the target to command its
	 * own wired-or resets without the risk of having to fight the
	 * programmer.
	 */
	PDPULLC = reset;

	/* allow Ben's C45 to charge */

	SET(clk);
	SET(dio);

	sleep(1);

	/* power up */

	ubb_power(1);

	usleep(200 * 1000);

	return 1;
}


bool ben_swd_open(struct ben_swd *ben, struct swd *swd,
    uint32_t reset, uint32_t dio, uint32_t clk)
{
	ben->reset = reset;
	ben->dio = dio;
	ben->clk = clk;

	if (!ben_swd_ubb_open(reset, dio, clk))
		return 0;

	ben->input = 0;

	ben->swd = swd;
	if (!swd_open(swd, &kl_ops, ben)) {
		fprintf(stderr, "swd_open failed\n");
		return 0;
	}

	return 1;
}


bool do_release(struct swd *swd)
{
	if (swd_release(swd))
		return 1;
	fprintf(stderr, "swd_release failed\n");
	return 0;
}


void ben_swd_close(struct ben_swd *ben)
{
	swd_close(ben->swd);
//	ubb_power(0);
	ubb_close(ben->clk);
}


/* ----- Operations backend ------------------------------------------------ */


struct progress {
	struct swd *swd;
	uint32_t words;
	uint8_t div;
	uint8_t bar;
};


static void progress(void *user, bool verify, uint16_t pos)
{
	struct progress *ctx = user;
	struct swd *swd = ctx->swd;

	if (!pos)
		ctx->bar = 0;
	if (pos == ctx->words) {
		swd_report("|%c", verify ? '\n' : '\r');
		return;
	}
	if (pos / ctx->div == ctx->bar) {
		swd_report("%c\b", "/-\\|"[pos & 3]);
	} else {
		swd_report(verify ? "#" : "-");
		ctx->bar++;
	}
}


#define	FSEC	0x40c


static enum protection decode_fsec(const uint32_t *buf, uint32_t words)
{
	uint8_t fsec;

	if (words < FSEC >> 2)
		return prot_none;
	fsec = buf[FSEC >> 2];
	if ((fsec & 3) == 2)
		return	prot_none;
	if (((fsec >> 4) & 2) == 2) /* mass erase disabled */
		return prot_irrev;
	else
		return prot_rev;
}


static bool compare_fsec(struct swd *swd,
    enum protection expect, enum protection has)
{
	if (expect == has)
		return 1;
	swd_report("binary has ");
	switch (has) {
	case prot_none:
		swd_report("no");
		break;
	case prot_rev:
		swd_report("reversible");
		break;
	case prot_irrev:
		swd_report("irreversible");
		break;
	default:
		abort();
	}
	swd_report(" protection, expected ");
	switch (expect) {
	case prot_none:
		swd_report("none");
		break;
	case prot_rev:
		swd_report("reversible");
		break;
	case prot_irrev:
		swd_report("irreversible");
		break;
	default:
		abort();
	}
	swd_report("\n");
	return 0;
}


static bool kl_program(struct swd *swd, uint32_t addr,
    const uint32_t *buf, uint32_t words, enum protection prot)
{
	struct progress ctx = {
		.swd	= swd,
		.words	= words,
		.div	= words < BAR_WIDTH - 1 ? 1 :
			  (words + BAR_WIDTH - 1 - 1) / (BAR_WIDTH - 1),
		.bar	= 0,
	};
	enum protection fsec_prot;

	fsec_prot = decode_fsec(buf, words);
	if (!compare_fsec(swd, prot, fsec_prot))
		return 0;
	if (!kl_mass_erase(swd))
		return 0;
	return kl_program_flash(swd, addr, buf, words, progress, &ctx);
}


static void *read_flash(struct swd *swd, uint32_t *size)
{
	uint32_t *buf;

	buf = calloc(FLASH_WORDS, sizeof(uint32_t));
	if (!buf) {
		perror("calloc");
		return NULL;
	}

	if (!kl_read_flash(swd, 0, buf, FLASH_WORDS)) {
		free(buf);
		return NULL;
	}

	*size = FLASH_WORDS * 4;
	return buf;
}


/* ----- Print UID --------------------------------------------------------- */


#define RFW(r, v)	swd_read_32(swd, &(r), (v))


static  bool print_uid(struct swd *swd)
{
	uint32_t mid_high, mid_low, low;

	if (!RFW(SIM_UIDMH, &mid_high))
		return 0;
	if (!RFW(SIM_UIDML, &mid_low))
		return 0;
	if (!RFW(SIM_UIDL, &low))
		return 0;
	fprintf(stderr, "UID %04x-%08x-%08x\n",
	    mid_high & 0xffff, mid_low, low);
	return 1;
}


/* ----- Target operations ------------------------------------------------- */


static void kl_op_assign(const char *s)
{
	ubbmap_assign(map, s);
}


static bool kl_op_identify(void)
{
	struct ben_swd kl;
	struct swd swd;
	bool can_erase, secure, backdoor;
	bool ok = 0;

	if (!ben_swd_open(&kl, &swd, KL_RESET, SWD_DIO, SWD_CLK))
		return 0;
	if (!kl_flash_security(&swd, &can_erase, &secure, &backdoor))
		goto out;
	fprintf(stderr, "Mass-erase: %s, secure: %s, backdoor: %s\n",
	    can_erase ? "yes" : "no", secure ? "yes" : "no",
	    backdoor ? "yes" : "no");
	if (secure) {
		ok = 1;
		goto out;
	}
	if (!do_release(&swd))
		return 0;
	ok = swd_identify_soc(&swd);
	ok = ok && print_uid(&swd);

out:
	ben_swd_close(&kl);
	return ok;
}


static bool kl_op_erase(void)
{
	struct ben_swd kl;
	struct swd swd;
	bool ok;

	if (!ben_swd_open(&kl, &swd, KL_RESET, SWD_DIO, SWD_CLK))
		return 0;
	ok = kl_mass_erase(&swd);
	ben_swd_close(&kl);
	return ok;
}


static bool kl_op_write(const char *file, enum protection prot)
{
	struct ben_swd kl;
	struct swd swd;
	bool can_erase, secure;
	bool ok = 0;

	if (!ben_swd_open(&kl, &swd, KL_RESET, SWD_DIO, SWD_CLK))
		return 0;

	if (!kl_flash_security(&swd, &can_erase, &secure, NULL))
		goto out;
	if (!can_erase) {
		fprintf(stderr, "Flash cannot be erased\n");
		goto out;
	}
	if (secure) {
		fprintf(stderr, "Chip is \"secure\" - please erase with -e\n");
		goto out;
	}

	if (!do_release(&swd))
		return 0;
	if (!swd_identify_soc(&swd))
		goto out;

	ok = do_flash(&swd, file, (flash_fn) kl_program, prot);
out:
	ben_swd_close(&kl);

	return ok;
}


static void *kl_op_read(uint32_t *size)
{
	struct ben_swd kl;
	struct swd swd;
	void *res = NULL;

	if (!ben_swd_open(&kl, &swd, KL_RESET, SWD_DIO, SWD_CLK))
		return NULL;
	if (!do_release(&swd))
		return 0;
	if (!swd_identify_soc(&swd))
		goto out;

	res = read_flash(&swd, size);

out:
	ben_swd_close(&kl);

	return res;
}


static void kl_op_run(void)
{
	char ch;

	ben_swd_ubb_open(KL_RESET, SWD_DIO, SWD_CLK);
	SET(KL_RESET);
	usleep(100 * 1000);
	CLR(KL_RESET);
	usleep(100 * 1000);
	SET(KL_RESET);
	fprintf(stderr, "Press [Enter] to exit\n");
	read(0, &ch, 1);
	ubb_close(SWD_CLK);
}


const struct target_ops kl_target_ops = {
	.assign		= kl_op_assign,
	.identify	= kl_op_identify,
	.erase		= kl_op_erase,
	.write		= kl_op_write,
	.read		= kl_op_read,
	.run		= kl_op_run,
};
