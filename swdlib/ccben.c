/*
 * swdlib/ccben.c - Ben Nanonote UBB driver for CCxxxx
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <ubb/ubb.h>
#include "ubbmap.h"
#include "ccdbg.h"
#include "ben.h"


struct ben_ccdbg {
	uint32_t reset, dd, dc;
	bool input;
	struct ccdbg *ccdbg;
};


static struct ubbmap map[] = {
	{ "reset",	UBB_CMD },
	{ "dd",		UBB_DAT3 },
	{ "dc",		UBB_DAT2 },
	{ NULL }
};


#define	CC_RESET	ubbmap_pin(map, "reset")
#define	CC_DD		ubbmap_pin(map, "dd")
#define	CC_DC		ubbmap_pin(map, "dc")

#define	FLASH_BYTES	0x8000
#define	FLASH_BASE	0x8000


/* ----- Bit-banging interface --------------------------------------------- */


static void cc_pulse(void *ctx)
{
	struct ben_ccdbg *ben = ctx;

	SET(ben->dc);
	DPRINTF("^\n");
	CLR(ben->dc);
}


static bool cc_sample(void *ctx)
{
	struct ben_ccdbg *ben = ctx;

	IN(ben->dd);
	ben->input = 1;
	return PIN(ben->dd);
}


static void cc_send(void *ctx, uint8_t data)
{
	struct ben_ccdbg *ben = ctx;
	uint8_t i;

	DPRINTF(">>> 0x%x\n", data);
	if (ben->input) {
		DPRINTF("->-\n");
		OUT(ben->dd);
		ben->input = 0;
	}
	for (i = 0; i != 8; i++) {
		if ((data << i) & 0x80)
			SET(ben->dd);
		else
			CLR(ben->dd);
		cc_pulse(ben);
	}
}


static uint8_t cc_recv(void *ctx)
{
	struct ben_ccdbg *ben = ctx;
	uint8_t val = 0;
	uint8_t i;

	if (!ben->input) {
		IN(ben->dd);
		DPRINTF("-<-\n");
		ben->input = 1;
	}
	for (i = 0; i != 8; i++) {
		cc_pulse(ben);
		val = (val << 1) | PIN(ben->dd);
	}
	DPRINTF("<<< 0x%x\n", val);
	return val;

}


static void cc_reset(void *ctx, bool active)
{
	struct ben_ccdbg *ben = ctx;

	DPRINTF("reset %u\n", active);
	if (active)
		CLR(ben->reset);
	else
		SET(ben->reset);
}


static const struct ccdbg_ops cc_ops = {
	.send	= cc_send,
	.recv	= cc_recv,
	.sample	= cc_sample,
	.pulse	= cc_pulse,
	.reset	= cc_reset,
	.report	= report,
};


static bool ben_ccdbg_open(struct ben_ccdbg *ben, struct ccdbg *ccdbg,
    uint32_t reset, uint32_t dd, uint32_t dc)
{
	ben->reset = reset;
	ben->dd = dd;
	ben->dc = dc;
	if (ubb_open(~(reset | dd | dc))) {
		fprintf(stderr, "ubb_open failed\n");
		return 0;
	}

	SET(reset);
	CLR(dc);
	CLR(dd);

	OUT(reset);
	OUT(dc);
	OUT(dd);

	ben->input = 0;

	ben->ccdbg = ccdbg;
	if (!ccdbg_open(ccdbg, &cc_ops, ben)) {
		fprintf(stderr, "ccdbg_open failed\n");
		return 0;
	}

	return 1;
}


static void ben_ccdbg_close(struct ben_ccdbg *ben)
{
	ccdbg_close(ben->ccdbg);
	ubb_close(ben->dc);
}


/* ----- Operations backend ------------------------------------------------ */


static void *cc_read_flash(struct ccdbg *ccdbg, uint32_t *size)
{
	uint8_t *buf;

	buf = malloc(FLASH_BYTES);
	if (!buf) {
		perror("malloc");
		return NULL;
	}

	ccdbg_read_block(ccdbg, buf, FLASH_BASE, FLASH_BYTES);

	*size = FLASH_BYTES;
	return buf;
}


static bool cc_program_flash(struct ccdbg *ccdbg,
    uint32_t addr, const uint32_t *buf, uint32_t words, enum protection prot)
{
	ccdbg_erase(ccdbg);
	ccdbg_flash_block(ccdbg, (const uint8_t *) buf, addr >> 2, words << 2);
	return 0;
}


/* ----- Target operations ------------------------------------------------- */


static void cc_op_assign(const char *s)
{
	ubbmap_assign(map, s);
}


static bool cc_op_erase(void)
{
	struct ben_ccdbg cc;
	struct ccdbg ccdbg;

	ben_ccdbg_open(&cc, &ccdbg, CC_RESET, CC_DD, CC_DC);
	ccdbg_erase(&ccdbg);
	ben_ccdbg_close(&cc);

	return 1;
}


static bool cc_op_write(const char *file, enum protection prot)
{
	struct ben_ccdbg cc;
	struct ccdbg ccdbg;
	bool ok;

	ben_ccdbg_open(&cc, &ccdbg, CC_RESET, CC_DD, CC_DC);
	ok = do_flash(&ccdbg, file, (flash_fn) cc_program_flash, prot);
	ben_ccdbg_close(&cc);

	return ok;
}


static void *cc_op_read(uint32_t *size)
{
	struct ben_ccdbg cc;
	struct ccdbg ccdbg;
	void *res;

	ben_ccdbg_open(&cc, &ccdbg, CC_RESET, CC_DD, CC_DC);
	res = cc_read_flash(&ccdbg, size);
	ben_ccdbg_close(&cc);
	return res;
}


const struct target_ops cc_target_ops = {
	.assign	= cc_op_assign,
	.erase	= cc_op_erase,
	.write	= cc_op_write,
	.read	= cc_op_read,
};

