/*
 * swdlib/ben.c - Ben Nanonote UBB driver
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ben.h"


/* ----- Diagnostics (same for CC2543 and KL2x) ---------------------------- */


void report(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fflush(stderr);
}


/* ----- Platform-independent file load and flash -------------------------- */


bool do_flash(void *dsc, const char *name, flash_fn fn, enum protection prot)
{
	int fd;
	struct stat st;
	off_t size;
	void *buf;
	ssize_t got;

	fd = open(name, O_RDONLY);
	if (fd < 0) {
		perror(name);
		return 0;
	}
	if (fstat(fd, &st) < 0) {
		perror(name);
		return 0;
	}
	size = (st.st_size + 3) & ~3;
	buf = malloc(size);
	if (!buf) {
		perror("malloc");
		return 0;
	}
	got = read(fd, buf, st.st_size);
	if (got < 0) {
		perror(name);
		return 0;
	}
	return fn(dsc, 0, (const uint32_t *) buf, (got + 3) >> 2, prot);
}


/* ----- Main -------------------------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [common] [-P|-I] [file.bin]\n"
"       %s [common] -d\n"
"       %s [common] -r\n"
"       %s [common] -e\n\n"
"Common items:\n"
"  [signal=pin] [-c]\n\n"
"  -c  target is CC25xx (default: KL2x)\n"
"  -d  dump Flash content (in binary)\n"
"  -e  erase whole Flash\n"
"  -r  reset and run target\n"
"  -P  file sets reversible chip protection\n"
"  -I  file sets irreversible chip protection\n"
	    , name, name, name, name);
	exit(1);
}


static void not_supported(void)
{
	fprintf(stderr, "opperation not supported by target driver\n");
	exit(1);
}


int main(int argc, char *const *argv)
{
	const struct target_ops *ops = &kl_target_ops;
	enum {
		mode_default = 0,
		mode_dump,
		mode_erase,
		mode_run,
	} mode = mode_default;
	enum protection prot = prot_none;
	int c;
	bool ok = 1;
	const void *buf;
	uint32_t bytes;

	while ((c = getopt(argc, argv, "cderPI")) != EOF)
		switch (c) {
		case 'c':
			ops = &cc_target_ops;
			break;
		case 'd':
			if (mode)
				usage(*argv);
			mode = mode_dump;
			break;
		case 'e':
			if (mode)
				usage(*argv);
			mode = mode_erase;
			break;
		case 'r':
			if (mode)
				usage(*argv);
			mode = mode_run;
			break;
		case 'P':
			if (prot != prot_none)
				usage(*argv);
			prot = prot_rev;
			break;
		case 'I':
			if (prot != prot_none)
				usage(*argv);
			prot = prot_irrev;
			break;
		default:
			usage(*argv);
		}

	while (argc != optind) {
		if (!strchr(argv[optind], '='))
			break;
		if (!ops->assign)
			not_supported();
		ops->assign(argv[optind]);
		optind++;
	}

	switch (argc - optind) {
	case 1:
		if (mode != mode_default)
			usage(*argv);
		/* fall through */
	case 0:
		break;
	default:
		usage(*argv);
	}

	switch (mode) {
	case mode_default:
		if (argc == optind) {
			if (!ops->identify)
				not_supported();
			ok = ops->identify();
		} else {
			if (!ops->write)
				not_supported();
			ok = ops->write(argv[optind], prot);
		}
		break;
	case mode_dump:
		if (!ops->read)
			not_supported();
		buf = ops->read(&bytes);
		if (!buf)
			return 1;
		fwrite(buf, 1, bytes, stdout);
		break;
	case mode_erase:
		if (!ops->erase)
			not_supported();
		ok = ops->erase();
		break;
	case mode_run:
		if (!ops->run)
			not_supported();
		ops->run();
		break;
	default:
		abort();
	}

	return !ok;
}
