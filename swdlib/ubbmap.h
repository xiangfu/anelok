/*
 * swdlib/ubbmap.h - UBB signal mapping
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef UBBMAP_H
#define	UBBMAP_H

#include <stdint.h>


struct ubbmap {
	const char *name;	/* NULL in last element of array */
	uint32_t pin;
};


void ubbmap_assign(struct ubbmap *map, const char *s);
uint32_t ubbmap_pin(const struct ubbmap *map, const char *name);

#endif /* !UBBMAP_H */
