/*
 * swdlib/swdlib.c - Experimental Anelok firmware
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Resources:
 *
 * ARM's documentation (coresight, v5 debug spec, v5.1 supplement):
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0480f/index.html
 * http://www.pjrc.com/arm/pdf/doc/ARM_debug.pdf
 * http://www.pjrc.com/arm/pdf/doc/ARM_debug.sup.pdf
 *
 * ARM DSTREAM SWD timing diagram:
 * http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0499d/BEHCBIJE.html
 *
 * AHB-AP (ARM):
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0480f/CIHEBBEI.html
 *
 * Cortex M1 DAP:
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0413c/Babbbebi.html
 *
 * SiLab's improved variant (includes valid IDCODEs):
 * http://www.silabs.com/Support%20Documents/TechnicalDocs/AN0062.pdf
 *
 * OpenOCD
 * http://openocd.sourceforge.net/doc/doxygen/html/arm__adi__v5_8c_source.html
 */

#include <stdbool.h>
#include <stdint.h>
//#include <stdlib.h>
//#include <stdio.h>

#include "dap.h"
#include "swdlib.h"
#include "../fw/bare-metal-arm/MKL25Z4.h"


/* ----- Wrappers for platform operations ---------------------------------- */


static inline void swd_send(struct swd *swd, uint32_t data, uint8_t bits)
{
	swd->ops->send(swd->ctx, data, bits);
}


static inline uint32_t swd_recv(struct swd *swd, uint8_t bits)
{
	return swd->ops->recv(swd->ctx, bits);
}


static inline void swd_reset(struct swd *swd, bool active)
{
	return swd->ops->reset(swd->ctx, active);
}


void swd_sleep_1s(struct swd *swd)
{
	return swd->ops->sleep_1s(swd->ctx);
}


#define swd_report(fmt, ...)	swd->ops->report(fmt, ##__VA_ARGS__)


/* ----- Request and Acknowledge ------------------------------------------- */


static enum swd_response swd_request(struct swd *swd,
    bool ap, bool rd, uint8_t reg)
{
	uint32_t val;
	bool parity;

	parity = ap ^ rd ^ (reg & 1) ^ (reg >> 1);
	val = 1			/* Start */
	    | ap << 1		/* APnDP */
	    | rd << 2		/* RnW */
	    | reg << 3		/* A[2:3] */
	    | parity << 5	/* Parity */
	    | 0 << 6		/* Stop */
	    | 1 << 7;		/* Park */
	swd_send(swd, val, 8);

	val = swd_recv(swd, 3);
	switch (val) {
	case 1:
		return SWD_OK;
	case 2:
		return SWD_WAIT;
	case 4:
		return SWD_FAULT;
	default:
		swd_report("ACK %u\n", (unsigned) val);
		return SWD_ERROR;
	}
}


/* ----- DP/AP register read/write ----------------------------------------- */


static bool parity32(uint32_t val)
{
	val ^= val >> 16;
	val ^= val >> 8;
	val ^= val >> 4;
	val ^= val >> 2;
	val ^= val >> 1;
	return val & 1;
}


static enum swd_response swd_read(struct swd *swd, bool ap, uint8_t reg,
    uint32_t *val)
{
	enum swd_response res;

	res = swd_request(swd, ap, 1, reg >> 2);
	if (res)
		return res;
	*val = swd_recv(swd, 32);
	if (parity32(*val) != swd_recv(swd, 1))
		return SWD_PARITY;
	DPRINTF("%s[%u] -> 0x%08x\n", ap ? "AP" : "DP", reg, *val);
	return SWD_OK;
}


static enum swd_response swd_write(struct swd *swd, bool ap, uint8_t reg,
    uint32_t val)
{
	enum swd_response res;

	DPRINTF("%s[%u] = 0x%08x\n", ap ? "AP" : "DP", reg, val);
	res = swd_request(swd, ap, 0, reg >> 2);
	if (res)
		return res;
	swd_send(swd, val, 32);
	swd_send(swd, parity32(val), 1);
	return SWD_OK;
}


/* ----- AP register read/write -------------------------------------------- */


static enum swd_response swd_ap_force_select(struct swd *swd,
    uint8_t ap, uint8_t bank)
{
	enum swd_response res;

	res = swd_write(swd, 0, SWD_DP_SELECT,
	    ap << SWD_DP_SELECT_APSEL_SHIFT |
	    bank << SWD_DP_SELECT_APBANKSEL_SHIFT);
	if (res)
		return res;
	swd->ap = ap;
	swd->ap_bank = bank;
	return SWD_OK;
}


static enum swd_response swd_ap_select(struct swd *swd,
    uint8_t ap,  uint8_t bank)
{
	if (swd->ap == ap && swd->ap_bank == bank)
		return SWD_OK;
	return swd_ap_force_select(swd, ap, bank);
}


enum swd_response swd_ap_read(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t *val)
{
	enum swd_response res;

	res = swd_ap_select(swd, ap, reg >> 4);
	if (res)
		return res;
	res = swd_read(swd, 1, reg & 0xc, val);
	if (res)
		return res;
	return swd_read(swd, 0, SWD_DP_RDBUF, val);
}


enum swd_response swd_ap_write(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t val)
{
	enum swd_response res;

	res = swd_ap_select(swd, ap, reg >> 4);
	if (res)
		return res;
	return swd_write(swd, 1, reg & 0xc, val);
}


/* ----- AHB read/write ---------------------------------------------------- */


static enum swd_response ahb_wait_idle(struct swd *swd)
{
	enum swd_response res;
	uint32_t s;

	do {
		res = swd_ap_read(swd, AHB_AP, AHB_AP_CSW, &s);
		if (res)
			return res;
	} while ((s & AHB_AP_CSW_TrInProg) || !(s & AHB_AP_CSW_DbgStatus));
	return 0;
}


static enum swd_response ahb_read_8(struct swd *swd,
    uint32_t addr, uint8_t *val)
{
	enum swd_response res;
	uint32_t tmp;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    AHB_AP_CSW_Size_8 | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	res = swd_ap_read(swd, AHB_AP, AHB_AP_DRW, &tmp);
	*val = tmp;
	return res;
}


static enum swd_response ahb_write_size(struct swd *swd,
    uint32_t addr, uint32_t val, uint32_t size)
{
	enum swd_response res;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    size | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	return swd_ap_write(swd, AHB_AP, AHB_AP_DRW, val);
}


static enum swd_response ahb_write_8(struct swd *swd,
    uint32_t addr, uint8_t val)
{
	return ahb_write_size(swd, addr, val, AHB_AP_CSW_Size_8);
}


static enum swd_response ahb_read_32(struct swd *swd,
    uint32_t addr, uint32_t *val)
{
	enum swd_response res;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    AHB_AP_CSW_Size_32 | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	return swd_ap_read(swd, AHB_AP, AHB_AP_DRW, val);
}


static enum swd_response ahb_write_32(struct swd *swd,
    uint32_t addr, uint32_t val)
{
	return ahb_write_size(swd, addr, val, AHB_AP_CSW_Size_32);
}


/* ----- Helper functions -------------------------------------------------- */


void swd_report_error(struct swd *swd, enum swd_response res)
{
	switch (res) {
	case SWD_WAIT:
		swd_report("WAIT\n");
		break;
	case SWD_FAULT:
		swd_report("FAULT\n");
		break;
	case SWD_ERROR:
		swd_report("ERROR\n");
		break;
	case SWD_PARITY:
		swd_report("PARITY\n");
		break;
	default:
		swd_report("unknown error %d\n", res);
		break;
	}
}


static void swd_sync(struct swd *swd)
{
	/* reset JTAG and SWD by sending > 50 cycles */

	swd_send(swd, 0xffffffff, 25);
	swd_send(swd, 0xffffffff, 26);
}


void swd_idle(struct swd *swd, uint16_t cycles)
{
	while (cycles >= 32) {
		swd_send(swd, 0, 32);
		cycles -= 32;
	}
	if (cycles)
		swd_send(swd, 0, cycles);
}


static void status(struct swd *swd)
{
	enum swd_response res;
	uint32_t s;

	res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &s);
	if (res)
		swd_report_error(swd, res);
	DPRINTF("status 0x%08x\n", s);
}


/* ----- SWD initialization ------------------------------------------------ */


static enum swd_response swd_init(struct swd *swd)
{
	enum swd_response res;
	uint32_t id;

	/* activate the SWD interface */

	swd_sync(swd);
	swd_send(swd, 0xe79e, 16);	/* JTAG-to-SWD select sequence */
	swd_sync(swd);
	swd_idle(swd, 8);		/* ADIv5.1: at least one idle cycle */

	/* check that we're talking */

	res = swd_read(swd, 0, SWD_DP_IDCODE, &id);
	if (res) {
		swd_report_error(swd, res);
		return res;
	}

	switch (id) {
	case IDCODE_M0_PLUS:
		swd_report("Cortex M0+\n");
		break;
	case IDCODE_M3_M4:
		swd_report("Cortex M3/M4\n");
		break;
	default:
		swd_report("unknown IDCODE 0x%08x\n", (unsigned) id);
		return SWD_ERROR;
	}

	/* clear all SWD the sticky error flags */

	res = swd_write(swd, 0, SWD_DP_ABORT,
	    SWD_DP_ABORT_STKERRCLR | SWD_DP_ABORT_WDERRCLR |
	     SWD_DP_ABORT_ORUNERRCLR);
	if (res)
		return res;

	/* initialize AP and register bank selection */

	res = swd_ap_force_select(swd, 0, 0);
	if (res)
		return res;

	return SWD_OK;
}


/* ----- Target read/write ------------------------------------------------- */


bool swd_read_8(struct swd *swd, volatile const uint8_t *addr, uint8_t *result)
{
	enum swd_response res;

	while (1) {
		res = ahb_read_8(swd, (uint32_t) addr, result);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_write_8(struct swd *swd, volatile uint8_t *addr, uint8_t val)
{
	enum swd_response res;

//fprintf(stderr, "0x%08x <- %02x\n", (uint32_t) addr, val);
	while (1) {
		res = ahb_write_8(swd, (uint32_t) addr, val);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_read_32(struct swd *swd, volatile const uint32_t *addr,
    uint32_t *result)
{
	enum swd_response res;

	while (1) {
		res = ahb_read_32(swd, (uint32_t) addr, result);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_write_32(struct swd *swd, volatile uint32_t *addr, uint32_t val)
{
	enum swd_response res;

	while (1) {
		res = ahb_write_32(swd, (uint32_t) addr, val);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 1;
	}
}


/* ----- Open/close SWD ---------------------------------------------------- */


static bool identify_aps(struct swd *swd)
{
	enum swd_response res;
	uint32_t id;

	res = swd_ap_read(swd, MDM_AP, MDM_AP_IDR, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	swd_report("AP #%u ID 0x%08x (%s)\n", MDM_AP, (unsigned) id,
	    id == MDM_AP_ID ? "MDM-AP" : "???");

	status(swd);

	res = swd_ap_read(swd, AHB_AP, AHB_AP_IDR, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	swd_report("AP #%u ID 0x%08x (%s)\n", AHB_AP, (unsigned) id,
	    id == AHB_AP_ID ? "AHB-AP" : "???");

	status(swd);
	return 1;
}


bool swd_identify_soc(struct swd *swd)
{
	enum swd_response res;
	uint32_t id, fcfg1;
	uint8_t v;

	/* SDID */

	res = ahb_read_32(swd, (uint32_t) &SIM_SDID, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	swd_report("SDID 0x%08x: ", (unsigned) id);

	/* FCFG1 */

	res = ahb_read_32(swd, (uint32_t) &SIM_FCFG1, &fcfg1);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}

	/* Kinetis series */

	v = (id & SIM_SDID_SERIESID_MASK) >> SIM_SDID_SERIESID_SHIFT;
	switch (v) {
	case 1:
		swd_report("Kinetis KL");
		break;
	default:
		swd_report("???\n");
		return 0;
	}

	/* Kinetis family */

	v = (id & SIM_SDID_FAMID_MASK) >> SIM_SDID_FAMID_SHIFT;
	switch (v) {
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
		swd_report("%u", v);
		break;
	default:
		swd_report("?");
		break;
	}

	/* Kinetis sub-family */

	v = (id & SIM_SDID_SUBFAMID_MASK) >> SIM_SDID_SUBFAMID_SHIFT;
	if (v <= 7)
		swd_report("%u", v);
	else
		swd_report("?");

	/* device revision */

	v = (id & SIM_SDID_REVID_MASK) >> SIM_SDID_REVID_SHIFT;
	swd_report(" rev %u", v);

	/* die number */

	v = (id & SIM_SDID_DIEID_MASK) >> SIM_SDID_DIEID_SHIFT;
	swd_report(".%u", v);

	/* program Flash size */

	v = (fcfg1 & SIM_FCFG1_PFSIZE_MASK) >> SIM_FCFG1_PFSIZE_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 8 kB");
		break;
	case 1:
		swd_report(", 16 kB");
		break;
	case 3:
		swd_report(", 32 kB");
		break;
	case 5:
		swd_report(", 64 kB");
		break;
	case 7:
		swd_report(", 128 kB");
		break;
	case 9:
		swd_report(", 256 kB");
		break;
	default:
		swd_report(", #%x", v);
	}
	swd_report(" Flash");

	/* RAM size */

	v = (id & SIM_SDID_SRAMSIZE_MASK) >> SIM_SDID_SRAMSIZE_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 0.5 kB RAM");
		break;
	default:
		swd_report(", %u kB RAM", 1 << (v - 1));
		break;
	}

	/* pincount */

	v = (id & SIM_SDID_PINID_MASK) >> SIM_SDID_PINID_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 16-pin");
		break;
	case 1:
		swd_report(", 24-pin");
		break;
	case 2:
		swd_report(", 32-pin");
		break;
	case 3:
		swd_report(", 36-pin");
		break;
	case 4:
		swd_report(", 48-pin");
		break;
	case 5:
		swd_report(", 64-pin");
		break;
	case 6:
		swd_report(", 80-pin");
		break;
	case 8:
		swd_report(", 100-pin");
		break;
	case 11:
		swd_report(", WLCSP");
		break;
	default:
		swd_report("??? package");
		break;
	}

	swd_report("\n");
	return 1;
}


#define	CTRL_STAT_TRIES	100000


static bool dp_ctrl_stat(struct swd *swd, uint32_t req, uint32_t mask, bool on)
{
	enum swd_response res;
	uint32_t v;
	unsigned i;

	res = swd_write(swd, 0, SWD_DP_CTRL_STAT, req);
	if (res)
		goto error;

	for (i = 0; i != CTRL_STAT_TRIES; i++) {
		res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &v);
		if (res)
			goto error;
		if ((v & mask) == mask)
			return 1;
	}

	swd_report("DP_CTRL_STAT timeout: expected 0x%x/0x%x, got 0x%x\n",
	    on ? (unsigned) mask : 0, (unsigned) mask, (unsigned) v);
	return 0;

error:
	swd_report_error(swd, res);
	return 0;
}


static bool activate_debug(struct swd *swd)
{
	uint32_t ctrl = SWD_DP_CTRL_CDBGPWRUPREQ | SWD_DP_CTRL_CSYSPWRUPREQ;
	uint32_t ack = SWD_DP_CTRL_CDBGPWRUPACK | SWD_DP_CTRL_CSYSPWRUPACK;
	enum swd_response res;
	uint32_t v;

	/* power up debug and system */

	if (!dp_ctrl_stat(swd, ctrl, ack, 1))
		return 0;

	/* reset debug */

	res = swd_write(swd, 0,
	    SWD_DP_CTRL_STAT, ctrl | SWD_DP_CTRL_CDBGRSTREQ);
	if (res)
		goto error;
	res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &v);
	if (res)
		goto error;

	/* CDBGRSTREQ not implemented */
	if (!(v & SWD_DP_CTRL_CDBGRSTACK)) {
		res = swd_write(swd, 0,
			SWD_DP_CTRL_STAT, ctrl | SWD_DP_CTRL_ORUNDETECT);
		if (res)
			goto error;
		return 1;
	}

	/* remove reset request */

	return dp_ctrl_stat(swd,
	    ctrl | SWD_DP_CTRL_ORUNDETECT, SWD_DP_CTRL_CDBGRSTACK, 0);

error:
	swd_report_error(swd, res);
	return 0;
}


bool swd_open(struct swd *swd, const struct swd_ops *ops, void *ctx)
{
	enum swd_response res;

	swd->ops = ops;
	swd->ctx = ctx;

	res = swd_init(swd);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	status(swd);

	if (!activate_debug(swd))
		return 0;
	status(swd);

	if (!identify_aps(swd))
		return 0;

	res = swd_ap_write(swd, MDM_AP, MDM_AP_CONTROL,
	    MDM_AP_CONTROL_Core_Hold_Reset |
	    MDM_AP_CONTROL_System_Reset_Request);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}

	return 1;
}


bool swd_release(struct swd *swd)
{
	enum swd_response res;

	swd_reset(swd, 0);

	res = swd_ap_write(swd, MDM_AP, MDM_AP_CONTROL,
	    MDM_AP_CONTROL_Core_Hold_Reset);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	status(swd);

	return 1;
}


void swd_close(struct swd *swd)
{
	swd_idle(swd, 1000);
}
