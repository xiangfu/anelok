/*
 * swdlib/ccdbg.h - TI CCxxx debug protocol driver
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CCDBG_H
#define	CCDBG_H

#include <stdbool.h>
#include <stdint.h>


struct ccdbg_ops {
	void (*send)(void *ctx, uint8_t data);
	uint8_t (*recv)(void *ctx);
	bool (*sample)(void *ctx);
	void (*pulse)(void *ctx);
	void (*reset)(void *ctx, bool active);
	void (*report)(const char *fmt, ...)
	    __attribute__((format (printf, 1, 2)));
};

struct ccdbg {
	const struct ccdbg_ops *ops;
	void *ctx;
};


void ccdbg_write_reg(struct ccdbg *ccdbg, uint8_t reg, uint8_t val);
uint8_t ccdbg_read_reg(struct ccdbg *ccdbg, uint8_t reg);

uint16_t ccdbg_get_chip_id(struct ccdbg *ccdbg);

void ccdbg_write_block(struct ccdbg *ccdbg, const uint8_t *buf,
    uint16_t addr, uint16_t len);
void ccdbg_read_block(struct ccdbg *ccdbg, uint8_t *buf,
    uint16_t addr, uint16_t len);

void ccdbg_flash_block(struct ccdbg *ccdbg, const uint8_t *buf,
    uint16_t addr, uint16_t len);

void ccdbg_erase(struct ccdbg *ccdbg);

bool ccdbg_open(struct ccdbg *ccdbg, const struct ccdbg_ops *ops, void *ctx);
void ccdbg_close(struct ccdbg *ccdbg);

#endif /* !CCDBG_H */
