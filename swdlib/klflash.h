/*
 * swdlib/klflash.c - KL2x Flash programming
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef KLFLASH_H
#define KLFLASH_H

#include <stdbool.h>
#include <stdint.h>

#include "swdlib.h"


bool kl_mass_erase(struct swd *swd);
bool kl_flash_security(struct swd *swd, bool *can_erase, bool *secure,
    bool *backdoor);

bool kl_program_flash(struct swd *swd,
    uint32_t addr, const uint32_t *buf, uint32_t words,
    void (*progress)(void *user, bool verify, uint16_t pos), void *user);
bool kl_read_flash(struct swd *swd,
    uint32_t addr, uint32_t *buf, uint32_t words);

#endif /* !KLFLASH_H */
