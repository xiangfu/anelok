/*
 * swdlib/ccdbg.c - TI CCxxx debug protocol driver
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdlib.h>

#include "8051.h"
#include "cc2543.h"

#include "ccdbg.h"


#define	CC2543_ID	0x43

enum {
	CHIP_ERASE	= 0x14,
	WR_CONFIG	= 0x1d,
	RD_CONFIG	= 0x20,	/* CC254x */
	GET_PC		= 0x28,	/* CC254x */
	READ_STATUS	= 0x34,
	SET_HW_BRKPNT	= 0x38,	/* CC254x */
	GET_CHIP_ID	= 0x68,
	HALT		= 0x44,
	RESUME		= 0x4c,
	DEBUG_INSTR_1	= 0x55,
	DEBUG_INSTR_2	= 0x56,
	DEBUG_INSTR_3	= 0x57,
	STEP_INSTR	= 0x58,	/* CC2543 */
	BURST_WRITE	= 0x80,	/* CC2543 */
};


/* ----- Wrappers for platform operations ---------------------------------- */


static inline void ccdbg_send(struct ccdbg *ccdbg, uint8_t data)
{
	ccdbg->ops->send(ccdbg->ctx, data);
}


static inline uint8_t ccdbg_recv(struct ccdbg *ccdbg)
{
	return ccdbg->ops->recv(ccdbg->ctx);
}


static inline bool ccdbg_sample(struct ccdbg *ccdbg)
{
	return ccdbg->ops->sample(ccdbg->ctx);
}


static inline void ccdbg_pulse(struct ccdbg *ccdbg)
{
	ccdbg->ops->pulse(ccdbg->ctx);
}


static inline void ccdbg_reset(struct ccdbg *ccdbg, bool active)
{
	return ccdbg->ops->reset(ccdbg->ctx, active);
}


#define	ccdbg_report(fmt, ...)	ccdbg->ops->report(fmt, ##__VA_ARGS__)


/* ----- Debug protocol primitives ----------------------------------------  */


static void turn(struct ccdbg *ccdbg)
{
	uint8_t i;

	for (i = 0; i != 10; i++) {
		if (!ccdbg_sample(ccdbg))
			return;
		ccdbg_recv(ccdbg);
	}
	ccdbg_report("timeout\n");
	//exit(1);
}


uint16_t ccdbg_get_chip_id(struct ccdbg *ccdbg)
{
	uint8_t id;

	ccdbg_send(ccdbg, GET_CHIP_ID);
	turn(ccdbg);
	id = ccdbg_recv(ccdbg);
	return id << 8 | ccdbg_recv(ccdbg);
}


static uint8_t wr_config(struct ccdbg *ccdbg, uint8_t val)
{
	ccdbg_send(ccdbg, WR_CONFIG);
	ccdbg_send(ccdbg, val);
	turn(ccdbg);
	return ccdbg_recv(ccdbg);
}


static uint8_t halt(struct ccdbg *ccdbg)
{
	ccdbg_send(ccdbg, HALT);
	turn(ccdbg);
	return ccdbg_recv(ccdbg);
}


static uint8_t debug_instr_1(struct ccdbg *ccdbg, uint8_t i1)
{
	ccdbg_send(ccdbg, DEBUG_INSTR_1);
	ccdbg_send(ccdbg, i1);
	turn(ccdbg);
	return ccdbg_recv(ccdbg);
}


static uint8_t debug_instr_2(struct ccdbg *ccdbg, uint8_t i1, uint8_t i2)
{
	ccdbg_send(ccdbg, DEBUG_INSTR_2);
	ccdbg_send(ccdbg, i1);
	ccdbg_send(ccdbg, i2);
	turn(ccdbg);
	return ccdbg_recv(ccdbg);
}


static uint8_t debug_instr_3(struct ccdbg *ccdbg,
    uint8_t i1, uint8_t i2, uint8_t i3)
{
	ccdbg_send(ccdbg, DEBUG_INSTR_3);
	ccdbg_send(ccdbg, i1);
	ccdbg_send(ccdbg, i2);
	ccdbg_send(ccdbg, i3);
	turn(ccdbg);
	return ccdbg_recv(ccdbg);
}


/* ----- Register access --------------------------------------------------- */


void ccdbg_write_reg(struct ccdbg *ccdbg, uint8_t reg, uint8_t val)
{
	debug_instr_3(ccdbg, 0x75, reg, val);	/* MOV reg, #val */
}


uint8_t ccdbg_read_reg(struct ccdbg *ccdbg, uint8_t reg)
{
	return debug_instr_2(ccdbg, 0xe5, reg);	/* MOV A, reg */
}


/* ---- Memory access (XRAM) ----------------------------------------------- */


void ccdbg_write_block(struct ccdbg *ccdbg, const uint8_t *buf,
    uint16_t addr, uint16_t len)
{
	debug_instr_3(ccdbg, MOV_DPRT_u16, addr >> 8, addr);
		/* MOV DPTR, #addr */
	while (len--) {
		debug_instr_2(ccdbg, MOV_A_u8, *buf++);	/* MOV A, #data */
		debug_instr_1(ccdbg, MOVX_DPTR_A);	/* MOVX @DPTR, A */
		debug_instr_1(ccdbg, INC_DPTR);		/* INC DPTR */
	}
}


void ccdbg_read_block(struct ccdbg *ccdbg, uint8_t *buf,
    uint16_t addr, uint16_t len)
{
	debug_instr_3(ccdbg, MOV_DPRT_u16, addr >> 8, addr);
		/* MOV DPTR, #addr */
	while (len--) {
		*buf++ = debug_instr_1(ccdbg, MOVX_A_DPTR); /* MOVX A, @DPTR */
		debug_instr_1(ccdbg, INC_DPTR);	/* INC DPTR */
	}
}


/* ----- Flash write ------------------------------------------------------- */


/*
 * Contrary to what swra410 suggests, we don't use DMA for sending data to
 * the CC2543. Instead, we use individual writes, which is slower but reuses
 * code we need either way.
 */

#define	BUF_ADDR	0x0000
#define	BUF_SIZE	0x0200
#define	DMA2_CFG_ADDR	0x0208


static void flash_block(struct ccdbg *ccdbg, const uint8_t *buf,
    uint16_t addr, uint16_t len)
{
	uint8_t dma_cfg_2[] = {
		BUF_ADDR >> 8,		/* 0 */
		BUF_ADDR,		/* 1 */
		FWDATA_REG >> 8,	/* 2 */
		FWDATA_REG & 0xff,	/* 3 */
		len >> 8,		/* 4 */
		len,			/* 5 */
		0x12,			/* 6: byte, single, FLASH */
		0x42,			/* 7: inc src 1, inc dst 0, high prio */
	};
	uint8_t addr_high = addr >> 10;	/* Flash controller is 32 bit */
	uint8_t addr_low = addr >> 2;
	uint8_t fctl = FCTL_WRITE;

	/* load the buffer */

	ccdbg_write_block(ccdbg, buf, BUF_ADDR, len);

	/* set up DMA configuration */

	ccdbg_write_block(ccdbg, dma_cfg_2, DMA2_CFG_ADDR, 8);
	ccdbg_write_reg(ccdbg, DMA1CFGH_REG, DMA2_CFG_ADDR >> 8);
	ccdbg_write_reg(ccdbg, DMA1CFGL_REG, DMA2_CFG_ADDR & 0xff);

	/* set Flash address */

	ccdbg_write_block(ccdbg, &addr_high, FADDRH, 1);
	ccdbg_write_block(ccdbg, &addr_low, FADDRL, 1);

	/* arm channel 1 */

	ccdbg_write_reg(ccdbg, DMAARM_REG, DMAARM_DMAARM1);

	/* start Flash controller */

	ccdbg_write_block(ccdbg, &fctl, FCTL_REG, 1);

	do ccdbg_read_block(ccdbg, &fctl, FCTL_REG, 1);
	while (fctl & FCTL_BUSY);
}


void ccdbg_flash_block(struct ccdbg *ccdbg, const uint8_t *buf,
    uint16_t addr, uint16_t len)
{
	while (len > BUF_SIZE) {
		flash_block(ccdbg, buf, addr, BUF_SIZE);
		buf += BUF_SIZE;
		addr += BUF_SIZE;
		len -= BUF_SIZE;
	}
	flash_block(ccdbg, buf, addr, len);
}


/* ----- Chip erase -------------------------------------------------------- */


void ccdbg_erase(struct ccdbg *ccdbg)
{
	uint8_t s;

	ccdbg_send(ccdbg, CHIP_ERASE);
	turn(ccdbg);
	s = ccdbg_recv(ccdbg);
	while (s & 0x80) {
		ccdbg_send(ccdbg, READ_STATUS);
		s = ccdbg_recv(ccdbg);
	}
}


/* ----- Open/close -------------------------------------------------------- */


bool ccdbg_open(struct ccdbg *ccdbg, const struct ccdbg_ops *ops, void *ctx)
{
	uint16_t id;

	ccdbg->ops = ops;
	ccdbg->ctx = ctx;

	ccdbg_reset(ccdbg, 1);
	ccdbg_pulse(ccdbg);
	ccdbg_pulse(ccdbg);
	ccdbg_reset(ccdbg, 0);

	id = ccdbg_get_chip_id(ccdbg);
	switch (id >> 8) {
	case CC2543_ID:
		ccdbg_report("CC2543");
		break;
	default:
		ccdbg_report("ID 0x%02x\n", id >> 8);
		return 0;
	}
	ccdbg_report(" rev %u\n", id & 0xff);

	wr_config(ccdbg, 0x20);	/* enable DMA and timers */
	halt(ccdbg);

	return 1;
}


void ccdbg_close(struct ccdbg *ccdbg)
{
}
