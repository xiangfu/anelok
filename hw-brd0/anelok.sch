EESchema Schematic File Version 2
LIBS:pwr
LIBS:r
LIBS:c
LIBS:led
LIBS:powered
LIBS:kl25-48
LIBS:memcard8
LIBS:micro_usb_b
LIBS:varistor
LIBS:pmosfet-gsd
LIBS:antenna
LIBS:er-oled-fpc30
LIBS:at86rf231
LIBS:balun-smt6
LIBS:xtal-4
LIBS:testpoint
LIBS:tswa
LIBS:device_sot
LIBS:gencon
LIBS:aat1217
LIBS:inductor
LIBS:switch
LIBS:anelok-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Anelok Password Safe"
Date "9 oct 2013"
Rev ""
Comp "Werner Almesberger"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9600 2100 1000 500 
U 5235DC63
F0 "User Interface" 50
F1 "user.sch" 50
$EndSheet
$Sheet
S 9600 3000 1000 500 
U 5235DC65
F0 "RF" 50
F1 "rf.sch" 50
$EndSheet
$Sheet
S 9600 3950 1000 450 
U 5235DEF3
F0 "Memcard" 50
F1 "memcard.sch" 50
$EndSheet
$Sheet
S 9600 1200 1000 500 
U 5235E128
F0 "Power+USB" 50
F1 "power.sch" 50
$EndSheet
Wire Wire Line
	2400 4500 3600 4500
Wire Wire Line
	2400 4350 3600 4350
Wire Wire Line
	3200 4650 3200 5600
Wire Wire Line
	3200 4650 3600 4650
Text GLabel 6250 6200 3    60   Input ~ 0
RF_CLK
Wire Wire Line
	6250 5800 6250 6200
$Comp
L C C10
U 1 1 5235EA1A
P 2400 5200
F 0 "C10" H 2450 5300 60  0000 L CNN
F 1 "100nF" H 2450 5100 60  0000 L CNN
F 2 "" H 2400 5200 60  0000 C CNN
F 3 "" H 2400 5200 60  0000 C CNN
	1    2400 5200
	1    0    0    -1  
$EndComp
Text GLabel 1100 3900 0    60   Input ~ 0
USB_VBUS
Text GLabel 1100 3450 0    60   3State ~ 0
USB_DP
Text GLabel 1100 3600 0    60   3State ~ 0
USB_DM
$Comp
L C C9
U 1 1 5235EA2B
P 1900 5200
F 0 "C9" H 1950 5300 60  0000 L CNN
F 1 "1uF" H 1950 5100 60  0000 L CNN
F 2 "" H 1900 5200 60  0000 C CNN
F 3 "" H 1900 5200 60  0000 C CNN
	1    1900 5200
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 5235EA31
P 1400 5200
F 0 "C8" H 1450 5300 60  0000 L CNN
F 1 "2.2uF" H 1450 5100 60  0000 L CNN
F 2 "" H 1400 5200 60  0000 C CNN
F 3 "" H 1400 5200 60  0000 C CNN
	1    1400 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3900 3600 3900
Wire Wire Line
	1100 3750 3600 3750
Wire Wire Line
	1400 3750 1400 5000
$Comp
L C C11
U 1 1 5235EA76
P 1400 6500
F 0 "C11" H 1450 6600 60  0000 L CNN
F 1 "1uF" H 1450 6400 60  0000 L CNN
F 2 "" H 1400 6500 60  0000 C CNN
F 3 "" H 1400 6500 60  0000 C CNN
	1    1400 6500
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 5235EA7C
P 2700 6500
F 0 "C12" H 2750 6600 60  0000 L CNN
F 1 "1uF" H 2750 6400 60  0000 L CNN
F 2 "" H 2700 6500 60  0000 C CNN
F 3 "" H 2700 6500 60  0000 C CNN
	1    2700 6500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5235EA99
P 2400 5600
F 0 "#PWR01" H 2400 5600 30  0001 C CNN
F 1 "GND" H 2400 5530 30  0001 C CNN
F 2 "" H 2400 5600 60  0000 C CNN
F 3 "" H 2400 5600 60  0000 C CNN
	1    2400 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5235EA9F
P 1900 5600
F 0 "#PWR02" H 1900 5600 30  0001 C CNN
F 1 "GND" H 1900 5530 30  0001 C CNN
F 2 "" H 1900 5600 60  0000 C CNN
F 3 "" H 1900 5600 60  0000 C CNN
	1    1900 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5235EAA5
P 1400 5600
F 0 "#PWR03" H 1400 5600 30  0001 C CNN
F 1 "GND" H 1400 5530 30  0001 C CNN
F 2 "" H 1400 5600 60  0000 C CNN
F 3 "" H 1400 5600 60  0000 C CNN
	1    1400 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5400 2400 5600
Wire Wire Line
	1900 5400 1900 5600
Wire Wire Line
	1400 5400 1400 5600
Text GLabel 1200 6100 0    60   Input ~ 0
VSYS
Text GLabel 1100 3150 0    60   Input ~ 0
VBAT
Text GLabel 5950 6200 3    60   Input ~ 0
VSYS
Wire Wire Line
	5950 5800 5950 6200
$Comp
L GND #PWR04
U 1 1 5235EB49
P 2700 6900
F 0 "#PWR04" H 2700 6900 30  0001 C CNN
F 1 "GND" H 2700 6830 30  0001 C CNN
F 2 "" H 2700 6900 60  0000 C CNN
F 3 "" H 2700 6900 60  0000 C CNN
	1    2700 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5235EB4F
P 1400 6900
F 0 "#PWR05" H 1400 6900 30  0001 C CNN
F 1 "GND" H 1400 6830 30  0001 C CNN
F 2 "" H 1400 6900 60  0000 C CNN
F 3 "" H 1400 6900 60  0000 C CNN
	1    1400 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5235EB55
P 6100 6000
F 0 "#PWR06" H 6100 6000 30  0001 C CNN
F 1 "GND" H 6100 5930 30  0001 C CNN
F 2 "" H 6100 6000 60  0000 C CNN
F 3 "" H 6100 6000 60  0000 C CNN
	1    6100 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5800 6100 6000
Wire Wire Line
	2500 6100 2700 6100
Wire Wire Line
	2700 6100 2700 6300
Wire Wire Line
	1400 6300 1400 6100
Wire Wire Line
	1400 6700 1400 6900
Wire Wire Line
	2700 6700 2700 6900
Text Notes 2300 7150 0    60   ~ 0
Near pins 1/2
$Comp
L C C7
U 1 1 5235EBEE
P 2800 4900
F 0 "C7" H 2850 5000 60  0000 L CNN
F 1 "100nF" H 2850 4800 60  0000 L CNN
F 2 "" H 2800 4900 60  0000 C CNN
F 3 "" H 2800 4900 60  0000 C CNN
	1    2800 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4700 2800 4500
Connection ~ 2400 4500
Connection ~ 2800 4500
$Comp
L GND #PWR07
U 1 1 5235EC2F
P 2800 5600
F 0 "#PWR07" H 2800 5600 30  0001 C CNN
F 1 "GND" H 2800 5530 30  0001 C CNN
F 2 "" H 2800 5600 60  0000 C CNN
F 3 "" H 2800 5600 60  0000 C CNN
	1    2800 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5235EC35
P 3200 5600
F 0 "#PWR08" H 3200 5600 30  0001 C CNN
F 1 "GND" H 3200 5530 30  0001 C CNN
F 2 "" H 3200 5600 60  0000 C CNN
F 3 "" H 3200 5600 60  0000 C CNN
	1    3200 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5100 2800 5600
$Comp
L GND #PWR09
U 1 1 5235EC59
P 3400 5600
F 0 "#PWR09" H 3400 5600 30  0001 C CNN
F 1 "GND" H 3400 5530 30  0001 C CNN
F 2 "" H 3400 5600 60  0000 C CNN
F 3 "" H 3400 5600 60  0000 C CNN
	1    3400 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4800 3400 4800
Wire Wire Line
	3400 3300 3400 5600
Wire Wire Line
	3600 3300 3400 3300
Connection ~ 3400 4800
Wire Wire Line
	1100 3150 3600 3150
Wire Wire Line
	1100 3450 3600 3450
Wire Wire Line
	3600 3600 1100 3600
Text GLabel 7450 3900 2    60   Output ~ 0
DISP_SDIN
Text GLabel 7450 4800 2    60   Input ~ 0
RF_IRQ
Text GLabel 5950 1950 1    60   Input ~ 0
RF_MISO
Text GLabel 7450 3450 2    60   Input ~ 0
RF_DIG2
Text GLabel 7450 3300 2    60   Output ~ 0
RF_nRST
Text GLabel 5800 1950 1    60   Output ~ 0
RF_MOSI
Text GLabel 7450 3600 2    60   Output ~ 0
RF_SLP_TR
Text GLabel 7450 3150 2    60   Output ~ 0
RF_nSS
Text GLabel 6100 1950 1    60   Output ~ 0
RF_SCLK
Text GLabel 4750 1950 1    60   Output ~ 0
DISP_SCLK
Text GLabel 7450 4200 2    60   Output ~ 0
DISP_nRES
Text GLabel 7450 4350 2    60   Output ~ 0
DISP_nCS
$Comp
L TESTPOINT TP1
U 1 1 5235F91E
P 8000 4650
F 0 "TP1" V 8000 4900 60  0000 C CNN
F 1 "TESTPOINT" H 8000 4600 60  0001 C CNN
F 2 "" H 8000 4650 60  0000 C CNN
F 3 "" H 8000 4650 60  0000 C CNN
	1    8000 4650
	0    1    1    0   
$EndComp
$Comp
L TESTPOINT TP3
U 1 1 5235F92B
P 5650 6700
F 0 "TP3" H 5650 6950 60  0000 C CNN
F 1 "TESTPOINT" H 5650 6650 60  0001 C CNN
F 2 "" H 5650 6700 60  0000 C CNN
F 3 "" H 5650 6700 60  0000 C CNN
	1    5650 6700
	-1   0    0    1   
$EndComp
$Comp
L TESTPOINT TP2
U 1 1 5235F931
P 5200 6700
F 0 "TP2" H 5200 6950 60  0000 C CNN
F 1 "TESTPOINT" H 5200 6650 60  0001 C CNN
F 2 "" H 5200 6700 60  0000 C CNN
F 3 "" H 5200 6700 60  0000 C CNN
	1    5200 6700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5200 5800 5200 6700
Wire Wire Line
	5650 5800 5650 6700
Text GLabel 5500 1950 1    60   3State ~ 0
CARD_DAT0
Text GLabel 5650 1950 1    60   3State ~ 0
CARD_DAT1
Text GLabel 4900 1950 1    60   3State ~ 0
CARD_DAT2
Text GLabel 5050 1950 1    60   3State ~ 0
CARD_DAT3
Wire Wire Line
	5650 1950 5650 2150
Wire Wire Line
	5500 1950 5500 2150
Wire Wire Line
	5350 1950 5350 2150
Wire Wire Line
	5200 1950 5200 2150
Text GLabel 5350 1950 1    60   Output ~ 0
CARD_CLK
Text GLabel 5200 1950 1    60   3State ~ 0
CARD_CMD
Wire Wire Line
	4900 1950 4900 2150
Wire Wire Line
	5050 1950 5050 2150
Wire Wire Line
	4750 2150 4750 1950
Text GLabel 1100 4200 0    60   Output ~ 0
CARD_OFF
Text GLabel 7450 4050 2    60   Output ~ 0
DISP_DnC
Text GLabel 4600 1950 1    60   Input ~ 0
USB_ID
Wire Wire Line
	5800 1950 5800 2150
Wire Wire Line
	5950 1950 5950 2150
Wire Wire Line
	6100 1950 6100 2150
Wire Wire Line
	7250 3150 7450 3150
Wire Wire Line
	7250 3300 7450 3300
Wire Wire Line
	7250 3450 7450 3450
Wire Wire Line
	7250 3600 7450 3600
Wire Wire Line
	7250 3900 7450 3900
Wire Wire Line
	7250 4050 7450 4050
Wire Wire Line
	7250 4200 7450 4200
Wire Wire Line
	7250 4350 7450 4350
Text GLabel 5800 6700 3    60   Input ~ 0
WHEEL_CENTER
Text GLabel 5350 6700 3    60   Input ~ 0
WHEEL_A
Text GLabel 5500 6700 3    60   Input ~ 0
WHEEL_B
Wire Wire Line
	5350 5800 5350 6700
Wire Wire Line
	5500 5800 5500 6700
Text Notes 4550 7300 0    60   ~ 0
4-way Nav
Wire Notes Line
	5100 7050 5100 7150
Wire Notes Line
	5100 7150 4550 7150
Wire Notes Line
	4550 7150 4550 7050
Wire Wire Line
	7250 4650 8000 4650
Wire Wire Line
	5800 5800 5800 6700
Wire Wire Line
	7250 4800 7450 4800
Text GLabel 6250 1950 1    60   Input ~ 0
CARD_SW
Wire Wire Line
	4600 1950 4600 2150
Text GLabel 1100 4050 0    60   Output ~ 0
LED
Wire Wire Line
	3600 4200 1100 4200
Wire Wire Line
	6250 1950 6250 2150
Text GLabel 7450 3750 2    60   Output ~ 0
PWR_EN
Wire Wire Line
	7250 3750 7450 3750
Text Notes 8400 4550 0    60   ~ 0
Light sensor
$Comp
L TESTPOINT TP5
U 1 1 52361304
P 4600 6700
F 0 "TP5" H 4600 6950 60  0000 C CNN
F 1 "TESTPOINT" H 4600 6650 60  0001 C CNN
F 2 "" H 4600 6700 60  0000 C CNN
F 3 "" H 4600 6700 60  0000 C CNN
	1    4600 6700
	-1   0    0    1   
$EndComp
$Comp
L TESTPOINT TP6
U 1 1 5236130A
P 4750 6700
F 0 "TP6" H 4750 6950 60  0000 C CNN
F 1 "TESTPOINT" H 4750 6650 60  0001 C CNN
F 2 "" H 4750 6700 60  0000 C CNN
F 3 "" H 4750 6700 60  0000 C CNN
	1    4750 6700
	-1   0    0    1   
$EndComp
$Comp
L TESTPOINT TP7
U 1 1 52361310
P 4900 6700
F 0 "TP7" H 4900 6950 60  0000 C CNN
F 1 "TESTPOINT" H 4900 6650 60  0001 C CNN
F 2 "" H 4900 6700 60  0000 C CNN
F 3 "" H 4900 6700 60  0000 C CNN
	1    4900 6700
	-1   0    0    1   
$EndComp
$Comp
L TESTPOINT TP8
U 1 1 52361316
P 5050 6700
F 0 "TP8" H 5050 6950 60  0000 C CNN
F 1 "TESTPOINT" H 5050 6650 60  0001 C CNN
F 2 "" H 5050 6700 60  0000 C CNN
F 3 "" H 5050 6700 60  0000 C CNN
	1    5050 6700
	-1   0    0    1   
$EndComp
$Comp
L KL25-48 U1
U 1 1 5235CAC5
P 5400 3950
F 0 "U1" H 4000 5550 60  0000 C CNN
F 1 "MKL25Z128VFT4" H 5400 4000 60  0000 C CNN
F 2 "~" H 6200 3800 60  0000 C CNN
F 3 "~" H 6200 3800 60  0000 C CNN
	1    5400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5800 5050 6700
Wire Wire Line
	4900 5800 4900 6700
Wire Wire Line
	4600 5800 4600 6700
Wire Wire Line
	4750 5800 4750 6700
$Comp
L TESTPOINT TP4
U 1 1 523614B4
P 8000 4500
F 0 "TP4" V 8000 4750 60  0000 C CNN
F 1 "TESTPOINT" H 8000 4450 60  0001 C CNN
F 2 "" H 8000 4500 60  0000 C CNN
F 3 "" H 8000 4500 60  0000 C CNN
	1    8000 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 4500 8000 4500
Text Notes 5250 7050 3    60   ~ 0
SWD_CLK
Text Notes 5700 7050 3    60   ~ 0
SWD_DIO
Text Notes 8400 4700 0    60   ~ 0
nRESET
Wire Wire Line
	1100 4050 3600 4050
Text GLabel 1100 3750 0    60   Output ~ 0
VSYS
Connection ~ 1400 3750
$Comp
L GND #PWR010
U 1 1 5236D8A3
P 6600 6000
F 0 "#PWR010" H 6600 6000 30  0001 C CNN
F 1 "GND" H 6600 5930 30  0001 C CNN
F 2 "" H 6600 6000 60  0000 C CNN
F 3 "" H 6600 6000 60  0000 C CNN
	1    6600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5800 6600 6000
Wire Wire Line
	1900 5000 1900 3900
Connection ~ 1900 3900
Connection ~ 2400 4350
Text Notes 950  7150 0    60   ~ 0
Near pins 22/23
Text GLabel 2500 6100 0    60   Input ~ 0
VBAT
Wire Wire Line
	1400 6100 1200 6100
Wire Wire Line
	2400 3750 2400 5000
Connection ~ 2400 3750
Text Label 5200 6500 1    60   ~ 0
SWD_CLK
Text Label 5650 6500 1    60   ~ 0
SWD_DIO
Text Label 7450 4650 0    60   ~ 0
nRESET
Text Label 4600 6500 1    60   ~ 0
PTE29
Text Label 4750 6500 1    60   ~ 0
PTE30
Text Label 4900 6500 1    60   ~ 0
PTE24
Text Label 5050 6500 1    60   ~ 0
PTE25
Text Label 7450 4500 0    60   ~ 0
PTB0
$EndSCHEMATC
