/*
 * subst.h - Miscellaneous substitute functions
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef	SUBST_H
#define	SUBST_H

#include <stdint.h>
#include <stdio.h>


extern FILE *sim_mmc_file;


uint32_t sim_ticks(void);
void toggle_switch(void);

#endif /* !SUBST_H */
