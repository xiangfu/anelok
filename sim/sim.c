/*
 * sim.c - Anelok UI simulator
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "tick.h"
#include "devcfg.h"
#include "display.h"
#include "console.h"
#include "icon.h"
#include "touch.h"
#include "input.h"
#include "sel.h"
#include "ui.h"

#include "SDL.h"

#include "subst.h"


extern SDL_Surface *surf;
extern bool leave_ui;

static bool test_selection = 0;
static bool down = 0;
static uint8_t curr_pos;
static uint32_t tick_offset = 0;
static bool paused = 0;
static uint32_t pause_tick;


/* ----- Delays ------------------------------------------------------------ */


void msleep(unsigned ms)
{
	struct timespec req = {
		.tv_sec	= ms / 1000,
		.tv_nsec = (ms % 1000) * 1000000,
	};

	nanosleep(&req, NULL);
}


/* ----- Timekeeping ------------------------------------------------------- */


uint32_t sim_ticks(void)
{
	return SDL_GetTicks() - tick_offset;
}


uint16_t tick_now(void)
{
	return sim_ticks();
}


uint16_t tick_delta(uint16_t *t)
{
        uint16_t now = tick_now();
        uint16_t dt = now - *t;

        *t = now;
        return dt;
}


/* ----- Event handling ---------------------------------------------------- */


static void touch_press(uint8_t pos)
{
	down = 1;
	curr_pos = FB_Y - pos - 1;
}


static void touch_release(void)
{
	down = 0;
}


static void window_title(void)
{
	const char *s;

	s = paused ? "Paused" : "Anelok";
	SDL_WM_SetCaption(s, s);
}


static bool process_events(void)
{
	SDL_Event event;

	if (!SDL_PollEvent(&event)) {
		msleep(10);
		return 1;
	}

	switch (event.type) {
	case SDL_QUIT:
		return 0;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_p:
			paused = !paused;
			if (paused) {
				pause_tick = sim_ticks();
			} else {
				tick_offset += sim_ticks() - pause_tick;
			}
			window_title();
			return 1;
		case SDLK_q:
			return 0;
		default:
			break;
		}
	}

	if (paused)
		return 1;

	switch (event.type) {
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_PLUS:
		case SDLK_EQUALS:
			sim_swipe(1);
			break;
		case SDLK_MINUS:
			sim_swipe(-1);
			break;
		case SDLK_r:
			toggle_switch();
			break;
		case SDLK_t:
			if (test_selection) {
				sim_jump(jump_top);
				sim_show();
			}
			break;
		case SDLK_m:
			if (test_selection) {
				sim_jump(jump_middle);
				sim_show();
			}
			break;
		case SDLK_b:
			if (test_selection) {
				sim_jump(jump_bottom);
				sim_show();
			}
			break;
		case SDLK_1:
			if (test_selection) {
				sim_scroll_mode(smooth_and_centered);
				sim_show();
			}
			break;
		case SDLK_2:
			if (test_selection) {
				sim_scroll_mode(smooth_but_wrong);
				sim_show();
			}
			break;
		case SDLK_3:
			if (test_selection) {
				sim_scroll_mode(bumpy_and_correct);
				sim_show();
			}
			break;
		default:
			break;
		}
		break;
	case SDL_MOUSEMOTION:
		if (event.motion.state & SDL_BUTTON_LMASK)
			touch_press(event.motion.y);
		else
			touch_release();
		break;
	case SDL_MOUSEBUTTONDOWN:
		if (event.button.state == SDL_BUTTON_LEFT)
			touch_press(event.button.y);
		break;
	case SDL_MOUSEBUTTONUP:
		touch_release();
		break;
	}
	return 1;
}


bool touch_filter(uint8_t *pos)
{
	do {
		if (!process_events()) 
			leave_ui = 1;
	} while (paused && !leave_ui);
	if (down)
		*pos = curr_pos;
	return down;
}


/* ----- Command-line processing ------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr, "usage: %s [-m memcard.bin] [-t]\n", name);
	exit(1);
}


int main(int argc, char **argv)
{
	int c;

	while ((c = getopt(argc, argv, "m:t")) != EOF)
		switch (c) {
		case 'm':
			sim_mmc_file = fopen(optarg, "r+");
			if (!sim_mmc_file) {
				perror(optarg);
				return 1;
			}
			break;
		case 't':
			test_selection = 1;
			break;
		default:
			usage(*argv);
		}
	if (argc != optind)
		usage(*argv);

	devcfg_init();
	display_init();
	console_init();
	window_title();

	SDL_EnableKeyRepeat(300, 50);

	if (test_selection)
		ui_select();
	else
		ui_off();

	ui_init();
	while (ui());

	SDL_UnlockSurface(surf);

	return 0;
}
