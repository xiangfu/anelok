/*
 * subst.c - Miscellaneous substitute functions
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "subst.h"


/* ----- panic ------------------------------------------------------------- */


#include "qa.h"


void panic(void)
{
	fprintf(stderr, "Panic !\n");
	abort();
}


/* ----- reset_cpu --------------------------------------------------------- */


/*
 * We can't or at least shouldn't include misc.h, so we have to provide our own
 * prototype.
 */

void reset_cpu(void) __attribute__((noreturn));


void reset_cpu(void)
{
	fprintf(stderr, "CPU reset\n");
	exit(1);
}


/* ----- id_eq, id_get ----------------------------------------------------- */


#include "id.h"


#define	UID_MH	0		/* make simulator entry easy to spot */
#define	UID_ML	0xa2e10b	/* "ANELOK" */
#define	UID_L	0x513c1a70	/* "SIMULATO" */


bool id_eq(const struct id *id)
{
	return id->uidmh == UID_MH && id->uidml == UID_ML &&
	    id->uidl == UID_L;

}


void id_get(struct id *id)
{
	id->uidmh = UID_MH;
	id->uidml = UID_ML;
	id->uidl = UID_L;
}


/* ----- Power management -------------------------------------------------- */


#include "power.h"


void power_disp(bool on)
{
}


void power_disp_force_on(void)
{
}


/* ----- USB --------------------------------------------------------------- */


#include "usb-board.h"
#include "vusb.h"
#include "hid-type.h"


/* Nothing is connected to USB */


bool usb_a(void)
{
	return 0;
}


bool vusb_sense(void)
{
	return 0;
}


void hid_type(struct hid_type_ctx *ctx, const char *s,
    void (*fn)(void *user), void *user)
{
	printf("HID: %s\n", s);
	if (fn)
		fn(user);
}


/* ----- Memory card ------------------------------------------------------- */


#include "mmc-hw.h"
#include "mmc.h"


FILE *sim_mmc_file = NULL;


bool card_present(void)
{
	return sim_mmc_file;
}


bool mmc_init(void)
{
	return sim_mmc_file;
}


bool mmc_read_cid(uint8_t *cid)
{
	return 0;
}


/* --- Memory card blocks --- */


bool mmc_begin_read(uint32_t addr)
{
	if (!sim_mmc_file)
		return 0;
	if (fseek(sim_mmc_file, addr, SEEK_SET) < 0)
		return 0;
	return ftell(sim_mmc_file) == addr;
}


uint8_t mmc_read(void)
{
	assert(sim_mmc_file);
	return fgetc(sim_mmc_file);
}


bool mmc_end_read(void)
{
	assert(sim_mmc_file);
	return 1;
}


/* ----- CC2543 ------------------------------------------------------------ */


#include "ccdbg.h"


uint16_t ccdbg_get_chip_id(struct ccdbg *ccdbg)
{
	return 0x43ff;	/* valid chip ID, nonsensical version */
}


#include "cc.h"


struct ccdbg ccdbg;

static bool rf_switch = 1;


bool cc_on(void)
{
	return rf_switch;
}


bool cc_acquire(void)
{
	return rf_switch; /* just in case */
}


void cc_release(void)
{
}


void toggle_switch(void)
{
	rf_switch = !rf_switch;
}


/* ----- Communication handler --------------------------------------------- */


#include "comm.h"


static struct hid_type_ctx type;


bool comm_send(const struct account *acct,
    const struct account_field *field)
{
	hid_type(&type, field->text, NULL, NULL);
	input_refresh();
	return 1;
}


enum comm comm_mode(const struct account *acct,
    const struct account_field *field)
{
	return field->type == field_pw ? comm_hid : comm_none;
}
