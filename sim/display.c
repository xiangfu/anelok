/*
 * sim/display.c - Display simulator based on SDL_gfx
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include "tick.h"
#include "led.h"
#include "display.h"

#include "subst.h"


#define	FB_X	128
#define	FB_Y	64

#define	LED_X	(FB_X - 1 - LED_R - 20)
#define	LED_Y	(FB_Y - 1 - LED_R)
#define	LED_R	5
#define	LED_COLOR	0xe80000ff

#define	SDL_SURFACE	SDL_HWSURFACE


SDL_Surface *surf;

static bool rotate;


/* ----- LED --------------------------------------------------------------- */


#include <stdio.h>


static bool led_is_on = 0;


static SDL_Surface *sdl_surface(Uint32 flags, int width, int height)
{
	SDL_Surface *s;
	SDL_PixelFormat *fmt;

	fmt = SDL_GetVideoInfo()->vfmt;
	s = SDL_CreateRGBSurface(flags, width, height,
	    fmt->BitsPerPixel, fmt->Rmask, fmt->Gmask, fmt->Bmask, fmt->Amask);
	if (!s) {
		fprintf(stderr, "SDL_CreateRGBSurface failed: %s\n",
		    SDL_GetError());
		exit(1);
	}
	return s;
}


static void sdl_blit(SDL_Surface *src, SDL_Rect *srcrect,
    SDL_Surface *dst, SDL_Rect *dstrect)
{
	if (SDL_BlitSurface(src, srcrect, dst, dstrect)) {
		fprintf(stderr, "SDL_BlitSurface failed: %s\n",
		    SDL_GetError());
		exit(1);
	}
}


static void display_led(bool on)
{
	SDL_Surface *s;
	SDL_Rect r;

	SDL_UnlockSurface(surf);

	s = sdl_surface(SDL_SURFACE, 2 * LED_R + 1, 2 * LED_R + 1);
	r.x = LED_X - LED_R;
	r.y = LED_Y - LED_R;
	r.w = r.h = 2 * LED_R + 1;
	sdl_blit(surf, &r, s, NULL);

	if (on)
		SDL_SetColorKey(s, SDL_SRCCOLORKEY,
		    SDL_MapRGB(s->format, 0, 0, 0));
	else
		SDL_SetColorKey(s, SDL_SRCCOLORKEY,
		    SDL_MapRGB(s->format,
		    (uint8_t) (LED_COLOR >> 24), (uint8_t) (LED_COLOR >> 16),
		    (uint8_t) (LED_COLOR >> 8)));

	SDL_LockSurface(s);
	if (on)
		filledCircleColor(surf, LED_X, LED_Y, LED_R, LED_COLOR);
	else
		SDL_FillRect(surf, &r, SDL_MapRGB(surf->format, 0, 0, 0));
	SDL_UnlockSurface(s);

	sdl_blit(s, NULL, surf, &r);

	SDL_FreeSurface(s);

	SDL_UpdateRect(surf, r.x, r.y, r.w, r.h);
	SDL_LockSurface(surf);
}


void led(bool on)
{
	static uint32_t last;
	uint32_t now;

	now = sim_ticks();
	if (now < last + 40)
		msleep(last + 40 - now);
	led_is_on = on;
	display_led(on);
	last = sim_ticks();
}


void led_toggle(void)
{
	led(!led_is_on);
}


/* ----- OLED display ------------------------------------------------------ */


void display_clear(void)
{
	SDL_FillRect(surf, NULL, SDL_MapRGB(surf->format, 0, 0, 0));
}


void display_set(uint8_t x, uint8_t y)
{
	if (rotate)
		pixelColor(surf, FB_X - 1 - x, FB_Y - 1 - y, 0xffffffff);
	else
		pixelColor(surf, x, y, 0xffffffff);
}


void display_clr(uint8_t x, uint8_t y)
{
	if (rotate)
		pixelColor(surf, FB_X - 1 - x, FB_Y - 1 - y, 0x000000ff);
	else
		pixelColor(surf, x, y, 0x000000ff);
}


void display_blit(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y)
{
	int8_t y1 = y + h;
	uint8_t t;

	while (y != y1) {
		for (t = 0; t != w; t++) {
			if (p[t >> 3] & (1 << (t & 7)))
				display_set(x + t, y);
		}
		y++;
		p += span;
	}
}



void display_clear_rect(uint8_t xa, uint8_t ya, uint8_t xb, uint8_t yb)
{
	if (rotate)
		boxColor(surf,
		    FB_X - xb - 1, FB_Y - yb - 1, FB_X - xa - 1, FB_Y - ya - 1,
		    0x000000ff);
	else
		boxColor(surf, xa, ya, xb, yb, 0x000000ff);
}


void display_rotate(bool on)
{
	/*
	 * In Anelok, the OLED is rotated, so when the device is rotated,
	 * the display isn't and vice versa.
	 */
	rotate = !on;
}


bool display_update(void)
{
	if (led_is_on)
		display_led(1);
	SDL_UnlockSurface(surf);
	SDL_UpdateRect(surf, 0, 0, 0, 0);
	SDL_LockSurface(surf);
	return 0; /* the simulator updates its display very quickly */
}


void display_on(bool clear)
{
	if (clear) {
		display_clear();
		display_update();
	}
}


void display_off(void)
{
}


void display_init(void)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf = SDL_SetVideoMode(FB_X, FB_Y, 0, SDL_SWSURFACE);
	SDL_LockSurface(surf);
}
