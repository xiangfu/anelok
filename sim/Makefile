#
# sim/Makefile - Makefile of the Anelok UI simulator
#
# Written 2014-2015 by Werner Almesberger
# Copyright 2014-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

include ../common/Makefile.c-common


NAME = sim

USB_STACK = ../../ben-wpan/atusb/fw/usb

CFLAGS_WARN = -Wall -Wshadow -Wmissing-prototypes \
	      -Wmissing-declarations -Wno-format-zero-length
CFLAGS = -g $(CFLAGS_WARN) \
	 -I../fw -I../fw/disp -I../fw/base -I../fw/ui -I../fw/plat \
	 -I../fw/board -I../swdlib -I../fw/cc -I../fw/db -I../fw/common \
	 -I../fw/usb -I../fw//mmc -I../fw/2014 -I$(USB_STACK) \
	 -DSIMULATOR \
	 $(shell sdl-config --cflags)

LDLIBS = $(shell sdl-config --libs) -lSDL_gfx
OBJS = $(NAME).o display.o subst.o \
       text.o console.o devcfg.o misc.o fmt.o icon.o \
       input.o sel.o textsel.o std.o account.o auth.o qa.o pm.o \
       ui.o ui_off.o ui_login.o ui_login_setup.o ui_select.o ui_select_setup.o \
       ui_account.o \
       ui_show.o ui_sorry.o \
       version.o

VERSION_C = ../fw/board/version.c
include ../common/Makefile.version

.PHONY:		all fonts spotless

all::		$(NAME)

#
# Note: since we already have a "phony" item (fonts) in our dependencies, each
# run of "make" will compile code. Since we can't avoid this, we just add
# version.o there as well and don't give it a special treatment (see
# ../fw/Makefile) when linking.
#

$(NAME):	$(OBJS)

vpath text.c ../fw/disp
vpath console.c ../fw/disp
vpath devcfg.c ../fw/board
vpath misc.c ../fw/base
vpath fmt.c ../fw/base
vpath pm.c ../fw/base
vpath qa.c ../fw/base
vpath account.c ../fw/db
vpath auth.c ../fw/db
vpath icon.c ../fw/ui
vpath input.c ../fw/ui
vpath sel.c ../fw/ui
vpath textsel.c ../fw/ui
vpath std.c ../fw/ui
vpath ui.c ../fw/ui
vpath ui_off.c ../fw/ui
vpath ui_login.c ../fw/ui
vpath ui_login_setup.c ../fw/ui
vpath ui_select.c ../fw/ui
vpath ui_select_setup.c ../fw/ui
vpath ui_account.c ../fw/ui
vpath ui_show.c ../fw/ui
vpath ui_sorry.c ../fw/ui

text.o:		fonts
devcfg.o:	../fw/devcfg.inc

fonts:
		$(MAKE) -C ../fw fonts

../fw/devcfg.inc:
		$(MAKE) -C ../fw devcfg.inc

spotless:	clean
		rm -f $(NAME)
		rm -f .version
